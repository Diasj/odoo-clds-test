from odoo import models, fields, tools, api, _, SUPERUSER_ID
from odoo.exceptions import ValidationError, Warning, UserError

class Interest(models.Model):
    _name = 'interest'
    _description = 'Interest'
    _order = 'name'


    name = fields.Char(string='Name', translate=True)