from odoo import api, tools, fields, models, SUPERUSER_ID, _
from openerp.exceptions import except_orm, Warning, RedirectWarning, UserError, ValidationError
from datetime import date, datetime

class IrAttachment(models.Model):
    _inherit = 'ir.attachment'
    

    def create(self, values):
        if not self.env.user.has_group('clds.group_clds_admin'):
            if type(values) == dict:
                res_model = values.get('res_model',False)
                if res_model == 'fleet.vehicle':
                    res_id = values.get('res_id',False)
                    fleet_vehicle_id = self.env[res_model].sudo().search([('id','=',res_id)])
                    if fleet_vehicle_id.company_id.id != self.env.user.company_id.id: 
                        raise Warning(_('You cannot create an attachment for a resource that does not belong to your entity'))

        return super(IrAttachment, self).create(values)

    def unlink(self):
        for record in self:
            if not self.env.user.has_group('clds.group_clds_admin'):
                if record.res_model == 'fleet.vehicle':
                    fleet_vehicle_id = self.env[record.res_model].sudo().search([('id','=',record.res_id)])
                    if fleet_vehicle_id:
                        if fleet_vehicle_id.company_id.id != self.env.user.company_id.id:
                            raise Warning(_('You cannot delete an attachment for a resource that does not belong to your entity'))

        return super(IrAttachment, self).unlink()

   