# -*- coding: utf-8 -*-

from odoo import models, fields, tools, api, _, SUPERUSER_ID
from odoo.exceptions import ValidationError, Warning, UserError

class Job(models.Model):
    _inherit = 'hr.job'


    INTERVENTION_SELECT = [
        ('child_youth', 'Childhood and youth'),
        ('adult_population', 'Adult population'),
        ('family_community', 'Family and Community'), 
        ('all', 'All')
    ]

    OCCUPATION_AREA_SELECT = [
        ('support_social', 'Support and Social Solidarity'),
        ('education_literacy', 'Education and Literacy'),
        ('health', 'Health'),
        ('science_tech', 'Science and Technologies'),
        ('culture', 'Culture / Arts / Recreational Activities'),
        ('right_citizenship', 'Rights and Citizenship'),
        ('env_protection', 'Protection of Heritage / Environment / Animals'),
        ('civil_protection', 'Civil Protection / Disasters'),
        ('sport_leisure', 'Sport and Leisure'),
        ('human_aid', 'Development cooperation and / or humanitarian aid'),
        ('justice', 'Justice'),
        ('dev_social_economy', 'Development of Associative Life and Social Economy'),
        ('employment', 'Employment and Training'),
        ('other', 'Other')
    ]

    TARGET_GROUP_SELECT = [
        ('support_children', 'Support for children'),
        ('support_young', 'Support for the young'),
        ('support_elderly', 'Support for the Elderly'),
        ('support_disability', 'Support for People with Disabilities'),
        ('ethnic_minority', 'Ethnic minorities and migrants'),
        ('family_community', 'Family and Community'), 
        ('gender_equality', 'Gender equality'),
        ('animal', 'Animals'),
        ('other', 'Other')
    ]

    LITERARY_SELECT = [
        ('under_4', 'UNDER 4 YEARS OF SCHOOLING'),
        ('4_year', '4 YEARS OF SCHOOLING (1st cycle of basic education)'),
        ('6_year', '6 YEARS OF SCHOOLING (2nd cycle of basic education)'),
        ('9_year', '9th YEAR (3rd cycle of basic education)'),
        ('11_year', '11th YEAR'),
        ('12_year', '12th YEAR (secondary education)'), 
        ('professional_couse', 'TECHNOLOGICAL / PROFESSIONAL / OTHERS COURSE (Level III) *'),
        ('bachelor', 'BACHELOR'),
        ('graduation', 'GRADUATION'),
        ('postgraduate', 'POSTGRADUATE'),
        ('master', 'MASTER'),
        ('doctorate', 'DOCTORATE'),
        ('tech_course', 'TECHNOLOGICAL SPECIALIZATION COURSE'),
        ('ignored', 'IGNORED HABILITATION')
    ]

    OCCUPATION_SELECT = [
        ('not_required', 'Not Required'),
        ('medicine', 'Medicine'),
        ('sys_analyst', 'Systems Analyst'),
        ('engineer', 'Engineering'),
        ('administration', 'Administration'),
        ('economy', 'Economy'),
        ('nursing', 'Nursing'), 
        ('law', 'Law'),
        ('marketing', 'Marketing'),
        ('pharmacy', 'Pharmacy'),
        ('dentist', 'Dentist'),
        ('not_required', 'Not required')
    ]

    IDIOM_SELECT = [
        ('not_required', 'Not Required'),
        ('portuguese', 'Portuguese'),
        ('english', 'English'),
        ('french', 'French'),
        ('spanish', 'Spanish'),
        ('german', 'German')
    ]

    AGE_SELECT = [
        ('all','All'),
        ('1', 'Younger than 18'),
        ('2', '18-23'),
        ('3', '24-29'),
        ('4', '30-35'),
        ('5', '36-41'),
        ('6', '42-47'),
        ('7', '48-53'),
        ('8', '54-59'),
        ('9', '60-65'),
        ('10', 'Bigger than 65')
    ]
    
    LICENSE_SELECT = [
        ('not_required','Not Required'),
        ('a1', 'A1'),
        ('a', 'A'),
        ('b1', 'B1'),
        ('b', 'B'),
        ('c1', 'C1'),
        ('c', 'C'),
        ('b_e', 'B+E'),
        ('c1_e', 'C1+E'),
        ('c_e', 'C+E')
    ]


    beggining_date = fields.Date(string = "Beggining Date")
    finishing_date = fields.Date(string = "Finishing Date")
    town = fields.Char(string = "Town")
    location = fields.Char(string = "Oportunity Location")
    entity_name = fields.Char(string="Entity Name")
    responsibilities = fields.Char(string="Volunteer Responsibilities")
    postal_code = fields.Char(string = "Postal Code")
    target_group = fields.Selection(selection=TARGET_GROUP_SELECT, string='Target Group', copy=False, default='others')
    occupation_area = fields.Selection(selection=OCCUPATION_SELECT, string='Occupation Area', copy=False, default='others')
    intervention_area = fields.Selection(selection=INTERVENTION_SELECT, string='Intervention Area', copy=False, default='others')
    parish_id = fields.Many2one(comodel_name = 'res.parish', string = 'Parish')

    """
        * Volunteer requirements
    """
    age = fields.Selection(selection=AGE_SELECT, string='Age', copy=False, default='all')
    idiom = fields.Selection(selection=IDIOM_SELECT, string='Idiom', copy=False, default='not_required')
    occupation = fields.Selection(selection=OCCUPATION_SELECT, string='occupation', copy=False, default='not_required')
    volunteer_license = fields.Selection(selection=LICENSE_SELECT, string='Volunteer License', copy=False, default='not_required')
    literary_abilities = fields.Selection(selection=LITERARY_SELECT, string='Literary Abilities', copy=False, default='ignored')
    activity_image = fields.Image("Activity photo", max_width=420, max_height=420, store=True)
    extra_requirements = fields.Char(string="Extra Requirements")


class NewJob(models.Model):
    _name = 'new.job'
    _description = """
    NewJob class for when a new opportunity is suggested    
    """

    NEW_JOB_SELECT = [
        ('draft', 'Draft'),
        ('submit', 'Submit'),
        ('approve', 'Approve'), 
        ('reject', 'Reject')
    ]

    INTERVENTION_SELECT = [
        ('child_youth', 'Childhood and youth'),
        ('adult_population', 'Adult population'),
        ('family_community', 'Family and Community'), 
        ('all', 'All')
    ]

    OCCUPATION_AREA_SELECT = [
        ('support_social', 'Support and Social Solidarity'),
        ('education_literacy', 'Education and Literacy'),
        ('health', 'Health'),
        ('science_tech', 'Science and Technologies'),
        ('culture', 'Culture / Arts / Recreational Activities'),
        ('right_citizenship', 'Rights and Citizenship'),
        ('env_protection', 'Protection of Heritage / Environment / Animals'),
        ('civil_protection', 'Civil Protection / Disasters'),
        ('sport_leisure', 'Sport and Leisure'),
        ('human_aid', 'Development cooperation and / or humanitarian aid'),
        ('justice', 'Justice'),
        ('dev_social_economy', 'Development of Associative Life and Social Economy'),
        ('employment', 'Employment and Training'),
        ('other', 'Other')
    ]

    TARGET_GROUP_SELECT = [
        ('support_children', 'Support for children'),
        ('support_young', 'Support for the young'),
        ('support_elderly', 'Support for the Elderly'),
        ('support_disability', 'Support for People with Disabilities'),
        ('ethnic_minority', 'Ethnic minorities and migrants'),
        ('family_community', 'Family and Community'), 
        ('gender_equality', 'Gender equality'),
        ('animal', 'Animals'),
        ('other', 'Other')
    ]

    LITERARY_SELECT = [
        ('under_4', 'UNDER 4 YEARS OF SCHOOLING'),
        ('4_year', '4 YEARS OF SCHOOLING (1st cycle of basic education)'),
        ('6_year', '6 YEARS OF SCHOOLING (2nd cycle of basic education)'),
        ('9_year', '9th YEAR (3rd cycle of basic education)'),
        ('11_year', '11th YEAR'),
        ('12_year', '12th YEAR (secondary education)'), 
        ('professional_couse', 'TECHNOLOGICAL / PROFESSIONAL / OTHERS COURSE (Level III) *'),
        ('bachelor', 'BACHELOR'),
        ('graduation', 'GRADUATION'),
        ('postgraduate', 'POSTGRADUATE'),
        ('master', 'MASTER'),
        ('doctorate', 'DOCTORATE'),
        ('tech_course', 'TECHNOLOGICAL SPECIALIZATION COURSE'),
        ('ignored', 'IGNORED HABILITATION')
    ]

    OCCUPATION_SELECT = [
        ('not_required', 'Not Required'),
        ('medicine', 'Medicine'),
        ('sys_analyst', 'Systems Analyst'),
        ('engineer', 'Engineering'),
        ('administration', 'Administration'),
        ('economy', 'Economy'),
        ('nursing', 'Nursing'), 
        ('law', 'Law'),
        ('marketing', 'Marketing'),
        ('pharmacy', 'Pharmacy'),
        ('dentist', 'Dentist'),
        ('not_required', 'Not required')
    ]

    IDIOM_SELECT = [
        ('not_required', 'Not Required'),
        ('portuguese', 'Portuguese'),
        ('english', 'English'),
        ('french', 'French'),
        ('spanish', 'Spanish'),
        ('german', 'German')
    ]

    AGE_SELECT = [
        ('all','All'),
        ('1', 'Younger than 18'),
        ('2', '18-23'),
        ('3', '24-29'),
        ('4', '30-35'),
        ('5', '36-41'),
        ('6', '42-47'),
        ('7', '48-53'),
        ('8', '54-59'),
        ('9', '60-65'),
        ('10', 'Bigger than 65')
    ]

    LICENSE_SELECT = [
        ('not_required','Not Required'),
        ('a1', 'A1'),
        ('a', 'A'),
        ('b1', 'B1'),
        ('b', 'B'),
        ('c1', 'C1'),
        ('c', 'C'),
        ('b_e', 'B+E'),
        ('c1_e', 'C1+E'),
        ('c_e', 'C+E')
    ]

    state = fields.Selection(selection=NEW_JOB_SELECT, string = 'State', default='draft')
    company_id = fields.Many2one('res.company', string='Company', default=lambda self: self.env.company)
    name = fields.Char(string = "Opportunity Name")
    town = fields.Char(string = "Town")
    location = fields.Char(string = "Oportunity Location")
    responsibilities = fields.Char(string="Volunteer Responsibilities")
    target_group = fields.Selection(selection=TARGET_GROUP_SELECT, string='Target Group', required=True, copy=False, default='others')
    occupation_area = fields.Selection(selection=OCCUPATION_SELECT, string='Occupation Area', required=True, copy=False, default='others')
    intervention_area = fields.Selection(selection=INTERVENTION_SELECT, string='Intervention Area', required=True, copy=False, default='others')
    description = fields.Text(string = "Description")
    postal_code = fields.Char(string = "Postal Code")
    beggining_date = fields.Date(string = "Beggining Date")
    finishing_date = fields.Date(string = "Finishing Date")
    job_id = fields.Many2one(comodel_name = 'hr.job', string = 'Job')
    parish_id = fields.Many2one(comodel_name = 'res.parish', string = 'Parish')
    """
        * Volunteer requirements
    """
    age = fields.Selection(selection=AGE_SELECT, string='Age', required=True, copy=False, default='all')
    idiom = fields.Selection(selection=IDIOM_SELECT, string='Idiom', required=True, copy=False, default='not_required')
    occupation = fields.Selection(selection=OCCUPATION_SELECT, string='occupation', required=True, copy=False, default='not_required')
    volunteer_license = fields.Selection(selection=LICENSE_SELECT, string='Volunteer License', copy=False, default='not_required')
    literary_abilities = fields.Selection(selection=LITERARY_SELECT, string='Literary Abilities', required=True, copy=False, default='ignored')
    entity_name = fields.Char(string="Entity Name")
    extra_requirements = fields.Char(string="Extra Requirements")
    activity_image = fields.Image("Activity photo", max_width=420, max_height=420, store=True)
    # volunteer_town = fields.Selection([
    #     ('all','All'),
    #     ('sao_jacinto', 'São Jacinto'),
    #     ('gloria_vera_cruz', 'Glória e Vera Cruz'),
    #     ('cacia', 'Cacia'),
    #     ('esgueira', 'Esgueira'),
    #     ('santa_joana', 'Santa Joana'),
    #     ('sao_bernardo', 'São Bernardo'),
    #     ('aradas', 'Aradas'),
    #     ('eixo_eirol', 'Eixo e Eirol'),
    #     ('oliveirinha', 'Oliveirinha'),
    #     ('requeixo', 'Requeixo')], 
    #     string='Volunteer Town', required=True, copy=False, default='all')

    def set_submitted(self):
        for record in self:
            record.state = 'submit'
    
    def set_approved(self):
        for record in self:
            record.state = 'approve'

    def set_rejected(self):
        for record in self:
            record.state = 'reject'
    
    def action_show_created_job(self):
        job_id = self.job_id
        action_id = self.env.ref('hr.action_hr_job')
        return {
            'name': action_id.name,
            'help': action_id.help,
            'type': action_id.type,
            'view_mode': 'kanban,tree,form',
            'target': action_id.target,
            'res_id': job_id.id,
            'domain':  [('id', '=', job_id.id)],
            'res_model': action_id.res_model,
        }

    def create_job_from_new_job(self):
        for record in self:
            HrJob = self.env['hr.job'].sudo()
            values = {
                'name': record.name,
                'company_id': self.env.user.company_id.id,
                'town': record.town,
                'location': record.location,
                'responsibilities': record.responsibilities,
                'target_group': record.target_group,
                'occupation_area': record.occupation_area,
                'intervention_area': record.intervention_area,
                'description': record.description,
                'postal_code': record.postal_code,
                'beggining_date': record.beggining_date,
                'finishing_date': record.finishing_date,
                'age': record.age,
                'idiom': record.idiom,
                'occupation': record.occupation,
                'volunteer_license': record.volunteer_license,
                'literary_abilities': record.literary_abilities,
                'entity_name': record.entity_name,
                'extra_requirements': record.extra_requirements,
                'activity_image': record.activity_image,
                'parish_id': record.parish_id.id if record.parish_id else False
            }
            job_id = HrJob.create(values)
            record.job_id = job_id.id
