# -*- coding: utf-8 -*-

from odoo import models, fields, tools, api, _, SUPERUSER_ID
from odoo.exceptions import ValidationError, Warning, UserError


class FleetVehicleModel(models.Model):
    _inherit = 'fleet.vehicle.model'


    MODEL_RESOURCE_TYPE = [
        ('light', 'Light'),
        ('heavy', 'Heavy'),
        ('tcc', 'TCC'),
        ('bed', 'Bed'),
        ('mobility', 'Mobility'),
        ('textile', 'Textile'),
        ('storage', 'Storage'),
        ('kitchen', 'Kitchen'),
        ('home_appliance', 'Home Appliance'),
        ('computer', 'Computer'),
        ('training', 'Training'),
        ('info_session', 'Info. Session'),
        ('room', 'Room'),
        ('furniture', 'Furniture'),
        ('human_resource', 'Human Resource'),
        ('cleaning', 'Cleaning'),
        ('maintenaince', 'Maintenaince'),
    ]


    model_resource_type = fields.Selection(selection=MODEL_RESOURCE_TYPE, string='Model Resource Type')
    brand_resource_type = fields.Selection(related="brand_id.resource_type")

    def name_get(self):
        result = []
        for record in self:
            result.append((record.sudo().id, '{0}'.format(record.sudo().name)))
            record.sudo().name = '{0}'.format(record.sudo().name)
        return result

class FleetVehicleModelBrand(models.Model):
    _inherit = 'fleet.vehicle.model.brand'
    

    RESOURCE_TYPE_SELECTION = [
        ('vehicle','Vehicle'),
        ('vehicle_with_driver','Vehicle with Driver'),
        ('institution','Institution'),
        ('installation','Installation'),
        ('training','Training'),
        ('human_resource','Human Resource'),
        ('tecnical_help','Tecnical Help'),
        ('home_supply','Home Supply'),
        ('equipment','Equipment'),
        ('furniture','Furniture'),
        ('non_specialized_service','Non Specialized Service')
    ]


    model_ids = fields.One2many(comodel_name = 'fleet.vehicle.model', inverse_name = 'brand_id', string = 'Models')
    resource_type = fields.Selection(selection=RESOURCE_TYPE_SELECTION, string='Type')