from odoo import models, fields, tools, api, _, SUPERUSER_ID
from odoo.exceptions import ValidationError, Warning, UserError

class ResourceSocialProgram(models.Model):
    _name = 'resource.social.program'
    _description = 'Social Program'

    # ! Temporary Fields
    #Going to be a foreign key in the future
    promoting_entity = fields.Char(string = "Promoting Entity")
    beneficiary_entity = fields.Char(string = "Beneficiary Entity")

    program_id = fields.Char(string = "Social Program id")
    name = fields.Char(string = "Project Designation")
    description = fields.Char(string = "Project Description")
    email = fields.Char(string = "Email")
    website = fields.Char(string = "Website")
    contact = fields.Integer(string = "Contact")
    social_media = fields.Char(string = "Social Media")
    duration = fields.Integer(string = "Duration")
    target_audience = fields.Char(string = "Target Audience")
    financial_source = fields.Char(string = "Financial Source")
    program_image = fields.Binary(string = "Program Image")







