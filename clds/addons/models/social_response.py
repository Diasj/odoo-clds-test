from odoo import models, fields, tools, api, _, SUPERUSER_ID
from odoo.exceptions import ValidationError, Warning, UserError
from odoo.http import request

class ResourceSocialResponse(models.Model):
    _name = 'resource.social.response'
    _description = 'Social Response'

    # * MAIN CATEGORY
    CATEGORY_SELECTION = [
        ('children_youth', 'Children and Youth'),
        ('children_youth_dis', 'Children and Youth with Disabilities'),
        ('children_youth_danger', 'Children and Youth in Danger'),
        ('old', 'Old people'),
        ('dis_adult', 'Disabled Adult People'),
        ('dependent', 'Dependent People'),
        ('homeless', 'Homeless People'),
        ('family_community', 'Family and Community'),
        ('family_aids', 'People with HIV / AIDS and their families'),
        ('addicted', 'Addicted People'),
        ('violence', 'People Victims of Violence'),
        ('one_time', 'One-Time Answers'),
    ]

    # * SUB-CATEGORIES --------------------------------------------
    CHILDREN_YOUTH_SELECTION = [
        ('babysitter', 'Babysitter'),
        ('family_day_care', 'Family Day Care'),
        ('nursery', 'Nursery'),
        ('eepe', 'Pre-School Education Establishment (EEPE)'),
        ('catl', 'Leisure Activities Center (CATL)')
    ]

    CHILDREN_YOUTH_DIS_SELECTION = [
        ('ipi', 'Early Childhood Intervention (IPI)'),
        ('support_home', 'Support Home'),
        ('dis_transport', 'Transport of People with Disabilities')
    ]

    CHILDREN_YOUTH_DANGER_SELECTION = [
        ('cafap', 'Family Support and Parental Counseling Center (CAFAP)'),
        ('street_youth_support', 'Street Children and Youth Support Team'),
        ('family_reception', 'Family Reception'),
        ('residential_home', 'Residential Home'),
        ('autonomy_apartment', 'Autonomy Apartment')
    ]

    OLD_PEOPLE_SELECTION = [
        ('sad', 'Home Support Service (SAD)'),
        ('social_center', 'Social Center'),
        ('day_care', 'Day Care'),
        ('night_center', 'Night Center'),
        ('care_for_elderly', 'Family Care for the Elderly'),
        ('erpi', 'Residential Structure for the Elderly (ERPI)')
    ]

    DISABLED_ADULT_SELECTION = [
        ('service_center_dis', 'Service Center / Monitoring and Entertainment for People with Disabilities'),
        ('service_center_rehab', 'Service Center, Monitoring and Social Rehabilitation'),
        ('sad', 'Home Support Service (SAD)'),
        ('cao', 'Occupational Activities Center (CAO)'),
        ('reception_dis', 'Family Reception for Disabled Adults'),
        ('residential_home', 'Residential Home'),
        ('autonomous_residence', 'Autonomous Residence'),
        ('transport_dis', 'Transport of People with Disabilities')
    ]

    DEPENDENT_SELECTION = [
        ('sad', 'Home Support Service (SAD)'),
        ('adi', 'Integrated Home Support (ADI)'),
        ('uai', 'Integrated Support Unit (UAI)'),
        ('supported_life', 'Supported Life Unit')
    ]

    HOMELESS_SELECTION = [
        ('street_team', 'Street Team For Homeless People'),
        ('occupational_workshop', 'Occupational Workshop')
    ]

    FAMILY_COMMUNITY_SELECTION = [
        ('saas', 'Customer Service and Social Monitoring (SAAS)'),
        ('self_help', 'Self-Help Group'),
        ('community_center', 'Community Center'),
        ('vacation_center', 'Vacation and Leisure Center'),
        ('caf_canteen', 'Social Cafeteria / Canteen'),
        ('life_support', 'Life Support Center'),
        ('insert_community', 'Insertion Community'),
        ('cat', 'Temporary Accommodation Center (CAT)'),
        ('food_aid', 'Food Aid')
    ]

    FAMILY_AIDS_SELECTION = [
        ('caap', 'Psychosocial Service / Monitoring Center (CAAP)'),
        ('sad', 'Home Support Service (SAD)'),
        ('residence_infected', 'Residence for People Infected with HIV / AIDS')
    ]

    ADDICTED_SELECTION = [
        ('direct_team', 'Direct Intervention Team'),
        ('reintegration_program', 'Social Reintegration Apartment')
    ]

    VIOLENCE_SELECTION = [
        ('call_center', 'Call Center'),
        ('shelter_house', 'Shelter House')
    ]

    ONE_TIME_SELECTION = [
        ('children_support', 'Home Support For Childcare'),
        ('ambulatory_care', 'Ambulatory Care Support'),
        ('braille_press', 'Braille Press'),
        ('school_dogs', 'School of Guide Dogs'),
        ('cri', 'Resource Centers for Inclusion (CRI)')
    ]

    # * Main Category
    category = fields.Selection(selection=CATEGORY_SELECTION, string='Main Category', required=True, copy=False, default='children_youth')

    # * Sub-Categories
    sub_children_youth = fields.Selection(selection=CHILDREN_YOUTH_SELECTION, string='Children and Youth Sub-Category', required=False, copy=False)
    sub_children_youth_dis = fields.Selection(selection=CHILDREN_YOUTH_DIS_SELECTION, string='Children and Youth with Disabilities Sub-Category', required=False, copy=False)
    sub_children_youth_danger = fields.Selection(selection=CHILDREN_YOUTH_DANGER_SELECTION, string='Children and Youth in Danger Sub-Category', required=False, copy=False)
    sub_old = fields.Selection(selection=OLD_PEOPLE_SELECTION, string='Old people Sub-Category', required=False, copy=False)
    sub_dis_adult = fields.Selection(selection=DISABLED_ADULT_SELECTION, string='Disabled Adult People Sub-Category', required=False, copy=False)
    sub_dependent = fields.Selection(selection=DEPENDENT_SELECTION, string='Dependent People Sub-Category', required=False, copy=False)
    sub_homeless = fields.Selection(selection=HOMELESS_SELECTION, string='Homeless Sub-Category', required=False, copy=False)
    sub_family_community = fields.Selection(selection=FAMILY_COMMUNITY_SELECTION, string='Family and Community Sub-Category', required=False, copy=False)
    sub_family_aids = fields.Selection(selection=FAMILY_AIDS_SELECTION, string='People with HIV / AIDS and their families Sub-Category', required=False, copy=False)
    sub_addicted = fields.Selection(selection=ADDICTED_SELECTION, string='Addicted People Sub-Category', required=False, copy=False)
    sub_violence = fields.Selection(selection=VIOLENCE_SELECTION, string='People Victims of Violence Sub-Category', required=False, copy=False)
    sub_one_time = fields.Selection(selection=ONE_TIME_SELECTION, string='One-Time Answers Sub-Category', required=False, copy=False)

    name = fields.Char(string="Resource Name")
    capacity = fields.Integer(string="Capacity")
    reimbursed_vacancies = fields.Integer(string="Reimbursed Vacancies")
    non_reimbursed_vacancies = fields.Integer(string="Non Reimbursed Vacancies")
    
    company_id = fields.Many2one('res.company', string='Company', default=lambda self: self.env.company)
    parish_id = fields.Many2one(comodel_name = 'res.parish', string = 'Parish')
    street = fields.Char(related='company_id.street', string="Street")
    postal_code = fields.Char(related='company_id.zip', string="ZIP Code")
    email = fields.Char(related='company_id.email', string="Email")
    legal_nature = fields.Char(related='company_id.legal_nature', string="Legal Nature")
    contact = fields.Char(related='company_id.phone', string='Contact')
    facebook = fields.Char(related='company_id.facebook', string="Facebook")
    instagram = fields.Char(related='company_id.instagram', string="Instagram")
    website = fields.Char(related='company_id.website', string="Website")

    def get_extra_info(company_id_value=None, sub_category_value=""):
        print("Company ID: ", company_id_value)
        print("SubCategory: ",sub_category_value)
        count = 0
        html = "<span class='hidden_table'>"
        html = ""
        content = request.env['resource.social.response'].sudo().search([('company_id','=', int(company_id_value.id))])

        for i in content:
            if (i.sub_children_youth == sub_category_value or i.sub_children_youth_dis == sub_category_value or 
                i.sub_children_youth_danger == sub_category_value or i.sub_old == sub_category_value or
                i.sub_dis_adult == sub_category_value or i.sub_dependent == sub_category_value or
                i.sub_homeless == sub_category_value or i.sub_family_community == sub_category_value or
                i.sub_family_aids == sub_category_value or i.sub_addicted == sub_category_value or
                i.sub_violence == sub_category_value or i.sub_one_time == sub_category_value):
                if count == 0:
                    html += "<tr style='background-color: #DADADA; color: #343a40;'><th scope='row' style='background-color: #434B53; color: white;'>" + str(i.name) + "</th><td></td>"
                    html += "<td>" + str(i.capacity) + "</td>"
                    html += "<td>" + str(i.reimbursed_vacancies) + "</td>"
                    html += "<td>" + str(i.non_reimbursed_vacancies) + "</td></tr>"
                    street = "<tr><th scope='row'>Morada</th><td colspan='4' scope='colgroup'>" + str(i.street) + "</td></tr>"
                    postal_code = "<tr><th scope='row'>Código Postal</th><td colspan='4' scope='colgroup'>" + str(i.postal_code) + "</td></tr>"
                    contact = "<tr><th scope='row'>Contacto</th><td colspan='4' scope='colgroup'>" + str(i.contact) + "</td></tr>"
                    email = "<tr><th scope='row'>Email</th><td colspan='4' scope='colgroup'>" + str(i.email) + "</td></tr>"
                    website = "<tr><th scope='row'>Website</th><td colspan='4' scope='colgroup'>" + str(i.website) + "</td></tr>"
                    facebook = "<tr><th scope='row'>Facebook</th><td colspan='4' scope='colgroup'>" + str(i.facebook) + "</td></tr>"
                    instagram = "<tr><th scope='row'>Instagram</th><td colspan='4' scope='colgroup'>" + str(i.instagram) + "</td></tr>"
                    legal_nature = "<tr><th scope='row'>Natureza Jurídica</th><td colspan='4' scope='colgroup'>" + str(i.legal_nature) + "</td></tr>"
                    count += 1
                else:
                    html += "<tr style='background-color: #DADADA; color: #343a40;'><th scope='row' style='background-color: #434B53; color: white;'>" + str(i.name) + "</th><td></td>"
                    html += "<td>" + str(i.capacity) + "</td>"
                    html += "<td>" + str(i.reimbursed_vacancies) + "</td>"
                    html += "<td>" + str(i.non_reimbursed_vacancies) + "</td></tr>"

        html += street + postal_code + contact + email + website + facebook + instagram + legal_nature
        return html



