# -*- coding: utf-8 -*-

from . import ir_http
from . import ir_attachment
from . import res_users
from . import res_company
from . import res_parish
from . import interest
from . import target_audience
from . import res_partner
from . import resource
from . import event
from . import fleet_vehicle
from . import fleet_vehicle_cost
from . import fleet_vehicle_models
from . import social_programs
from . import social_response
from . import appeal
from . import event
from . import hr_job
from . import hr_recruitment
from . import website
