# -*- coding: utf-8 -*-

import werkzeug.urls

from odoo import api, tools, fields, models, SUPERUSER_ID, _
from openerp.exceptions import except_orm, Warning, RedirectWarning, UserError, ValidationError
from datetime import date




class ResPartner(models.Model):
    _inherit = "res.partner"
    

    company_type = fields.Selection(store = True)
    is_volunteer = fields.Boolean(string = 'Volunteer', default=False)
    parish_id = fields.Many2one(comodel_name = 'res.parish', string = 'Parish')
    interest_ids = fields.Many2many(comodel_name='interest', column1='partner_id', column2='interest_id', string='Interests')
    target_audience_ids = fields.Many2many(comodel_name='target.audience', column1='partner_id', column2='target_audience_id', string='Target Audience')
    cc_card = fields.Char(string = 'CC Card')


    def add_partner_supplier(self):
        for record in self:
            record.sudo().write({
                'supplier_rank': 1,
            })

    def remove_partner_supplier(self):
        for record in self:
            record.sudo().write({
                'supplier_rank': 0,
            })
    
    def add_partner_volunteer(self):
        for record in self:
            record.sudo().write({
                'is_volunteer': True,
            })

    def remove_partner_volunteer(self):
        for record in self:
            record.sudo().write({
                'is_volunteer': False,
            })

    def _get_signup_url_for_action(self, url=None, action=None, view_type=None, menu_id=None, res_id=None, model=None):
        """ generate a signup url for the given partner ids and action, possibly overriding
            the url state components (menu_id, id, view_type) """
        res = dict.fromkeys(self.ids, False)
        for partner in self:
            base_url = partner.get_base_url()
            # when required, make sure the partner has a valid signup token
            if self.env.context.get('signup_valid') and not partner.user_ids:
                partner.sudo().signup_prepare()

            route = 'login'
            # the parameters to encode for the query
            query = dict(db=self.env.cr.dbname)
            signup_type = self.env.context.get('signup_force_type_in_url', partner.sudo().signup_type or '')
            if signup_type:
                route = 'reset_password' if signup_type == 'reset' else signup_type

            if partner.sudo().signup_token and signup_type:
                query['token'] = partner.sudo().signup_token
            elif partner.user_ids:
                query['login'] = partner.user_ids[0].login
            else:
                continue        # no signup token, no user, thus no signup url!

            if url:
                query['redirect'] = url
            else:
                fragment = dict()
                base = '/web#'
                if action == '/mail/view':
                    base = '/mail/view?'
                elif action:
                    fragment['action'] = action
                if view_type:
                    fragment['view_type'] = view_type
                if menu_id:
                    fragment['menu_id'] = menu_id
                if model:
                    fragment['model'] = model
                if res_id:
                    fragment['res_id'] = res_id

                if fragment:
                    query['redirect'] = base + werkzeug.urls.url_encode(fragment)

            url = "/web/{0}?{1}&request=reset_request".format(
                route,
                werkzeug.urls.url_encode(query)
            )

            if not self.env.context.get('relative_url'):
                url = werkzeug.urls.url_join(base_url, url)
            res[partner.id] = url

        return res