from odoo import models, fields, tools, api, _, SUPERUSER_ID
from odoo.exceptions import ValidationError, Warning, UserError

class Country(models.Model):
    _name = 'res.parish'
    _description = 'Parish'
    _order = 'name'


    name = fields.Char(string='Parish Name', translate=True, help='The full name of the parish.')
    country_id = fields.Many2one(comodel_name = 'res.country', string = 'Country')