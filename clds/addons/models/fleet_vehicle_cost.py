# -*- coding: utf-8 -*-

from odoo import models, fields, tools, api, _, SUPERUSER_ID
from odoo.exceptions import ValidationError, Warning, UserError


class FleetVehicleCost(models.Model):
    _inherit = 'fleet.vehicle.cost'
    
    
    def _get_odometer(self):
        self.odometer = 0.0
        for record in self:
            record.odometer = False
            if record.odometer_id:
                record.odometer = record.odometer_id.value
    
    def _set_odometer(self):
        for record in self:
            if not record.odometer:
                raise UserError(_('Emptying the odometer value of a vehicle is not allowed.'))

            odometer = self.env['fleet.vehicle.odometer'].create({
                'value': record.odometer,
                'date': record.date or fields.Date.context_today(record),
                'vehicle_id': record.vehicle_id.id,
                # 'resource_reservation_id': record.resource_reservation_id.id,
                'origin': record.origin,
                'destination': record.destination,
                'start_odometer': record.start_odometer,
                'clds_purchaser_id': record.clds_purchaser_id.id,
            })
            self.odometer_id = odometer
    
    def _get_resource_domain(self):
        if self.env.user.has_group('clds.group_clds_admin'):
            domain = [(1,'=',1)]
        else:
            domain = [('company_id','=',self.env.user.company_id.id)]
        return domain
    
    company_id = fields.Many2one(comodel_name='res.company', string='Owner', related='vehicle_id.company_id', store=True)
    vehicle_id = fields.Many2one(comodel_name='fleet.vehicle', domain=lambda self: self._get_resource_domain())
    origin = fields.Char(string='Origin', size=64)
    destination = fields.Char(string='Destination', size=64)
    # resource_reservation_id = fields.Many2one('resource.reservation', 'Reservation')
    clds_purchaser_id = fields.Many2one(comodel_name='res.users', string='Purchaser', default=lambda self: self.env.user.id, readonly=True)
    start_odometer = fields.Char(string='Start Odometer', size=10)
    odometer = fields.Float(compute='_get_odometer', inverse='_set_odometer', string='Odometer Value', help='Odometer measure of the vehicle at the moment of this log')


    def search(self, args, offset=0, limit=None, order=None, count=False):
        context = self._context or {}
        if self.env.user.has_group('clds.group_clds_admin'): # sees all records
            args += [('company_id', 'in', self.env.user.company_ids.ids)]
        else: # sees records related to own company
            args += [('company_id', '=', self.env.user.company_id.id)]
           
        return super(FleetVehicleCost, self).search(args, offset, limit, order, count=count)


class FleetVehicleLogContract(models.Model):
    _inherit = "fleet.vehicle.log.contract"

    
    company_id = fields.Many2one(comodel_name='res.company', string='Owner', related='vehicle_id.company_id', store=True)
    resource_type = fields.Selection(related='vehicle_id.model_id.brand_id.resource_type', store=True)
    clds_purchaser_id = fields.Many2one(comodel_name='res.users', string='Purchaser', default=lambda self: self.env.user.id, readonly=True)


    def search(self, args, offset=0, limit=None, order=None, count=False):
        context = self._context or {}
        if self.env.user.has_group('clds.group_clds_admin'): # sees all records
            args += [('company_id', 'in', self.env.user.company_ids.ids)]
        else: # sees records related to own company
            args += [('company_id', '=', self.env.user.company_id.id)]
           
        return super(FleetVehicleLogContract, self).search(args, offset, limit, order, count=count)


class FleetVehicleLogServices(models.Model):
    _inherit = "fleet.vehicle.log.services"
    

    company_id = fields.Many2one(comodel_name='res.company', string='Owner', related='vehicle_id.company_id', store=True)
    resource_type = fields.Selection(related='vehicle_id.model_id.brand_id.resource_type', store=True)
    clds_purchaser_id = fields.Many2one(comodel_name='res.users', string='Purchaser', default=lambda self: self.env.user.id)


    def search(self, args, offset=0, limit=None, order=None, count=False):
        context = self._context or {}
        if self.env.user.has_group('clds.group_clds_admin'): # sees all records
            args += [('company_id', 'in', self.env.user.company_ids.ids)]
        else: # sees records related to own company
            args += [('company_id', '=', self.env.user.company_id.id)]
           
        return super(FleetVehicleLogServices, self).search(args, offset, limit, order, count=count)


class FleetVehicleLogFuel(models.Model):
    _inherit = "fleet.vehicle.log.fuel"

    
    UNITS = [
        ('kilometers', 'Kilometers'),
        ('miles', 'Miles')
    ]


    company_id = fields.Many2one(comodel_name='res.company', string='Owner', related='vehicle_id.company_id', store=True)
    resource_type = fields.Selection(related='vehicle_id.model_id.brand_id.resource_type', store=True)
    odometer_unit_start = fields.Selection(selection=UNITS, related='vehicle_id.odometer_unit', string="Unit", store=True, readonly=True)
    clds_purchaser_id = fields.Many2one(comodel_name='res.users', string='Purchaser', default=lambda self: self.env.user.id, readonly=True)
    

    def search(self, args, offset=0, limit=None, order=None, count=False):
        context = self._context or {}
        if self.env.user.has_group('clds.group_clds_admin'): # sees all records
            args += [('company_id', 'in', self.env.user.company_ids.ids)]
        else: # sees records related to own company
            args += [('company_id', '=', self.env.user.company_id.id)]
           
        return super(FleetVehicleLogFuel, self).search(args, offset, limit, order, count=count)
