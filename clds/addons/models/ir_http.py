import os

from odoo import models, fields, tools, api, _, SUPERUSER_ID
from odoo.exceptions import ValidationError, Warning, UserError
from odoo.http import request
from odoo.addons.http_routing.models.ir_http import slug, _guess_mimetype


class Http(models.AbstractModel):
    _inherit = 'ir.http'

    @classmethod
    def _serve_page(cls, values=False):
        req_page = request.httprequest.path
        page_domain = [('url', '=', req_page)] + request.website.website_domain()

        published_domain = page_domain
        page = request.env['website.page'].sudo().search(published_domain, order='website_id asc', limit=1)
        if page and (request.website.is_publisher() or page.is_visible):
            _, ext = os.path.splitext(req_page)
            
            render_values = {
                'deletable': True,
                'main_object': page,
            }

            if values:
                website_key = values.get('website_key', False)
                if website_key and website_key == 'clds.clds_homepage_aveiro_em_rede':
                    render_values.update({
                        'resource_number': values.get('resource_number'),
                        'entity_number' : values.get('entity_number'),
                        'resource_ids' : values.get('resource_ids')
                    })
                elif website_key and website_key == 'clds.clds_homepage_aveiro_voluntario':
                    pass
                    # render_values.update({
                    #     'resource_number': values.get('resource_number'),
                    #     'entity_number' : values.get('entity_number')
                    # })
                
            return request.render(page.get_view_identifier(), render_values, mimetype=_guess_mimetype(ext))
        return False