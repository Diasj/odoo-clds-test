from odoo import models, fields, tools, api, _, SUPERUSER_ID
from odoo.exceptions import ValidationError, Warning, UserError

class Applicant(models.Model):
    _inherit = "hr.applicant"


    def create_volunteer_from_applicant(self):
        """ Create a re.partner (volunteer) from the hr.applicants """
        for applicant in self:
            if not applicant.partner_name:
                raise UserError(_('You must define a Contact Name for this applicant.'))
            if not applicant.email_from:
                raise UserError(_('You must define an Email for this applicant.'))

            ResPartner = self.env['res.partner'].sudo()
            partner_id = ResPartner.search([
                ('email','=',applicant.email_from)
            ])

            if partner_id:
                applicant.partner_id = partner_id
                applicant.is_volunteer = True
            else:
                new_partner_id = self.env['res.partner'].create({
                    'is_company': False,
                    'name': applicant.partner_name,
                    'email': applicant.email_from,
                    'phone': applicant.partner_phone,
                    'mobile': applicant.partner_mobile,
                    'is_volunteer': True
                })

                applicant.partner_id = new_partner_id