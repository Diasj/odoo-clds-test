# -*- coding: utf-8 -*-

from odoo import api, tools, fields, models, SUPERUSER_ID, _
from openerp.exceptions import except_orm, Warning, RedirectWarning, UserError, ValidationError
from datetime import date


class ResUsers(models.Model):
    _inherit = "res.users"


    opt_out = fields.Boolean(string = 'Opt Out from emails', default=False)
