from odoo import models, fields, tools, api, _, SUPERUSER_ID
from odoo.exceptions import ValidationError, Warning, UserError

class Appeal(models.Model):
    _name = 'appeal'
    _description = 'Appeal'


    APPEAL_CATEG = [
        ('electrical_appliance', 'Electrical Appliance'),
        ('technology', 'Technology'), 
        ('clothes', 'Clothes'), 
        ('transport', 'Transport'), 
        ('essential', 'health/hygiene essentials')
    ]

    STATE = [
        ('active', 'Active'), 
        ('closed', 'Closed')
    ]

    name = fields.Char(string = "Appeal Designation", required=True)
    
    # ! Future foreign key
    entity = fields.Char(string = "Entidade", required=True)
    context = fields.Text(string = "Appeal Context", required=True)
    appeal_assists_ids = fields.One2many(comodel_name="appeal.assists", inverse_name='appeal_id', string="Appeal Assists", index=True)

    # ? Susceptible to change
    category = fields.Selection(selection=APPEAL_CATEG, string='Category', required=True, copy=False, default='electrical_appliance') #Instead of having an icon uploaded by the user
    state = fields.Selection(selection=STATE, string='Status', required=True, copy=False, default='active')

    def set_closed(self):
        for record in self:
            record.state = 'closed'
            

class AppealAssist(models.Model):
    _name = 'appeal.assists'
    _description = 'Appeal Assists'


    TRANSPORT_REQUEST = [
        ('yes','Yes'),
        ('no','No')
    ]
    
    RECEIPT = [
        ('yes','Yes'),
        ('no','No')
    ]


    name = fields.Char(string = "Name", required = True)
    nif = fields.Integer(string = "NIF", required = True)
    contact = fields.Integer(string = "Contact", required = True)
    address = fields.Char(string = "Address", required = True)
    email = fields.Char(string = "Email", required = True)
    appeal_id = fields.Many2one(comodel_name="appeal", string="Appeal", index=True)

    transport_request = fields.Selection(selection=TRANSPORT_REQUEST, string = "Transport Request", copy = False, default = 'no')
    receipt = fields.Selection(selection=RECEIPT, string = "Receipt", copy = False, default = 'yes')
    product_image = fields.Image(string="Product photo", max_width=128, max_height=128, store=True)
    observations = fields.Char(string="Observations", required = False)