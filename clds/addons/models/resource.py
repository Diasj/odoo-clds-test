# -*- coding: utf-8 -*-

from odoo import api, tools, fields, models, SUPERUSER_ID, _
from odoo.tools import DEFAULT_SERVER_DATETIME_FORMAT
from odoo.exceptions import ValidationError, Warning, UserError
import random
import datetime
import werkzeug.urls

class ResourceReservation(models.Model):
    _name = 'resource.reservation'
    _inherit = ['mail.thread', 'mail.activity.mixin']
    _description = u"Resource Reservation"


    STATE_SELECTION = [
        ('new', 'New'),
        ('rejected', 'Rejected'),
        ('submitted', 'Submitted'),
        ('approved', 'Approved'),
        ('delivered', 'Delivered'),
        ('received', 'Received'),
        ('returned', 'Returned'),
        ('accepted', 'Accepted'),
        ('canceled', 'Canceled'),
    ]

    def get_signup_url(self):
        self.ensure_one()
        base_url = self.env['ir.config_parameter'].sudo().get_param('web.base.url')
        fragment =  "/web/?db={db}s#view_type={view_type}&id={id}&type={type}&model={model}".format(
            db=self.env.cr.dbname,
            view_type='form',
            id=self.id,
            type='signup',
            model='resource.reservation',
        )
        return werkzeug.urls.url_join(base_url, fragment)

    def disable_buttons(self):
        for record in self:
            current_user_company_id = record.env.user.company_id
            requestee_user_company_id = record.requestee_company_id
            requester_user_company_id = record.user_id.company_id

            is_clds_admin = record.env.user.has_group('clds.group_clds_admin')
            is_manager = record.env.user.has_group('clds.group_clds_manager')
            
            record.cross_company_request = False
            record.same_company_request = False
            record.current_user_is_requester = False

            bool_for_cross_company_requests = (((current_user_company_id != requestee_user_company_id) or (current_user_company_id == requester_user_company_id)) and (requester_user_company_id != requestee_user_company_id))
            bool_same_company_requests = ((current_user_company_id == requestee_user_company_id) and (current_user_company_id == requester_user_company_id) and (requester_user_company_id == requestee_user_company_id))
            bool_check_user_is_requestor = (current_user_company_id == requester_user_company_id)

            if (record.env.user.id == SUPERUSER_ID) or is_clds_admin or is_manager:
                if (record.env.user.id == SUPERUSER_ID) or is_clds_admin: #! validation for admin
                    record.same_company_request = True
                    record.cross_company_request = False
                    record.current_user_is_requester = True
                elif is_manager: #! validation for manager
                    record.same_company_request = bool_same_company_requests 
                    record.cross_company_request = bool_for_cross_company_requests
                    record.current_user_is_requester = True
            else: #! validation for user
                record.same_company_request = bool_same_company_requests 
                record.cross_company_request = bool_for_cross_company_requests
                record.current_user_is_requester = bool_check_user_is_requestor


    name = fields.Char(string='Name', size=40, tracking=True, readonly = True, default='New Reservation')
    origin = fields.Char(string='Origin', size=64, tracking=True)
    inv_ref = fields.Char(string='Invoice Reference', size=64, tracking=True)
    signup_url = fields.Char(compute="get_signup_url")
    destination = fields.Char(string='Destination', size=64, tracking=True)
    state = fields.Selection(selection=STATE_SELECTION, string='State', readonly=True, index=True, tracking=True, default='new')
    resource_type = fields.Selection(related='resource_id.model_id.brand_id.resource_type', store=True)
    odometer_unit = fields.Selection(related='resource_id.odometer_unit', string="Unit", readonly=True, default='kilometers')
    date_fuel = fields.Date(string='Date',help='Date when the cost has been executed', tracking=True)
    date_stop = fields.Datetime(string='Date Stop', required=True, tracking=True)
    date_start = fields.Datetime(string='Date Start', required=True, tracking=True, default=fields.datetime.now())
    user_id = fields.Many2one(comodel_name='res.users', string='Requester', required=True, default=lambda self: self.env.uid)
    model_id = fields.Many2one(comodel_name='fleet.vehicle.model', string='Resource Model', required=True, tracking=True)
    vendor_id = fields.Many2one(comodel_name='res.partner', string='Supplier', domain="[('supplier_rank', '>=', 1)]", tracking=True)
    resource_id = fields.Many2one(comodel_name='fleet.vehicle', string='Resource', required = True, states = { 
        'approved': [('required', True)],
        'received': [('required', True)],
        'returned': [('required', True)],
        'accepted': [('required', True)]
    }, tracking=True)
    clds_purchaser_id = fields.Many2one(comodel_name='res.users', string='Purchaser', tracking=True, default=lambda self: self.env.uid)
    requestee_company_id = fields.Many2one(comodel_name='res.company', string='Requestee', related='resource_id.company_id', readonly=True)
    requester_company_id = fields.Many2one(comodel_name='res.company', string='Requestor', default=lambda self: self.env.user.company_id)
    liter = fields.Float(string='Liter', tracking=True)
    amount = fields.Float(string='Total Price', tracking=True)
    start_odometer = fields.Float(string='Start Odometer', size=10, tracking=True, readonly = True)
    final_odometer = fields.Float(string='Final Odometer', size=10, tracking=True)
    odometer_unit_final = fields.Float(related='resource_id.odometer', string="Unit", readonly=True)
    log_fuel = fields.Boolean(string='Refuel?', tracking=True)
    odometer_checked = fields.Boolean(string = 'Verifies if the users checked the odometer', default = False)
    same_company_request = fields.Boolean(compute='disable_buttons', string = 'Same Company Request', default = False)
    cross_company_request = fields.Boolean(compute='disable_buttons', string = 'Cross Company Request', default = False)
    current_user_is_requester = fields.Boolean(compute='disable_buttons', default = False)
    comments = fields.Text(string='Comments', tracking=True)
    odometer_id = fields.One2many(comodel_name='fleet.vehicle.odometer', inverse_name='resource_reservation_id', string='Odometer')
    company_user_ids = fields.Many2many(comodel_name='res.users', string='User in Company', related='requester_company_id.user_ids' )


    def send_mail(self, template, send_to_managers=False):
        template_id = self.env.ref(template)

        if template_id:
            email_ctx = self.env.context.copy()
            email_ctx['resource_reservation_state'] = dict(self.STATE_SELECTION).get(self.state)
            if send_to_managers:
                #! Get manager Group
                group_id = self.env.ref('clds.group_clds_manager')
                #! Get all users from manager Group
                users_managers_id = group_id.users

                email_list = list()
                for manager in users_managers_id:
                    if manager.active and not manager.opt_out:
                        if manager.email:
                            email_list.append(manager.email)
                        
                email_to = (', ').join(email_list)
                if email_to:
                    template_id.email_to = email_to
                    template_id.email_cc = email_to
                    
            self.signup_url = self.get_signup_url()
            template_id.with_context(email_ctx).sudo().send_mail(self.id, force_send=True)

    def get_odometer(self):
        FleetVehicalOdometer = self.env['fleet.vehicle.odometer'].sudo()
        for record in self:
            resource = record.resource_id
            vehicle_odometer = FleetVehicalOdometer.search([('vehicle_id', '=', resource.id)], limit=1, order='value desc')
            if vehicle_odometer:
                return vehicle_odometer.value
            else:
                return 0
    
    def get_price_per_liter_amount(self, liter, amount):
        if amount <= 0:
            raise Warning(_("Amount cannot be negative and needs to be bigger than zero."))
        if liter <= 0:
            raise Warning(_("Liters cannot be negative and needs to be bigger than zero."))

        liter = float(liter)
        amount = float(amount)
        return round(amount / liter, 2)

    def set_new(self):
        for record in self:
            record.state = 'new'
    
    def set_submitted(self):
        for record in self:
            record.state = 'submitted'
            record.send_mail(template='clds.email_template_resource_reservation_manager', send_to_managers=True)

    def set_approved(self):
        for record in self:
            if not record.resource_id:
                raise ValidationError(_('You must fill the field Resource'))
            
            record.state = 'approved'
            record.send_mail(template='clds.email_template_resource_reservation_user')

    def set_rejected(self):
        for record in self:
            record.state = 'rejected'
            record.send_mail(template='clds.email_template_resource_reservation_user')

    def set_canceled(self):
        for record in self:
            record.state = 'canceled'
            record.send_mail(template='clds.email_template_resource_reservation_user')
    
    def set_delivered(self):
        for record in self:
            record.state = 'delivered'
            record.send_mail(template='clds.email_template_resource_reservation_user')
    
    def set_received(self):
        for record in self:
            record.state = 'received'
            record.start_odometer = record.get_odometer()
            record.send_mail(template='clds.email_template_resource_reservation_manager', send_to_managers=True)
    
    def set_returned(self):    
        for record in self:
            if record.resource_type in ['vehicle','vehicle_with_driver']:
                if not record.origin:
                    raise Warning(_('You need to set an Origin'))
                if not record.destination:
                    raise Warning(_('You need to set a Destination'))
                if record.final_odometer <= record.start_odometer:
                    raise Warning(_('Odometer value cannot be lower than or equal to the value it started with'))

            record.state = 'returned'
            record.send_mail(template='clds.email_template_resource_reservation_manager', send_to_managers=True)
            
    def set_accepted(self):
        for record in self:
            if record.resource_type in ('vehicle','vehicle_with_driver'):
                date = record.date_stop
                if record.date_fuel:
                    date = record.date_fuel

                vals = {
                    'date': date,
                    'origin': record.origin,
                    'vehicle_id': record.resource_id.id,
                    'destination': record.destination,
                    'start_odometer':record.start_odometer,
                    'resource_reservation_id': record.id,
                }

                if record.log_fuel:
                    log_fuel = self.env['fleet.vehicle.log.fuel']
                    vals.update({
                        'liter': record.liter,
                        'amount': record.amount,
                        'inv_ref': record.inv_ref,
                        'odometer': record.final_odometer,
                        'vendor_id': record.vendor_id.id,
                        'price_per_liter': record.get_price_per_liter_amount(record.liter, record.amount),
                        'clds_purchaser_id': record.clds_purchaser_id.id,
                    })
                    log_fuel.create(vals)
                else:
                    if record.final_odometer:
                        odometer = self.env['fleet.vehicle.odometer']
                        vals.update({
                            'value': record.final_odometer,
                        })
                        odometer.create(vals)
            
            record.state = 'accepted'        
    
    @api.model
    def create(self, values):
        if not values.get('user_id'):
            values.update({
                'user_id': self.env.uid
            })
        
        result = super(ResourceReservation, self).create(values)
        result.name = self.env['ir.sequence'].sudo().next_by_code('resource.reservation.seq', sequence_date=result.create_date) or _('New Reservation')
        return result


class ResourceRequest(models.Model):
    _name = "resource.request"
    _description = "Resource Request"

  
    RESOURCE_STATE = [
        ('submitted', 'Submitted'),
        ('approved', 'Approved'),
        ('rejected', 'Rejected')
    ]


    name = fields.Char(string='Name', default='New Request')
    state = fields.Selection(selection=RESOURCE_STATE, string="Request State", default='submitted')
    contact = fields.Integer(string="Contact", states={
            'approved': [('readonly', True)],
            'rejected': [('readonly', True)]
        }
    )
    message = fields.Text(string="Message", states={
            'approved': [('readonly', True)],
            'rejected': [('readonly', True)]
        }
    )
    requestor = fields.Char(string="Requestor", states={
            'approved': [('readonly', True)],
            'rejected': [('readonly', True)]
        }
    )
    requested_resource = fields.Char(string="Requested Resource", states={
            'approved': [('readonly', True)],
            'rejected': [('readonly', True)]
        }
    )

    def set_submitted(self):
        for record in self:
            record.state = 'submitted'

    def set_approved(self):
        for record in self:
            record.state = 'approved'
    
    def set_rejected(self):
        for record in self:
            record.state = 'rejected'

    @api.model
    def create(self, values):
        result = super(ResourceRequest, self).create(values)
        result.name = self.env['ir.sequence'].sudo().next_by_code('resource.request.seq', sequence_date=result.create_date) or _('New Request')
        return result
