# -*- coding: utf-8 -*-

from odoo import models, fields, tools, api, _, SUPERUSER_ID
from odoo.exceptions import ValidationError, Warning, UserError


class Event(models.Model):
    _inherit = 'event.event'


    description = fields.Char(string="Description")
    objective = fields.Char(string="Objectives")
    content = fields.Char(string="Content")
    website_id = fields.Many2one(comodel_name='website', default=lambda self: self.website_id.search([('aux_domain','=','aveirovoluntaria4g')], limit=1))


class EventRegistration(models.Model):
    _inherit = 'event.registration'

    gender = fields.Selection([
        ('male', 'Male'),
        ('female', 'Female'),
        ('other', 'Other')], 
        string='Gender', required=True, copy=False, default='male')

    nationality = fields.Char(string="Nationality")
    place_of_birth = fields.Char(string="Place Of Birth") #Naturalidade
    bday = fields.Date(string="Birthday")
    postal_code = fields.Integer(string="Postal Code")
    town = fields.Char(string="Town")
    cc = fields.Integer(string="CC")
    validity = fields.Date(string="CC Validity")
    occupation = fields.Char(string="Occupation/Profession")

    def confirm_registration(self):
        super(EventRegistration, self).confirm_registration()

        ResPartner = self.env['res.partner'].sudo()
        partner_id = ResPartner.search([
            ('email','=',self.email)
        ])

        if partner_id:
            self.partner_id = partner_id
            self.is_volunteer = True
        else:
            new_partner_id = self.env['res.partner'].create({
                'is_company': False,
                'name': self.name,
                'email': self.email,
                'phone': self.phone or '',
                'mobile': self.mobile or '',
                'is_volunteer': True
            })

            self.partner_id = new_partner_id
