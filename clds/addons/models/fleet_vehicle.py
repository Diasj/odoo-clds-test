# -*- coding: utf-8 -*-

from datetime import date, datetime

from odoo import models, fields, tools, api, _, SUPERUSER_ID
from odoo.exceptions import ValidationError, Warning, UserError


class FleetVehicle(models.Model):
    _inherit = 'fleet.vehicle'

    RESOURCE_MODEL_SELECTION = [
        ('light', 'Passenger Cars'),
        ('merch_light', 'Freight Cars'),
        ('van', 'Passenger Van'),
        ('merch_van', 'Freight Wagon'),
        ('small_bus', 'Mini Bus'),
        ('bus', 'Bus'),
    ]

    RESOURCE_STATUS_SELECTION = [
        ('shared', 'Shared'),
        ('stashed', 'Stashed'),
    ]

    TRAVEL_SELECTION = [
        ('yes', 'Yes'),
        ('no', 'No'),
    ]

    VEHICLE_COST_SELECTION = [
        ('per_km', '/Km'),
        ('per_hour', '/Hour'),
        ('per_day', '/Day'),
        ('per_passenger', '/Passenger'),
    ]

    INSTALLATION_SELECTION = [
        ('per_hour', '/Hour'),
        ('per_day', '/Day'),
    ]

    TRAINING_SELECTION = [
        ('per_trainee', '/Trainee')
    ]

    TECNICAL_HELP_SELECTION = [
        ('per_day', '/Day'),
        ('per_week', '/Week'),
        ('per_month', '/Month')
    ]

    HUMAN_RESOURCE_SELECTION = [
        ('per_hour', '/Hour')
    ]

    LEGAL_NATURE_SELECTION = [
        ('non_lucrative', 'Non Lucrative'),
        ('lucrative', 'Lucrative')
    ]
    
    def _compute_current_user_company(self):
        for record in self:
            if self.env.user.has_group('clds.group_clds_admin'):
                record.same_company = True
            else:
                record.same_company = record.company_id.id == self.env.user.company_id.id

    def _set_odometer(self):
        for record_id in self:
            if record_id.odometer:
                date = fields.Date.context_today(record_id)
                data = {
                    'value': record_id.odometer, 
                    'date': date, 
                    'vehicle_id': record_id.id, 
                    'origin': _('The change was manually done on the resource form!')
                }
                self.env['fleet.vehicle.odometer'].create(data)
    

    company_id = fields.Many2one(comodel_name='res.company', string='Owner', help='Owner of the vehicle', default=lambda self:self.env.user.company_id)
    same_company = fields.Boolean(string = 'Is Same Company', compute='_compute_current_user_company', default=True)
    legal_nature = fields.Selection(selection=LEGAL_NATURE_SELECTION, string = 'Legal Nature', default='non_lucrative')
    resource_type = fields.Selection(related='model_id.brand_id.resource_type')
    resource_model = fields.Selection(selection=RESOURCE_MODEL_SELECTION, string='Resource Model')
    resource_status = fields.Selection(selection=RESOURCE_STATUS_SELECTION, string='Resource Status', default='stashed')
    travel_to_institution = fields.Selection(selection=TRAVEL_SELECTION, string='Travel to the requesting institution', default='yes')
    vehicle_cost_selection = fields.Selection(selection=VEHICLE_COST_SELECTION, default='per_km')
    training_cost_selection = fields.Selection(selection=TRAINING_SELECTION, default='per_trainee')
    installation_cost_selection = fields.Selection(selection=INSTALLATION_SELECTION, default='per_hour')
    tecnical_help_cost_selection = fields.Selection(selection=TECNICAL_HELP_SELECTION, default='per_day')
    human_resource_cost_selection = fields.Selection(selection=HUMAN_RESOURCE_SELECTION, default='per_hour')
    odometer = fields.Float(compute='_get_odometer', inverse='_set_odometer', string='Last Odometer', help='Odometer measure of the vehicle at the moment of this log')
    vehicle_cost = fields.Float(string='Cost')
    training_cost = fields.Float(string='Cost')
    installation_cost = fields.Float(string='Cost')
    tecnical_help_cost = fields.Float(string='Cost')
    human_resource_cost = fields.Float(string='Cost')
    driver = fields.Char(string='Driver', help='Driver of the vehicle')
    share_date = fields.Char(string='Share Date', store = True )
    description = fields.Char(string='Description', size=256)
    availability = fields.Char(string='Availability')
    training_date = fields.Char(string='Date and schedule')
    training_place = fields.Char(string='Training Place')
    training_entity = fields.Char(string='Training Entity', size=20)
    instalation_type = fields.Char(string='Instalation Type')
    tec_help_typology = fields.Char(string='Typology')
    available_material = fields.Char(string='Available Material')
    professional_category = fields.Char(string='Professional Category')
    capacity = fields.Integer(string='Capacity')
    quantity = fields.Integer(string='Quantity')
    available_seats = fields.Integer(string='# of Available Seats')
    training_total_hours = fields.Integer(string='Total Hours')   
    schedule = fields.Text(string='Schedule')
    comments = fields.Text(string='Comments')
    itinerary = fields.Text(string='Itinerary')
    internal_obs = fields.Text(string='Comments')
    instalation_date = fields.Date(string='Construction Date')
    location = fields.Char(string='Location')
    resource_image = fields.Binary(string="Resource Image", attachment=True, help="This field holds the image used for resource")

    

    def reserve_resource(self):
        return {
            "type": "ir.actions.act_window",
            "name": "Reservation",
            "res_model": "resource.reservation",
            "views": [[False, 'form']],
            "target": 'new',
            "context": {
                'search_default_model_id': self.model_id.id,
                'default_model_id': self.model_id.id,
                'search_default_resource_id': self.id,
                'default_resource_id': self.id
            },
            "view_type": 'form',
            "view_mode": 'form',
            "flags": { 
                'form': { 
                    'action_buttons': False, 
                    'options': { 
                        'mode': 'edit' 
                    } 
                }
            }
        }            


    def set_shared_resource(self):
        for record_id in self:
            record_id.resource_status = 'shared'
            record_id.share_date = datetime.now().strftime('%Y-%m-%d %H:%M:%S') # Ex: '2018-05-10 09:15:31'

    def set_not_shared_resource(self):
        for record_id in self:
            record_id.resource_status = 'stashed'

    def copy(self, default=None):
        if not self.env.user.has_group('clds.group_clds_admin'):
            if self.company_id != self.env.user.company_id:
                raise Warning(_('Cannot duplicate a resource that does not belong to your entity.'))
            
        return super(FleetVehicle, self).copy(default)
    
    def write(self, values):        
        if values.get('company_id'):
            if not self.env.user.has_group('clds.group_clds_admin'):
                raise Warning(_('You do not have permission to change owner.'))
        
        return super(FleetVehicle, self).write(values)

    def unlink(self):
        if not self.env.user.has_group('clds.group_clds_admin'):
            if not self.env.user.has_group('clds.group_clds_manager'):
                for record in self:
                    if record.company_id != self.env.user.company_id:
                        raise Warning(_('You do not have permissions to remove this resource'))
        
        return super(FleetVehicle, self).unlink()

    def name_get(self):
        res = super(FleetVehicle, self).name_get()
        result = []
        for record in self:
            name = "{model_name}/{license_plate}".format(
                model_name=record.model_id.name,
                license_plate=record.license_plate
            )
            res.append((record.id, name))
        return res


class FleetVehicleOdometer(models.Model):
    _inherit = "fleet.vehicle.odometer"


    def _get_resource_domain(self):
        if self.env.user.has_group('clds.group_clds_admin'):
            domain = [
                '|',
                ('resource_type','=','vehicle'),
                ('resource_type','=','vehicle_with_driver'),
            ]
        else:
            domain = [
                '|',
                ('resource_type','=','vehicle'),
                ('resource_type','=','vehicle_with_driver'),
                '|',
                ('company_id','in',self.env.user.company_id.child_ids.ids),
                ('company_id','=',self.env.user.company_id.id)
            ]
        
        return domain

    vehicle_id = fields.Many2one(comodel_name='fleet.vehicle', string='Vehicle', required=True, domain=lambda self: self._get_resource_domain())
    company_id = fields.Many2one(string='Owner', related='vehicle_id.company_id', readonly=True, store=True)
    clds_purchaser_id = fields.Many2one(comodel_name='res.users', string='Purchaser', default=lambda self:self.env.user.id, readonly = True)
    resource_reservation_id = fields.Many2one(comodel_name='resource.reservation', string='Reservation')
    origin = fields.Char(string='Origin', size=64)
    driver = fields.Char(related="vehicle_id.driver", string="Driver")
    destination = fields.Char(string='Destination', size=64)
    resource_type = fields.Selection(related='vehicle_id.resource_type', store=True)
    start_odometer = fields.Float(string='Start Odometer', size=10, group_operator="max")
    child_company_ids = fields.One2many(comodel_name='res.company', related='company_id.child_ids')


    def write(self, values):
        import pdb; pdb.set_trace()

        return super(FleetVehicleOdometer, self).write(values)
    
    def search(self, args, offset=0, limit=None, order=None, count=False):
        context = self._context or {}
        if self.env.user.has_group('clds.group_clds_admin'): # sees all records
            args += [('company_id', 'in', self.env.user.company_ids.ids)]
        else: # sees records related to own company
            args += [('company_id', '=', self.env.user.company_id.id)]
           
        return super(FleetVehicleOdometer, self).search(args, offset, limit, order, count=count)
