# -*- coding: utf-8 -*-

from odoo import api, tools, fields, models, SUPERUSER_ID, _
from odoo.exceptions import ValidationError, Warning, UserError
from datetime import date


class ResCompany(models.Model):
    _inherit = "res.company"
    
    
    INSTITUTE_TOPOLOGY = [
        ('ipss', 'IPSS'),
        ('grouping_of_schools', 'Grouping of Schools'),
        ('public_institute', 'Public Institute'),
        ('non_profit_organization', 'Non Profit Organization'),
        ('for_profit_organization', 'For Profit Organization '),
    ]

    def _get_parent_company(self):
        company_id = self.search([], limit=1, order='id')
        return company_id.id if company_id else False
    
    def _compute_current_user_company(self):
        for record in self:
            if self.env.user.has_group('clds.group_clds_admin'):
                record.same_company = True
            else:
                record.same_company = record.id == self.env.user.company_id.id
    

    same_company = fields.Boolean(string = 'Is Same Company', compute='_compute_current_user_company', default=True)
    parent_id = fields.Many2one(comodel_name='res.company', string='Parent Company', index=True, readonly=True, store=True, default=_get_parent_company)
    institute_type = fields.Selection(INSTITUTE_TOPOLOGY, string = 'Institute Type')
    legal_nature = fields.Char(string="Legal Nature")
    
    # * Fields for social media [question for next meeting]
    facebook = fields.Char(string="Facebook")
    instagram = fields.Char(string="Instagram")
    website = fields.Char(string="Website")
    
    
    @api.model
    def fields_view_get(self, view_id=None, view_type='form', toolbar=False, submenu=False):
        res = super(ResCompany, self).fields_view_get(
            view_id=view_id,
            view_type=view_type,
            toolbar=toolbar,
            submenu=submenu
        )

        if res.get('toolbar', False) and res.get('toolbar').get('print', False):
            reports = res.get('toolbar').get('print')
            for report in reports:
                res['toolbar']['print'].remove(report)

        return res