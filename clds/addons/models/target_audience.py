from odoo import models, fields, tools, api, _, SUPERUSER_ID
from odoo.exceptions import ValidationError, Warning, UserError

class TargetAudience(models.Model):
    _name = 'target.audience'
    _description = 'Target Audience'
    _order = 'name'


    name = fields.Char(string='Name', translate=True)