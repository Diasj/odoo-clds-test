# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo import models, fields, tools, api, _, SUPERUSER_ID
from odoo.exceptions import ValidationError, Warning, UserError

class Website(models.Model):
    _inherit = "website"

    WEBSITE_DOMAIN = [
        ('aveiroemrede4g','Aveiro em Rede 4G'),
        ('aveirovoluntaria4g','Aveiro Voluntaria 4G')
    ]

    aux_domain = fields.Selection(selection=WEBSITE_DOMAIN, string = 'Aux Website Domain')