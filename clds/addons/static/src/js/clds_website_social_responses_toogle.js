odoo.define('clds.clds_website_social_responses_toogle', function (require) {

    var ajax = require('web.ajax');
    var core = require('web.core');
    var Dialog = require('web.Dialog');
    var Widget = require('web.Widget');
    var publicWidget = require('web.public.widget');
    var rpc = require('web.rpc');
    
    var _t = core._t;

    let main_container = document.getElementById('main_container');
    let table_title = document.getElementById('content_info_title');
    let table_content = document.getElementById('content_info_description');
    let main_table = document.getElementById('info_table_id');
    let entities_table = document.getElementById('entities_table');
    let row = main_table.insertRow(-1);
    var cell = row.insertCell(-1);
    cell.style.display = 'none';

    let hide_content = (class_name) => {
        $('#entities_table').find('tbody').each(function () {
            $(this).hide();
        });
    }
    
    publicWidget.registry.ToggleMore = publicWidget.Widget.extend({
        selector: '.o_wresource_main_row',
        events: { 
            'click #toggle_more': '_ToggleMore',
            'click .know_more' : '_KnowMore',

            // * BLUE CATEGORY

            'click #babysitter_nav_item' : '_ShowBabbySitterInfo',
            'click #family_day_care_nav_item' : '_ShowFamilyDayCareInfo',
            'click #nursery_nav_item' : '_ShowNurseryInfo',
            'click #eepe_nav_item' : '_ShowEEPEInfo',
            'click #catl_nav_item' : '_ShowCATLInfo',

            'click #ipi_nav_item' : '_ShowIpiInfo',
            'click #support_home_nav_item' : '_ShowSupportHomeInfo',
            'click #dis_transport_nav_item' : '_ShowDisTransport',

            'click #cafap_nav_item' : '_ShowCAFAPInfo',
            'click #street_youth_support_nav_item' : '_ShowStreetYouthSupportInfo',
            'click #family_reception_nav_item' : '_ShowFamilyReceptionInfo',
            'click #residential_home_nav_item' : '_ShowResidentialHomeInfo',
            'click #autonomy_apartment_nav_item' : '_ShowAutonomyApartmentInfo',

            // * YELLOW CATEGORY

            'click #sad_nav_item':'_ShowSADInfo',
            'click #social_center_nav_item':'_ShowSocialCenterInfo',
            'click #day_care_nav_item':'_ShowDayCareInfo',
            'click #night_center_nav_item':'_ShowNightCenterInfo',
            'click #care_for_elderly_nav_item':'_ShowCareElderlyInfo',
            'click #erpi_nav_item':'_ShowERPIInfo',

            'click #service_center_dis_nav_item':'_ShowServiceCenterDisInfo',
            'click #service_center_rehab_nav_item':'_ShowServiceCenterRehabInfo',
            'click #home_support_sad_nav_item':'_ShowHomeSupportSADInfo',
            'click #cao_nav_item':'_ShowCAOInfo',
            'click #reception_dis_nav_item':'_ShowReceptionDisInfo',
            'click #residential_adult_home_nav_item':'_ShowAdultResidentialHomeInfo',
            'click #autonomous_residence_nav_item':'_ShowAutonomousResidenceInfo',
            'click #transport_dis_nav_item':'_ShowTransportDisInfo',

            'click #dependent_sad_nav_item' : '_ShowDependentSADInfo',
            'click #adi_nav_item' : '_ShowADIInfo',
            'click #uai_nav_item' : '_ShowUAIInfo',
            'click #supported_life_nav_item' : '_ShowSupportedLifeInfo',

            'click #street_team_nav_item' : '_ShowStreetTeamInfo',
            'click #occupational_workshop_nav_item' : '_ShowOccupationalWorkshopInfo',

            // * RED CATEGORY

            'click #saas_nav_item' : '_ShowSAASInfo',
            'click #self_help_nav_item' : '_ShowSelfHelpInfo',
            'click #community_center_nav_item' : '_ShowCommunityCenterInfo',
            'click #vacation_center_nav_item' : '_ShowVacationCenterInfo',
            'click #caf_canteen_nav_item' : '_ShowCafCanteenInfo',
            'click #life_support_nav_item' : '_ShowLifeSupportInfo',
            'click #insert_community_nav_item' : '_ShowInsertCommunityInfo',
            'click #cat_nav_item' : '_ShowCATInfo',
            'click #food_aid_nav_item' : '_ShowFoodAIDInfo',

            'click #caap_nav_item' : '_ShowCAAPInfo',
            'click #family_sad_nav_item' : '_ShowFamilySADInfo',
            'click #residence_infected_nav_item' : '_ShowResidenceInfectedInfo',

            'click #direct_team_nav_item' : '_ShowDirectTeamInfo',
            'click #reintegration_program_nav_item' : '_ShowReintegrationProgramInfo',

            'click #call_center_nav_item' : '_ShowCallCenterInfo',
            'click #shelter_house_nav_item' : '_ShowShelterHouseInfo',

            // * GREEN CATEGORY

            'click #children_support_nav_item' : '_ShowChildrenSupportInfo',
            'click #ambulatory_care_nav_item' : '_ShowAmbulatoryCareInfo',
            'click #braille_press_nav_item' : '_ShowBraillePressInfo',
            'click #school_dogs_nav_item' : '_ShowSchoolDogsInfo',
            'click #cri_nav_item' : '_ShowCRIInfo',

        },

        start: function () {
            return this._super.apply(this, arguments);
        },
     
        _ToggleMore: function(ev){
            var self = this;
            var class_list = ev.currentTarget.classList;
            classes = class_list.value.split(" ")
            var current_target = $(ev.currentTarget)

            if ( classes.includes("resource_collapse") ){
                current_target.removeClass('resource_collapse');
                current_target.removeClass('fa-chevron-right');
                current_target.addClass('fa-chevron-down');
            } else {
                current_target.addClass('resource_collapse');
                current_target.removeClass('fa-chevron-down');
                current_target.addClass('fa-chevron-right');
            }
        },

        _KnowMore: function(ev) {
            let btn = ev.currentTarget;
            let parent_row = btn.parentElement;
            let company_id_value = $(parent_row).find(".company_id_class").html();
            let sub_category_value = $(parent_row).find(".sub_category_value").attr("data-value");
            // * Fetch Extra Info
            console.log(company_id_value);
            console.log(sub_category_value);


            if ($(btn).attr("data-attribute-type") == "plus") {
                let args = [
                    company_id_value,
                    sub_category_value,
                ]
                rpc.query({
                    model: 'resource.social.response',
                    method: 'get_extra_info',
                    args: args
                }).then(queryResult => {
                    $(queryResult).insertAfter(parent_row);
                    $(btn).attr("data-attribute-type","minus");
                    btn.innerHTML = '<i class="fa fa-minus"></i>';
                })
            }
            else {
                $(parent_row.parentElement).find("tr:gt(0)").remove();
                $(btn).attr("data-attribute-type","plus");
                btn.innerHTML = '<i class="fa fa-plus"></i>';
            }
        },

        _ShowBabbySitterInfo: function(ev) {
            hide_content();
            $(cell).hide();
            table_title.innerHTML = 'Ama';
            table_content.innerHTML = '<h4>Enquadramento</h4><p style="font-size: 16px; font-weight: normal;">Resposta social que consiste no exercício de atividade de ama, destinada a cuidar na sua residência de crianças até aos três anos de idade, ou até atingir a idade de ingresso no estabelecimento de educação pré-escolar, por tempo correspondente ao período de trabalho ou impedimento dos pais ou de quem exerce as responsabilidades parentais (família).</p><p style="font-size: 16px; font-weight: normal;">Fonte: <a href="http://www.seg-social.pt">http://www.seg-social.pt</a></p>';
            $(main_container).show();
            $('.babysitter_tbody').show();
        },

        _ShowFamilyDayCareInfo : function(ev) {
            hide_content();
            $(cell).hide();
            table_title.innerHTML = 'Creche Familiar';
            table_content.innerHTML = '<h4>Enquadramento</h4><p style="font-size: 16px; font-weight: normal;">Resposta social que consiste no exercício de atividade de ama quando desenvolvida no âmbito de uma instituição de enquadramento, destinada ao cuidado de crianças até aos três anos de idade, ou até atingirem a idade de ingresso no estabelecimento de educação pré-escolar, por tempo correspondente ao período de trabalho ou impedimento dos pais ou de quem exerce as responsabilidades parentais.</p><p style="font-size: 16px; font-weight: normal;">Fonte: <a href="http://www.seg-social.pt">http://www.seg-social.pt</a></p>';
            $(main_container).show();
            $('.family_day_care_tbody').show();
        },

        _ShowNurseryInfo : function(ev) {
            hide_content();
            cell.innerHTML = '<h5 style="margin-bottom: 10px;">Distribuição por idades:</h5><p style="font-weight: normal;"><b>Berçario</b> - Até à aquisição da marcha</p><p style="font-weight: normal;"><b>Aquisição da marcha</b> - Da aquisição marcha – 24 meses</p><p style="font-weight: normal;"><b>Transição</b> - Dos 24 - 36 meses</p>'
            $(cell).show();
            table_title.innerHTML = 'Creche';
            table_content.innerHTML = '<h4>Enquadramento</h4><p style="font-size: 16px; font-weight: normal;">Resposta social de natureza sócio-educativa, para acolher crianças até aos 3 anos de idade, durante operíodo de impedimento dos pais ou da pessoa que tenha a sua guarda de facto.</p><p style="font-size: 16px; font-weight: normal;">Fonte: <a href="http://www.seg-social.pt">http://www.seg-social.pt</a></p>';
            $(main_container).show();
            $('.nursery_tbody').show();
        },

        _ShowEEPEInfo : function(ev) {
            hide_content();
            $(cell).hide();
            table_title.innerHTML = 'Estabelecimento de Educação Pré-Escolar (EEPE)';
            table_content.innerHTML = '<h4>Enquadramento</h4><p style="font-size: 16px; font-weight: normal;">Resposta social orientada para o desenvolvimento de crianças com idades compreendidas entre os 3 anos e a idade de ingresso no ensino básico, proporcionando-lhes atividades educativas e atividades de apoio à família.</p><p style="font-size: 16px; font-weight: normal;">Fonte: <a href="http://www.seg-social.pt">http://www.seg-social.pt</a></p>';
            $(main_container).show();
            $('.eepe_tbody').show();
        },

        _ShowCATLInfo : function(ev) {
            hide_content();
            cell.innerHTML = '<h5 style="margin-bottom: 10px;">Categorias:</h5><p style="font-weight: normal;"><b>CATL clássico com almoço</b></p><p style="font-weight: normal;"><b>CATL clássico sem almoço</b></p><p style="font-weight: normal;"><b>CATL com extensão de horário com almoço</b></p><p style="font-weight: normal;"><b>CATL com extensão de horário em almoço</b></p><p style="font-weight: normal;"><b>CATL de conciliação familiar</b></p>';
            $(cell).show();
            table_title.innerHTML = 'Centro de Atividades de Tempos Livres (CATL)';
            table_content.innerHTML = '<h4>Enquadramento</h4><p style="font-size: 16px; font-weight: normal;">Resposta social de natureza sócio-educativa, para acolher crianças até aos 3 anos de idade, durante operíodo de impedimento dos pais ou da pessoa que tenha a sua guarda de facto.</p><p style="font-size: 16px; font-weight: normal;">Fonte: <a href="http://www.seg-social.pt">http://www.seg-social.pt</a></p>';
            $(main_container).show();
            $('.catl_tbody').show();
        },

        _ShowIpiInfo : function(ev) {
            hide_content();
            $(cell).hide();
            table_title.innerHTML = 'Intervenção Precoce na Infância (IPI)';
            table_content.innerHTML = '<h4>Enquadramento</h4><p style="font-size: 16px; font-weight: normal;">Resposta que visa garantir condições de desenvolvimento das crianças com alterações nas funções ou estruturas do corpo que limitam o crescimento pessoal e social e a sua participação nas atividades típicas para a idade, bem como das crianças com risco grave de atraso de desenvolvimento.</p><p style="font-size: 16px; font-weight: normal;">Fonte: <a href="http://www.seg-social.pt">http://www.seg-social.pt</a></p>';
            $(main_container).show();
            $('.ipi_tbody').show();
        },

        _ShowSupportHomeInfo : function(ev) {
            hide_content();
            $(cell).hide();
            table_title.innerHTML = 'Lar de Apoio';
            table_content.innerHTML = '<h4>Enquadramento</h4><p style="font-size: 16px; font-weight: normal;">Resposta social, desenvolvida em equipamento, destinada a acolher crianças e jovens com necessidades educativas especiais que necessitem de frequentar estruturas de apoio específico situadas longe do local da sua residência habitual ou que, por comprovadas necessidades familiares, precisem, temporariamente, de resposta substitutiva da família.</p><p style="font-size: 16px; font-weight: normal;">Fonte: <a href="http://www.cartasocial.pt/">http://www.cartasocial.pt/</a></p>';
            $(main_container).show();
            $('.support_home_tbody').show();
        },

        _ShowDisTransport : function(ev) {
            hide_content();
            $(cell).hide();
            table_title.innerHTML = 'Transporte de Pessoas com Deficiência';
            table_content.innerHTML = '<h4>Enquadramento</h4><p style="font-size: 16px; font-weight: normal;">Serviço de transporte e acompanhamento personalizado, para pessoas com deficiência, independentemente da idade.</p><p style="font-size: 16px; font-weight: normal;">Fonte: <a href="http://www.seg-social.pt">http://www.seg-social.pt</a></p>';
            $(main_container).show();
            $('.dis_transport_tbody').show();
        },

        _ShowCAFAPInfo : function(ev) {
            hide_content();
            $(cell).hide();
            table_title.innerHTML = 'Centro de Apoio Familiar e Aconselhamento Parental (CAFAP)';
            table_content.innerHTML = '<h4>Enquadramento</h4><p style="font-size: 16px; font-weight: normal;">Resposta desenvolvida através de um serviço de apoio especializado às famílias com crianças e jovens, vocacionado para a prevenção e reparação de situações de risco psicossocial mediante o desenvolvimento de competências parentais, pessoais e sociais das famílias.</p><p style="font-size: 16px; font-weight: normal;">Fonte: <a href="http://www.seg-social.pt">http://www.seg-social.pt</a></p>';
            $(main_container).show();
            $('.cafap_tbody').show();
        },

        _ShowStreetYouthSupportInfo : function(ev) {
            hide_content();
            $(cell).hide();
            table_title.innerHTML = 'Equipa de Rua de Apoio a Crianças e Jovens';
            table_content.innerHTML = '<h4>Enquadramento</h4><p style="font-size: 16px; font-weight: normal;">Resposta desenvolvida através de um serviço de apoio especializado às famílias com crianças e jovens, vocacionado para a prevenção e reparação de situações de risco psicossocial mediante o desenvolvimento de competências parentais, pessoais e sociais das famílias.</p><p style="font-size: 16px; font-weight: normal;">Fonte: <a href="http://www.seg-social.pt">http://www.seg-social.pt</a></p>';
            $(main_container).show();
            $('.street_youth_support_tbody').show();
        },

        _ShowFamilyReceptionInfo : function(ev) {
            $(cell).hide();
            table_title.innerHTML = 'Acolhimento Familiar';
            table_content.innerHTML = '<h4>Enquadramento</h4><p style="font-size: 16px; font-weight: normal;">O acolhimento familiar consiste na atribuição da confiança da criança ou do jovem a uma pessoa singular ou a uma família, habilitadas para o efeito, visando proporcionar à criança ou jovem a integração em meio familiar estável que lhe garanta os cuidados adequados às suas necessidades e ao seu bem -estar, bem como a educação e o afeto necessários ao seu desenvolvimento integral.</p><p style="font-size: 16px; font-weight: normal;">Fonte: <a href="http://www.seg-social.pt">http://www.seg-social.pt</a></p>';
            $(main_container).show();
            $('.family_reception_tbody').show();
        },

        _ShowResidentialHomeInfo : function(ev) {
            hide_content();
            cell.innerHTML = '<h5 style="margin-bottom: 10px;">Residências:</h5><p style="font-weight: normal;"><b>Centro de Acolhimento Temporário (CAT)</b></p><p style="font-weight: normal;"><b>Lar de Infância e Juventude (LIJ)</b></p>';
            $(cell).show();
            table_title.innerHTML = 'Acolhimento Residencial';
            table_content.innerHTML = '<h4>Enquadramento</h4><p style="font-size: 16px; font-weight: normal;">O acolhimento residencial tem lugar em casas de acolhimento as quais são estabelecimentos de apoio social que asseguram resposta a situações que impliquem o afastamento ou retirada da criança ou do jovem da situação de perigo, podendo incluir unidades residenciais e/ou unidades residenciais especializadas, tendo em conta as situações, problemáticas e características específicas das crianças e dos jovens.</p><p style="font-size: 16px; font-weight: normal;">Fonte: <a href="http://www.seg-social.pt">http://www.seg-social.pt</a></p>';
            $(main_container).show();
            $('.residential_home_tbody').show();
        },

        _ShowAutonomyApartmentInfo : function(ev) {
            hide_content();
            $(cell).hide();
            table_title.innerHTML = 'Apartamento de Autonomização';
            table_content.innerHTML = '<h4>Enquadramento</h4><p style="font-size: 16px; font-weight: normal;">Resposta social, desenvolvida em equipamento – apartamento inserido na comunidade local – destinada a apoiar a transição para a vida adulta de jovens que possuem competências pessoais específicas, através da dinamização de serviços que articulem e potenciem recursos existentes nos espaços territoriais.</p><p style="font-size: 16px; font-weight: normal;">Fonte: <a href="http://www.seg-social.pt">http://www.seg-social.pt</a></p>';
            $(main_container).show();
            $('.autonomy_apartment_tbody').show();
        },

        _ShowSADInfo : function(ev) {
            hide_content();
            $(cell).hide();
            table_title.innerHTML = 'Serviço de Apoio Domiciliário (SAD)';
            table_content.innerHTML = '<h4>Enquadramento</h4><p style="font-size: 16px; font-weight: normal;">Resposta social que consiste na prestação de cuidados e serviços a famílias e ou pessoas que se encontrem no seu domicílio, em situação de dependência física e ou psíquica e que não possam assegurar, temporária ou permanentemente, a satisfação das suas necessidades básicas e ou a realização das atividades instrumentais da vida diária, nem disponham de apoio familiar para o efeito.</p><p style="font-size: 16px; font-weight: normal;">Fonte: <a href="http://www.seg-social.pt">http://www.seg-social.pt</a></p>';
            $(main_container).show();
            $('.sub_old_sad_tbody').show();
        },

        _ShowSocialCenterInfo : function(ev) {
            hide_content();
            $(cell).hide();
            table_title.innerHTML = 'Centro de Convívio';
            table_content.innerHTML = '<h4>Enquadramento</h4><p style="font-size: 16px; font-weight: normal;">Resposta social de apoio a atividades sociais, recreativas e culturais, organizadas e dinamizadas com participação ativa das pessoas idosas, residentes numa determinada comunidade.</p><p style="font-size: 16px; font-weight: normal;">Fonte: <a href="http://www.seg-social.pt">http://www.seg-social.pt</a></p>';
            $(main_container).show();
            $('.social_center_tbody').show();
        },

        _ShowDayCareInfo : function(ev) {
            hide_content();
            $(cell).hide();
            table_title.innerHTML = 'Centro de Dia';
            table_content.innerHTML = '<h4>Enquadramento</h4><p style="font-size: 16px; font-weight: normal;">Resposta social que presta um conjunto de serviços que contribuem para a manutenção no seu meio social e familiar, das pessoas com 65 e mais anos, que precisem dos serviços prestados pelo Centro de Dia.</p><p style="font-size: 16px; font-weight: normal;">Fonte: <a href="http://www.seg-social.pt">http://www.seg-social.pt</a></p>';
            $(main_container).show();
            $('.day_care_tbody').show();
        },

        _ShowNightCenterInfo : function(ev) {
            hide_content();
            $(cell).hide();
            table_title.innerHTML = 'Centro de Noite';
            table_content.innerHTML = '<h4>Enquadramento</h4><p style="font-size: 16px; font-weight: normal;">Resposta social que funciona em equipamento de acolhimento noturno, dirigido a pessoas idosas com autonomia que, durante o dia permaneçam no seu domicílio e que por vivenciarem situações de solidão, isolamento e insegurança, necessitam de acompanhamento durante a noite.</p><p style="font-size: 16px; font-weight: normal;">Fonte: <a href="http://www.seg-social.pt">http://www.seg-social.pt</a></p>';
            $(main_container).show();
            $('.night_center_tbody').show();
        },

        _ShowCareElderlyInfo : function(ev) {
            hide_content();
            $(cell).hide();
            table_title.innerHTML = 'Acolhimento Familiar de Pessoas Idosas';
            table_content.innerHTML = '<h4>Enquadramento</h4><p style="font-size: 16px; font-weight: normal;">Resposta social que consiste em integrar, temporária ou permanentemente, pessoas idosas em famílias capazes de lhes proporcionar um ambiente estável e seguro.</p><p style="font-size: 16px; font-weight: normal;">Fonte: <a href="http://www.seg-social.pt">http://www.seg-social.pt</a></p>';
            $(main_container).show();
            $('.care_for_elderly_tbody').show();
        },

        _ShowERPIInfo : function(ev) {
            hide_content();
            $(cell).hide();
            table_title.innerHTML = 'Estrutura Residencial para Pessoas Idosas (ERPI)';
            table_content.innerHTML = '<h4>Enquadramento</h4><p style="font-size: 16px; font-weight: normal;">Resposta social destinada a alojamento coletivo, de utilização temporária ou permanente, para idosos.</p><p style="font-size: 16px; font-weight: normal;">Fonte: <a href="http://www.seg-social.pt">http://www.seg-social.pt</a></p>';
            $(main_container).show();
            $('.erpi_tbody').show();
        },

        _ShowServiceCenterDisInfo : function(ev) {
            hide_content();
            $(cell).hide();
            table_title.innerHTML = 'Centro de Atendimento/Acompanhamento e Animação Para Pessoas com Deficiência';
            table_content.innerHTML = '<h4>Enquadramento</h4><p style="font-size: 16px; font-weight: normal;">Resposta social, desenvolvida em equipamento, organizada em espaço polivalente, destinado a informar, orientar e apoiar as pessoas com deficiência, promovendo o desenvolvimento das competências necessárias à resolução dos seus próprios problemas, bem como atividades de animação sociocultural.</p><p style="font-size: 16px; font-weight: normal;">Fonte: <a href="http://www.seg-social.pt">http://www.seg-social.pt</a></p>';
            $(main_container).show();
            $('.service_center_dis_tbody').show();
        },

        _ShowServiceCenterRehabInfo : function(ev) {
            hide_content();
            $(cell).hide();
            table_title.innerHTML = 'Centro de Atendimento, Acompanhamento e Reabilitação Social';
            table_content.innerHTML = '<h4>Enquadramento</h4><p style="font-size: 16px; font-weight: normal;">Resposta social destinada a assegurar o atendimento, acompanhamento e o processo de reabilitação social a pessoas comdeficiência e incapacidade e a disponibilizar serviços de capacitação e suporte às suas famílias ou cuidadores informais.</p><p style="font-size: 16px; font-weight: normal;">Fonte: <a href="http://www.seg-social.pt">http://www.seg-social.pt</a></p>';
            $(main_container).show();
            $('.service_center_rehab_tbody').show();
        },

        _ShowHomeSupportSADInfo : function(ev) {
            hide_content();
            $(cell).hide();
            table_title.innerHTML = 'Serviço de Apoio Domiciliário (SAD)';
            table_content.innerHTML = '<h4>Enquadramento</h4><p style="font-size: 16px; font-weight: normal;">Resposta social que consiste na prestação de cuidados e serviços a famílias e ou pessoas que se encontrem no seu domicílio, em situação de dependência física e ou psíquica e que não possam assegurar, temporária ou permanentemente, a satisfação das suas necessidades básicas e ou a realização das atividades instrumentais da vida diária, nem disponham de apoio familiar para o efeito.</p><p style="font-size: 16px; font-weight: normal;">Fonte: <a href="http://www.seg-social.pt">http://www.seg-social.pt</a></p>';
            $(main_container).show();
            $('.disabled_sad_tbody').show();
        },

        _ShowCAOInfo : function(ev) {
            hide_content();
            $(cell).hide();
            table_title.innerHTML = 'Centro de Atividades Ocupacionais (CAO)';
            table_content.innerHTML = '<h4>Enquadramento</h4><p style="font-size: 16px; font-weight: normal;">Resposta social destinada a promover atividades para jovens e adultos, a partir dos 16 anos, com deficiência grave.</p><p style="font-size: 16px; font-weight: normal;">Fonte: <a href="http://www.seg-social.pt">http://www.seg-social.pt</a></p>';
            $(main_container).show();
            $('.cao_tbody').show();
        },

        _ShowReceptionDisInfo : function(ev) {
            hide_content();
            $(cell).hide();
            table_title.innerHTML = 'Acolhimento Familiar para Pessoas Adultas com Deficiência';
            table_content.innerHTML = '<h4>Enquadramento</h4><p style="font-size: 16px; font-weight: normal;">Resposta social que consiste em integrar temporária ou permanentemente pessoas adultas com deficiência, em famílias capazes de lhes proporcionar um ambiente estável e seguro.</p><p style="font-size: 16px; font-weight: normal;">Fonte: <a href="http://www.seg-social.pt">http://www.seg-social.pt</a></p>';
            $(main_container).show();
            $('.reception_dis_tbody').show();
        },

        _ShowAdultResidentialHomeInfo : function(ev) {
            hide_content();
            $(cell).hide();
            table_title.innerHTML = 'Lar Residencial';
            table_content.innerHTML = '<h4>Enquadramento</h4><p style="font-size: 16px; font-weight: normal;">Estabelecimento para alojamento coletivo, de utilização temporária ou permanente, de pessoas com deficiência e incapacidade, de idade igual ou superior a 16 anos, que se encontrem impedidas de residir no seu meio familiar.</p><p style="font-size: 16px; font-weight: normal;">Fonte: <a href="http://www.seg-social.pt">http://www.seg-social.pt</a></p>';
            $(main_container).show();
            $('.residential_home_tbody').show();
        },

        _ShowAutonomousResidenceInfo : function(ev) {
            hide_content();
            $(cell).hide();
            table_title.innerHTML = 'Residência Autónoma';
            table_content.innerHTML = '<h4>Enquadramento</h4><p style="font-size: 16px; font-weight: normal;">Estabelecimento de alojamento temporário ou permanente em apartamento, moradia ou outra tipologia similar,destinado a pessoas com deficiência e incapacidade, de idade igual ou superior a 18 anos, que, mediante apoio, têm capacidade para viver de forma autónoma</p><p style="font-size: 16px; font-weight: normal;">Fonte: <a href="http://www.seg-social.pt">http://www.seg-social.pt</a></p>';
            $(main_container).show();
            $('.autonomous_residence_tbody').show();
        },

        _ShowTransportDisInfo : function(ev) {
            hide_content();
            $(cell).hide();
            table_title.innerHTML = 'Transporte de Pessoas com Deficiência';
            table_content.innerHTML = '<h4>Enquadramento</h4><p style="font-size: 16px; font-weight: normal;">Serviço de transporte e acompanhamento personalizado, para pessoas com deficiência, independentemente da idade.</p><p style="font-size: 16px; font-weight: normal;">Fonte: <a href="http://www.seg-social.pt">http://www.seg-social.pt</a></p>';
            $(main_container).show();
            $('.transport_dis_tbody').show();
        },

        _ShowDependentSADInfo : function(ev) {
            hide_content();
            $(cell).hide();
            table_title.innerHTML = 'Serviço de Apoio Domiciliário (SAD)';
            table_content.innerHTML = '<h4>Enquadramento</h4><p style="font-size: 16px; font-weight: normal;">Resposta social que consiste na prestação de cuidados e serviços a famílias e ou pessoas que se encontrem no seu domicílio, em situação de dependência física e ou psíquica e que não possam assegurar, temporária ou permanentemente, a satisfação das suas necessidades básicas e ou a realização das atividades instrumentais da vida diária, nem disponham de apoio familiar para o efeito.</p><p style="font-size: 16px; font-weight: normal;">Fonte: <a href="http://www.seg-social.pt">http://www.seg-social.pt</a></p>';
            $(main_container).show();
            $('.sub_dependent_sad_tbody').show();
        },

        _ShowADIInfo : function(ev) {
            hide_content();
            $(cell).hide();
            table_title.innerHTML = 'Apoio Domiciliário Integrado (ADI)';
            table_content.innerHTML = '<h4>Enquadramento</h4><p style="font-size: 16px; font-weight: normal;">Resposta que se concretiza através de um conjunto de ações e cuidados pluridisciplinares, flexíveis, abrangentes, acessíveis e articulados, de apoio social e de saúde, a prestar no domicílio, durante vinte e quatro horas por dia e sete dias por semana.</p><p style="font-size: 16px; font-weight: normal;">Fonte: <a href="http://www.cartasocial.pt/">http://www.cartasocial.pt/</a></p>';
            $(main_container).show();
            $('.adi_tbody').show();
        },

        _ShowUAIInfo : function(ev) {
            hide_content();
            $(cell).hide();
            table_title.innerHTML = 'Unidade de Apoio Integrado (UAI)';
            table_content.innerHTML = '<h4>Enquadramento</h4><p style="font-size: 16px; font-weight: normal;">Resposta, desenvolvida em equipamento, que visa prestar cuidados temporários, globais e integrados, a pessoas que, por motivo de dependência, não podem manter-se apoiadas no seu domicílio, mas que não carecem de cuidados clínicos em internamento hospitalar</p><p style="font-size: 16px; font-weight: normal;">Fonte: <a href="http://www.cartasocial.pt/">http://www.cartasocial.pt/</a></p>';
            $(main_container).show();
            $('.uai_tbody').show();
        },

        _ShowSupportedLifeInfo : function(ev) {
            hide_content();
            $(cell).hide();
            table_title.innerHTML = 'Unidade de Vida Apoiada';
            table_content.innerHTML = '<h4>Enquadramento</h4><p style="font-size: 16px; font-weight: normal;">Resposta, desenvolvida em equipamento, destinada a pessoas adultas que, por limitação mental crónica e fatores sociais graves, alcançaram um grau de desvantagem que não lhes permite organizar, sem apoio, as atividades de vida diária, mas que não necessitam de intervenção médica frequente.</p><p style="font-size: 16px; font-weight: normal;">Fonte: <a href="http://www.cartasocial.pt/">http://www.cartasocial.pt/</a></p>';
            $(main_container).show();
            $('.supported_life_tbody').show();
        },

        _ShowStreetTeamInfo : function(ev) {
            hide_content();
            $(cell).hide();
            table_title.innerHTML = 'Equipa de Rua Para Pessoas Sem Abrigo';
            table_content.innerHTML = '<h4>Enquadramento</h4><p style="font-size: 16px; font-weight: normal;">Resposta social, desenvolvida através de um serviço prestado por equipa multidisciplinar, que estabelece uma abordagem com os sem-abrigo, visando melhorar as condições de vida da população sem-abrigo que não se desloca aos serviços.</p><p style="font-size: 16px; font-weight: normal;">Fonte: <a href="http://www.seg-social.pt">http://www.seg-social.pt</a></p>';
            $(main_container).show();
            $('.street_team_tbody').show();
        },

        _ShowOccupationalWorkshopInfo : function(ev) {
            hide_content();
            $(cell).hide();
            table_title.innerHTML = 'Atelier Ocupacional';
            table_content.innerHTML = '<h4>Enquadramento</h4><p style="font-size: 16px; font-weight: normal;">Resposta social destinada ao apoio à população adulta, sem abrigo, com vista à reabilitação das suas capacidades e competências sociais, através do desenvolvimento de atividades integradas em programas "estruturados" que implicam uma participação assídua do indivíduo, ou "flexíveis" onde a assiduidade depende da sua disponibilidade e motivação.</p><p style="font-size: 16px; font-weight: normal;">Fonte: <a href="http://www.seg-social.pt">http://www.seg-social.pt</a></p>';
            $(main_container).show();
            $('.occupational_workshop_tbody').show();
        },

        _ShowSAASInfo : function(ev) {
            hide_content();
            $(cell).hide();
            table_title.innerHTML = 'Serviço de Atendimento e Acompanhamento Social (SAAS)';
            table_content.innerHTML = '<h4>Enquadramento</h4><p style="font-size: 16px; font-weight: normal;">Resposta social que visa apoiar as pessoas e as famílias, residentes numa determinada área geográfica, na prevenção e/ou reparação de problemas geradores ou gerados por situações de exclusão social e, em certos casos, atuar em situações de emergência.</p><p style="font-size: 16px; font-weight: normal;">Fonte: <a href="http://www.seg-social.pt">http://www.seg-social.pt</a></p>';
            $(main_container).show();
            $('.saas_tbody').show();
        },

        _ShowSelfHelpInfo : function(ev) {
            hide_content();
            $(cell).hide();
            table_title.innerHTML = 'Grupo de Autoajuda';
            table_content.innerHTML = '<h4>Enquadramento</h4><p style="font-size: 16px; font-weight: normal;">Resposta social, desenvolvida através de pequenos grupos para interajuda, organizados e integrados por pessoas que passam ou passaram pela mesma situação/problema, com vista a encontrar soluções pela partilha de experiências e troca de informação</p><p style="font-size: 16px; font-weight: normal;">Fonte: <a href="http://www.seg-social.pt">http://www.seg-social.pt</a></p>';
            $(main_container).show();
            $('.self_help_tbody').show();
        },

        _ShowCommunityCenterInfo : function(ev) {
            hide_content();
            $(cell).hide();
            table_title.innerHTML = 'Centro Comunitário';
            table_content.innerHTML = '<h4>Enquadramento</h4><p style="font-size: 16px; font-weight: normal;">Resposta social para pessoas e famílias de uma determinada área geográfica, onde se prestam serviços e desenvolvem atividades que, de uma forma articulada, tendem a constituir um pólo de animação com vista à prevenção de problemas sociais e à definição de um projeto de desenvolvimento local, coletivamente assumido.</p><p style="font-size: 16px; font-weight: normal;">Fonte: <a href="http://www.seg-social.pt">http://www.seg-social.pt</a></p>';
            $(main_container).show();
            $('.community_center_tbody').show();
        },

        _ShowVacationCenterInfo : function(ev) {
            hide_content();
            $(cell).hide();
            table_title.innerHTML = 'Centro de Férias e Lazer';
            table_content.innerHTML = '<h4>Enquadramento</h4><p style="font-size: 16px; font-weight: normal;">Resposta social destinada a todas as faixas etárias da população e à família na sua globalidade para satisfação de necessidades de lazer e de quebra da rotina, essencial ao equilíbrio físico, psicológico e social dos seus utilizadores.</p><p style="font-size: 16px; font-weight: normal;">Fonte: <a href="http://www.seg-social.pt">http://www.seg-social.pt</a></p>';
            $(main_container).show();
            $('.vacation_center_tbody').show();
        },

        _ShowCafCanteenInfo : function(ev) {
            hide_content();
            $(cell).hide();
            table_title.innerHTML = 'Refeitório/Cantina Social';
            table_content.innerHTML = 'h4>Enquadramento</h4><p style="font-size: 16px; font-weight: normal;">Resposta social destinada ao fornecimento de refeições, a pessoas e famílias economicamente desfavorecidas, podendo integrar outros serviços, nomeadamente de higiene pessoal e tratamento de roupas.</p><p style="font-size: 16px; font-weight: normal;">Fonte: <a href="http://www.seg-social.pt">http://www.seg-social.pt</a></p>';
            $(main_container).show();
            $('.caf_canteen_tbody').show();
        },

        _ShowLifeSupportInfo : function(ev) {
            hide_content();
            $(cell).hide();
            table_title.innerHTML = 'Centro de Apoio à vida';
            table_content.innerHTML = ' <h4>Enquadramento</h4><p style="font-size: 16px; font-weight: normal;">Resposta social para apoiar e acompanhar mulheres grávidas ou com filhos recém-nascidos, que se encontram em risco emocional ou social.</p><p style="font-size: 16px; font-weight: normal;">Fonte: <a href="http://www.seg-social.pt">http://www.seg-social.pt</a></p></th>';
            $(main_container).show();
            $('.life_support_tbody').show();
        },

        _ShowInsertCommunityInfo : function(ev) {
            hide_content();
            $(cell).hide();
            table_title.innerHTML = 'Comunidade de Inserção';
            table_content.innerHTML = '<h4>Enquadramento</h4><p style="font-size: 16px; font-weight: normal;">Resposta social com ou sem alojamento, que compreende um conjunto de ações integradas com vista à inserção social de pessoas e famílias vulneráveis que necessitam de apoio na sua integração social (mães solteiras, ex-reclusos e sem-abrigo) que, por determinados fatores, se encontram em situação de exclusão ou de marginalização social.</p<p style="font-size: 16px; font-weight: normal;">Fonte: <a href="http://www.seg-social.pt">http://www.seg-social.pt</a></p>';
            $(main_container).show();
            $('.insert_community_tbody').show();
        },

        _ShowCATInfo : function(ev) {
            hide_content();
            $(cell).hide();
            table_title.innerHTML = 'Centro de Alojamento Temporário (CAT)';
            table_content.innerHTML = '<h4>Enquadramento</h4><p style="font-size: 16px; font-weight: normal;">Resposta social que visa o acolhimento, por um período de tempo limitado, de pessoas adultas em situação de carência, tendo em vista o encaminhamento para a resposta social mais adequada.</p><p style="font-size: 16px; font-weight: normal;">Fonte: <a href="http://www.seg-social.pt">http://www.seg-social.pt</a></p>';
            $(main_container).show();
            $('.cat_tbody').show();
        },

        _ShowFoodAIDInfo : function(ev) {
            hide_content();
            $(cell).hide();
            table_title.innerHTML = 'Ajuda Alimentar';
            table_content.innerHTML = '<h4>Enquadramento</h4><p style="font-size: 16px; font-weight: normal;">Resposta social que proporciona a distribuição de géneros alimentícios, através de associações ou de entidades sem fins lucrativos, contribuindo para a resolução de situações de carência alimentar de pessoas e famílias desfavorecidas.</p><p style="font-size: 16px; font-weight: normal;">Fonte: <a href="http://www.seg-social.pt">http://www.seg-social.pt</a></p>';
            $(main_container).show();
            $('.food_aid_tbody').show();
        },

        _ShowCAAPInfo : function(ev) {
            hide_content();
            $(cell).hide();
            table_title.innerHTML = 'Centro de Atendimento/Acompanhamento Psicossocial (CAAP)';
            table_content.innerHTML = '<h4>Enquadramento</h4><p style="font-size: 16px; font-weight: normal;">Resposta social dirigida a pessoas infetadas com o VIH/SIDA e suas famílias, orientada para o atendimento, acompanhamento e ocupação em regime diurno</p><p style="font-size: 16px; font-weight: normal;">Fonte: <a href="http://www.seg-social.pt">http://www.seg-social.pt</a></p>';
            $(main_container).show();
            $('.caap_tbody').show();
        },

        _ShowFamilySADInfo : function(ev) {
            hide_content();
            $(cell).hide();
            table_title.innerHTML = 'Serviço de Apoio Domiciliário (SAD)';
            table_content.innerHTML = '<h4>Enquadramento</h4><p style="font-size: 16px; font-weight: normal;">Resposta social que consiste na prestação de cuidados e serviços a famílias e ou pessoas que se encontrem no seu domicílio, em situação de dependência física e ou psíquica e que não possam assegurar, temporária ou permanentemente, a satisfação das suas necessidades básicas e ou a realização das atividades instrumentais da vida diária, nem disponham de apoio familiar para o efeito.</p><p style="font-size: 16px; font-weight: normal;">Fonte: <a href="http://www.seg-social.pt">http://www.seg-social.pt</a></p>';
            $(main_container).show();
            $('.aids_sad_tbody').show();
        },

        _ShowResidenceInfectedInfo : function(ev) {
            hide_content();
            $(cell).hide();
            table_title.innerHTML = 'Residência para Pessoas Infetadas Pelo VIH/SIDA';
            table_content.innerHTML = '<h4>Enquadramento</h4><p style="font-size: 16px; font-weight: normal;">Resposta social que visa o alojamento de pessoas infetadas com o VIH/SIDA, em rutura familiar e desfavorecimento socioeconómico.</p><p style="font-size: 16px; font-weight: normal;">Fonte: <a href="http://www.seg-social.pt">http://www.seg-social.pt</a></p>';
            $(main_container).show();
            $('.residence_infected_tbody').show();
        },

        _ShowDirectTeamInfo : function(ev) {
            hide_content();
            $(cell).hide();
            table_title.innerHTML = 'Equipa de Intervenção Direta';
            table_content.innerHTML = '<h4>Enquadramento</h4><p style="font-size: 16px; font-weight: normal;">Serviço constituído por unidades de intervenção junto da população toxicodependente, suas famílias e comunidades afetadas pela toxicodependência.</p><p style="font-size: 16px; font-weight: normal;">Fonte: <a href="http://www.seg-social.pt">http://www.seg-social.pt</a></p>';
            $(main_container).show();
            $('.direct_team_tbody').show();
        },

        _ShowReintegrationProgramInfo : function(ev) {
            hide_content();
            $(cell).hide();
            table_title.innerHTML = 'Apartamento de Reinserção Social';
            table_content.innerHTML = '<h4>Enquadramento</h4><p style="font-size: 16px; font-weight: normal;">Alojamento temporário para pessoas toxicodependentes que, após a saída de unidades de tratamento, de estabelecimento prisional, de centros tutelares ou de outros estabelecimentos da área da justiça, tenham dificuldades de reintegração na família ou comunidade, na escola ou no trabalho.</p><p style="font-size: 16px; font-weight: normal;">Fonte: <a href="http://www.seg-social.pt">http://www.seg-social.pt</a></p>';
            $(main_container).show();
            $('.reintegration_program_tbody').show();
        },

        _ShowCallCenterInfo : function(ev) {
            hide_content();
            $(cell).hide();
            table_title.innerHTML = 'Centro de Atendimento';
            table_content.innerHTML = '<h4>Enquadramento</h4><p style="font-size: 16px; font-weight: normal;">Resposta desenvolvida através de um serviço constituído por uma ou mais equipas técnicas e pluridisciplinares,que assegura o atendimento, apoio e reencaminhamento das vítimas de violência doméstica, independentemente do sexo, tendo em vista a sua proteção.</p><p style="font-size: 16px; font-weight: normal;">Fonte: <a href="http://www.seg-social.pt">http://www.seg-social.pt</a></p>';
            $(main_container).show();
            $('.call_center_tbody').show();
        },

        _ShowShelterHouseInfo : function(ev) {
            hide_content();
            $(cell).hide();
            table_title.innerHTML = 'Casa de Abrigo';
            table_content.innerHTML = '<h4>Enquadramento</h4><p style="font-size: 16px; font-weight: normal;">Resposta social que consiste no acolhimento temporário a mulheres vítimas de violência doméstica, acompanhadas ou não de filhos menores, que não possam, por questões de segurança, permanecer nas suas residências habituais.</p><p style="font-size: 16px; font-weight: normal;">Fonte: <a href="http://www.seg-social.pt">http://www.seg-social.pt</a></p>';
            $(main_container).show();
            $('.shelter_house_tbody').show();
        },

        _ShowChildrenSupportInfo : function(ev) {
            hide_content();
            $(cell).hide();
            table_title.innerHTML = 'Apoio Domiciliário Para Guarda De Crianças';
            table_content.innerHTML = '<h4>Enquadramento</h4><p style="font-size: 16px; font-weight: normal;">Serviço prestado por pessoas enquadradas por uma instituição que, por conta própria, mediante pagamento pecuniário, se deslocam ao domicílio para prestação de cuidados individuais a crianças, durante um determinado período de tempo, fora dos horários dos equipamentos tradicionais e de acordo com as necessidades da família.</p><p style="font-size: 16px; font-weight: normal;">Fonte: <a href="http://www.cartasocial.pt/">http://www.cartasocial.pt/</a></p>';
            $(main_container).show();
            $('.children_support_tbody').show();
        },

        _ShowAmbulatoryCareInfo : function(ev) {
            hide_content();
            $(cell).hide();
            table_title.innerHTML = 'Apoio em Regime Ambulatório';
            table_content.innerHTML = '<h4>Enquadramento</h4><p style="font-size: 16px; font-weight: normal;">Resposta social, desenvolvida através de um serviço/equipamento, destinada ao apoio de pessoas com deficiência, a partir dos 7 anos, suas famílias e técnicos da comunidade, que desenvolve atividades de avaliação orientação e intervenção terapêutica e socioeducativa, promovidas por equipas transdisciplinares.</p><p style="font-size: 16px; font-weight: normal;">Fonte: <a href="http://www.cartasocial.pt/">http://www.cartasocial.pt/</a></p>';
            $(main_container).show();
            $('.ambulatory_care_tbody').show();
        },

        _ShowBraillePressInfo : function(ev) {
            hide_content();
            $(cell).hide();
            table_title.innerHTML = 'Imprensa Braille';
            table_content.innerHTML = '<h4>Enquadramento</h4><p style="font-size: 16px; font-weight: normal;">Serviço de apoio a crianças, jovens e adultos com deficiência visual, que se destina a produzir, adaptar e editar a produzir, adaptar e editar livros em Braille, de suporte ao processo de ensino/aprendizagem, assim como às atividades de natureza cultural e recreativa.</p><p style="font-size: 16px; font-weight: normal;">Fonte: <a href="http://www.cartasocial.pt/">http://www.cartasocial.pt/</a></p>';
            $(main_container).show();
            $('.braille_press_tbody').show();
        },

        _ShowSchoolDogsInfo : function(ev) {
            hide_content();
            $(cell).hide();
            table_title.innerHTML = 'Escolas de Cães-Guia';
            table_content.innerHTML = '<h4>Enquadramento</h4><p style="font-size: 16px; font-weight: normal;">Equipamento onde se desenvolvem atividades de formação, educação e treino de cães-guia para apoio à pessoa cega.</p><p style="font-size: 16px; font-weight: normal;">Fonte: <a href="http://www.cartasocial.pt/">http://www.cartasocial.pt/</a></p>';
            $(main_container).show();
            $('.school_dogs_tbody').show();
        },

        _ShowCRIInfo : function(ev) {
            hide_content();
            $(cell).hide();
            table_title.innerHTML = 'Centros de Recursos para a Inclusão (CRI)';
            table_content.innerHTML = '<h4>Enquadramento</h4><p style="font-size: 16px; font-weight: normal;">São serviços especializados existentes na comunidade, acreditados pelo Ministério da Educação, que apoiam e intensificam a capacidade da escola na promoção do sucesso educativo de todos os alunos.</p><p style="font-size: 16px; font-weight: normal;">Fonte: <a href="https://www.dge.mec.pt">https://www.dge.mec.pt</a></p>';
            $(main_container).show();
            $('.cri_tbody').show();
        }
    });
});
    