odoo.define('clds.clds_website_appeal', function (require) {
    "use strict";

    let core = require('web.core');
    let rpc = require("web.rpc");
    let Widget = require('web.Widget');
    let publicWidget = require('web.public.widget');


    publicWidget.registry.ModalPopUp = publicWidget.Widget.extend({
        selector: '.appeals_container',

        events: {
            'click .o_open_modal_btn' : '_OpenModal',
            'keyup #searchInput' : '_TableSearch'
        },

        start: function() {
            console.log('Reaching the start function');
            return this._super.apply(this, arguments);
        },

        destroy: function () {
            console.log('Reaching the destroy function');
            this.instance.setElement(null);
            this._super.apply(this, arguments);
            this.instance.setElement(this.$el);
        },

        _OpenModal: function(ev) {
            console.log('Open modal!');

            let self = this;
            let btn = ev.currentTarget;
            let appeal_id = btn.childNodes[1].innerHTML;
            let context = btn.childNodes[3].innerHTML;

            $('#appeal_id').val(appeal_id);
            $('#appeal_id').attr('placeholder',appeal_id);
            $('#appeal_id').css('display', 'none');
            $('#modal_title').html('Apelo #' + appeal_id);
            $('#modal_context').html(context);
            $('#appeal_modal').modal("show");

            
            $('#submit_btn').bind("click",function() 
            { 
                var imgVal = $('#file-upload').val(); 
                if(imgVal=='') { 
                    $('#image_alert').show().delay(5000).fadeOut();
                }
                else {
                    $('#image_alert').hide();
                }
                return true;
            });
        },

        _TableSearch: function(ev) {

            console.log(ev);
            console.log("WORKING!");

            var td, i, txtValue;
            let input = document.getElementById("searchInput");
            let filter = input.value.toUpperCase();
            let table = document.getElementById("appeal_table");
            var tr = table.getElementsByTagName("tr");
            for (i = 0; i < tr.length; i++) {
                td = tr[i].getElementsByTagName("td")[0];
                if (td) {
                txtValue = td.textContent || td.innerText;
                if (txtValue.toUpperCase().indexOf(filter) > -1) {
                    tr[i].style.display = "";
                } else {
                    tr[i].style.display = "none";
                }
                }       
            }
        }
    });
});
