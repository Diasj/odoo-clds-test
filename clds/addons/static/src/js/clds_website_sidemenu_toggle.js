odoo.define('clds.clds_website_sidemenu_toggle', function (require) {

    var ajax = require('web.ajax');
    var core = require('web.core');
    var Dialog = require('web.Dialog');
    var Widget = require('web.Widget');
    var publicWidget = require('web.public.widget');
    var rpc = require('web.rpc');
    
    var _t = core._t;
    
    publicWidget.registry.ToggleMore = publicWidget.Widget.extend({
        selector: '#wresources_collapse',
        events: {
            'click #toggle_more': '_ToggleMore',
        },

        start: function () {
            return this._super.apply(this, arguments);
        },
     
        _ToggleMore: function(ev){
            var self = this;
            var class_list = ev.currentTarget.classList;
            classes = class_list.value.split(" ")
            var current_target = $(ev.currentTarget)

            if ( classes.includes("resource_collapse") ){
                current_target.removeClass('resource_collapse');
                current_target.removeClass('fa-chevron-right');
                current_target.addClass('fa-chevron-down');
            } else {
                current_target.addClass('resource_collapse');
                current_target.removeClass('fa-chevron-down');
                current_target.addClass('fa-chevron-right');
            }
        },
    });
});
    