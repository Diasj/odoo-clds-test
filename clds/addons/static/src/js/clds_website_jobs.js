odoo.define('clds.clds_jobs_index', function (require) {
    "use strict";

    let core = require('web.core');
    let rpc = require("web.rpc");
    let Widget = require('web.Widget');
    let publicWidget = require('web.public.widget');

    publicWidget.registry.ModalPopUp = publicWidget.Widget.extend({
        selector: '#testt',

        events: {
            'click #btnaddmarker' : '_Marker'
        },

        start: function() {
            console.log("INIT JS 2");
            return this._super.apply(this, arguments);
        },

        destroy: function () {
            this.instance.setElement(null);
            this._super.apply(this, arguments);
            this.instance.setElement(this.$el);
        },

        _Marker: function(ev) {
            console.log("Working");

            /* 
            
            var latlng = new google.maps.LatLng(42.745334, 12.738430);

            function addmarker(latilongi) {
                var marker = new google.maps.Marker({
                    position: latilongi,
                    title: 'new marker',
                    draggable: true,
                    map: map
                });
                map.setCenter(marker.getPosition())
            }

            $('#btnaddmarker').on('click', function() {
                addmarker(latlng)
            })

            */

        },
    });
});
