odoo.define('clds.clds_user_menu', function (require) {
    "use strict";
    
    var UserMenu = require('web.UserMenu');
    var CldsUserMenu = UserMenu.include({
        _onMenuSupport: function () {
            window.open('https://boldint.com/', '_blank');
        },
    });
    return CldsUserMenu;
});
