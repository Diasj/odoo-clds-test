odoo.define('clds.clds_multi_select', function (require) {
    var core = require('web.core');
    var Widget = require('web.Widget');
    var publicWidget = require('web.public.widget');
    var rpc = require('web.rpc');

    var _t = core._t;

    publicWidget.registry.MultiSelect = publicWidget.Widget.extend({
        selector: '.multi_select',

        start: function () {
            return this._super.apply(this, arguments).then(function (){
                $('.multi_select').multiselect({
                    includeSelectAllOption: true,
                });
            })
        },
    });
});