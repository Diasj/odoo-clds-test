odoo.define('clds.clds_website_social_programs_toogle', function (require) {
    "use strict";

    var ajax = require('web.ajax');
    var core = require('web.core');
    var Dialog = require('web.Dialog');
    var Widget = require('web.Widget');
    var publicWidget = require('web.public.widget');
    var rpc = require('web.rpc');

    publicWidget.registry.getSocialProgramInfo = publicWidget.Widget.extend({
        selector: '.social_programs_container',
        events: {
            'click .o_get_info_btn' : '_FetchExtraInfo',
        },

        start: function () {
            return this._super.apply(this, arguments);
        },
     
        _FetchExtraInfo: function(ev){
            // ! Array values must be altered if more arguments are sent from the button
            var self = this;
            let btn = ev.currentTarget;
            let hidden_info = btn.parentElement.parentElement.parentElement.nextElementSibling;

            if ($(hidden_info).is(":hidden")) {
                let description = btn.childNodes[1].innerHTML;
                let promoting_entity = btn.childNodes[3].innerHTML;
                let beneficiary_entity = btn.childNodes[5].innerHTML;
                let email = btn.childNodes[7].innerHTML;
                let website = btn.childNodes[9].innerHTML;
                let contact = btn.childNodes[11].innerHTML;
                let social_media = btn.childNodes[13].innerHTML;

                hidden_info.children[0].children[1].innerHTML = description;
                hidden_info.children[1].children[1].innerHTML = promoting_entity;
                hidden_info.children[2].children[1].innerHTML = beneficiary_entity;
                hidden_info.children[3].children[1].innerHTML = email;
                hidden_info.children[3].children[3].innerHTML = website;
                hidden_info.children[4].children[1].innerHTML = contact;
                hidden_info.children[4].children[3].innerHTML = social_media;
                $(hidden_info).show();
                //hidden_info.style.display = "inline-block";
                //hidden_info.style.display = "table";
                btn.style.borderColor = "red";
                btn.style.backgroundColor = "red";
                btn.lastElementChild.innerHTML = "-";
            }
            else {
                //hidden_info.style.display = "none";
                $(hidden_info).hide();
                btn.style.backgroundColor = "#64b1ba";
                btn.style.borderColor = "#64b1ba";
                btn.lastElementChild.innerHTML = "+";
            }
        }
    });

    /* 

            if (btn.style.backgroundColor == "rgb(100, 177, 186)") {
                var row = table.insertRow(counter);

                var cell1 = row.insertCell(0);
                var cell2 = row.insertCell(1);
                cell1.innerHTML = "NEW CELL1";
                cell2.innerHTML = "NEW CELL2";

                btn.style.backgroundColor = "red";

                counter ++;
                console.log("gdssssssssssssssssssssssssssssssf: " + counter);
            }
            else {
                counter--;
                table.deleteRow(parseInt(counter) + 1);
                btn.style.backgroundColor = "#64b1ba";
                counter--;
                console.log("Counter: " + counter);
            }

            */

    /* 

    var rowCount = $("#social_programs_table tr").length;
    console.log("Total Row Count: " + rowCount);
    var ajax = require('web.ajax');

    var FetchExtraInfo = Widget.extend({
        events: {
            'click .o_get_info_btn' : '_FetchExtraInfo'
        },

        start: function () {
            console.log("start");
            return this._super.apply(this, arguments);
        },

        _FetchExtraInfo: function(ev) {
            console.log("FETCH FUNC");
            var self = this;
            var class_list = ev.currentTarget.classList;
            var index_class = class_list[class_list.length - 1];
            var val = document.getElementsByClassName("oe_program_id" + index_class);

            var obj_id = val[0].innerHTML;
            id_args = [[obj_id]];

            rpc.query({
                model: 'resource.social.program',
                method: 'search_read',
                args: id_args,
            }).then(queryResult => {
                console.log("QUERY RESULT: " + queryResult);
            });
        }
    });

    publicWidget.registry.getSocialProgramInfo = publicWidget.Widget.extend({
        selector: '#social_programs_container',

        start: function() {
            var def = this._super.apply(this, arguments);
            this.instance = new FetchExtraInfo(this);
            return Promise.all([def, this.instance.attachTo(this.$el)]);
        },

    
        destroy: function () {
            this.instance.setElement(null);
            this._super.apply(this, arguments);
            this.instance.setElement(this.$el);
        },
    });

    return FetchExtraInfo;

    */

});

    /*

    let getInfo = () => {
        var val = document.getElementsByClassName("obj_id_class")[0].innerHTML;
        ajax.jsonRpc(
            "/social-programs/info",
            'call',
            {'obj_id' : val}).then(function(data) {
            if (data) {
                console.log(data);
            }
            else {
                console.log("Error!");
            }
        })
    }
    
    
    console.log(document.getElementsByClassName("test_name")[0].innerHTML); 

    var table = document.getElementById("social_programs_table");
    var incr = 0;

    //i = 1, because the first row should be ignored 
     
    for (var i = 1, row; row = table.rows[i]; i++) {
        //iterate through rows
        if (i % 2 != 0) {
            incr++;
            row.cells[5].firstChild.nextElementSibling.dataset.target = "target" + incr;
            row.cells[5].firstChild.nextElementSibling.id = "collapse_btn" + incr;
            console.log("BTN ID: " + row.cells[5].firstChild.nextElementSibling.id);
            console.log("Row Target ID: " + row.cells[5].firstChild.nextElementSibling.dataset.target);
        }
        else {
            row.id = "target" + incr;
            console.log("Row ID: " + row.id);
        }
    } */
