from odoo import http, models, fields, tools, api, _, SUPERUSER_ID
from odoo.http import request
from odoo.exceptions import ValidationError, Warning, UserError
from odoo.addons.portal.controllers.web import Home
import random

class Website(Home):
    @http.route('/', type='http', auth="public", website=True)
    def index(self, **kw):
        homepage = request.website.sudo().homepage_id
        values = {
            'website_key': homepage.key
        }

        if homepage.key == 'clds.clds_homepage_aveiro_em_rede':
            #! Prepare data for Aveiro em Rede website
            Entity = request.env['res.company'].sudo()
            Resource = request.env['fleet.vehicle'].sudo()
            RandomResources = request.env['fleet.vehicle'].sudo()
            
            entity_count = Entity.search_count([('parent_id','!=',False)])
            resource_ids = Resource.search([
                ('resource_status', '=', 'shared')
            ])
            resource_count = len(resource_ids)
            
            if resource_count < 4:
                RandomResources = resource_ids
            else:
                while len(RandomResources) < 4:
                    try:
                        index = random.randint(0, resource_count - 1)
                        if resource_ids[index].id not in RandomResources.ids:
                            RandomResources |= resource_ids[index]
                    except:
                        pass

            ParishContent = request.env['res.parish'].sudo()

            print("Content")

            import pdb
            pdb.set_trace()

            print(ParishContent)

            values.update({
                'resource_ids': RandomResources,
                'resource_number': resource_count,
                'entity_number': entity_count,
                'parish': ParishContent
            })

        elif homepage.key == 'clds.clds_homepage_aveiro_voluntario':
            #! Prepare data for Aveiro voluntario website
            values = {}

        if homepage and (homepage.is_visible or request.env.user.has_group('base.group_user')) and homepage.url != '/':
            return request.env['ir.http'].reroute(homepage.url)

        website_page = request.env['ir.http']._serve_page(values)
        if website_page:
            return website_page
        else:
            top_menu = request.website.menu_id
            first_menu = top_menu and top_menu.child_id and top_menu.child_id.filtered(lambda menu: menu.is_visible)
            if first_menu and first_menu[0].url not in ('/', '', '#') and (not (first_menu[0].url.startswith(('/?', '/#', ' ')))):
                return request.redirect(first_menu[0].url)

        raise request.not_found()