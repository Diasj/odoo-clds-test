# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.
import logging
import werkzeug

from odoo import http, models, fields, tools, api, _, SUPERUSER_ID
from odoo.addons.auth_signup.models.res_users import SignupError
from odoo.addons.base_setup.controllers.main import BaseSetup
from odoo.addons.auth_signup.controllers.main import AuthSignupHome, ensure_db
from odoo.exceptions import UserError
from odoo.http import request
import copy

_logger = logging.getLogger(__name__)


class AuthSignupHome(AuthSignupHome):

    def do_signup(self, qcontext):
        """ Shared helper that creates a res.partner out of a token """
        values = {
            key: qcontext.get(key) for key in ('login', 'name', 'password', 'vat', 'parish_id', 'cc_card','target_audience_ids','interest_ids')
        }
        reset_request = qcontext.get('reset_request')

        if not values:
            raise UserError(_("The form was not properly filled in."))

        if values.get('password') != qcontext.get('confirm_password'):
            raise UserError(_("Passwords do not match; please retype them."))

        supported_lang_codes = [code for code, _ in request.env['res.lang'].get_installed()]
        lang = request.context.get('lang', '').split('_')[0]

        if lang in supported_lang_codes:
            values['lang'] = lang

        if reset_request:
            values.pop('vat')
            values.pop('parish_id')
            values.pop('cc_card')
            values.pop('target_audience_ids')
            values.pop('interest_ids')
        if not reset_request:
            values.update({
                'vat': values.get('vat'),
                'parish_id': values.get('parish_id'),
                'cc_card': values.get('cc_card'),
                'target_audience_ids': [(6,0,values.get('target_audience_ids'))],
                'interest_ids': [(6,0,values.get('interest_ids'))],
            })

        self._signup_with_values(qcontext.get('token'), values)
        request.env.cr.commit()

    @http.route('/web/signup', type='http', auth='public', website=True, sitemap=False)
    def web_auth_signup(self, *args, **kw):
        reset_request = kw.get('request', False)
        qcontext = self.get_auth_signup_qcontext()
        qcontext["reset_request"] = reset_request
        qcontext["target_audience_ids"] = request.httprequest.form.getlist('target_audience_ids')
        qcontext["interest_ids"] = request.httprequest.form.getlist('interest_ids')

        if not qcontext.get('token') and not qcontext.get('signup_enabled'):
            raise werkzeug.exceptions.NotFound()

        if 'error' not in qcontext and request.httprequest.method == 'POST':
            try:
                self.do_signup(qcontext)
                # Send an account creation confirmation email
                if qcontext.get('token'):
                    User = request.env['res.users']
                    user_sudo = User.sudo().search(
                        User._get_login_domain(qcontext.get('login')), order=User._get_login_order(), limit=1
                    )

                    template = request.env.ref('auth_signup.mail_template_user_signup_account_created', raise_if_not_found=False)
                    if user_sudo and template:
                        template.sudo().with_context(
                            lang=user_sudo.lang,
                            auth_login=werkzeug.url_encode({'auth_login': user_sudo.email}),
                        ).send_mail(user_sudo.id, force_send=True)

                return self.web_login(*args, **kw)
            except UserError as e:
                qcontext['error'] = e.name or e.value
            
            except (SignupError, AssertionError) as e:
                if request.env["res.users"].sudo().search([("login", "=", qcontext.get("login"))]):
                    qcontext["error"] = _("Another user is already registered using this login.")
                else:
                    _logger.error("%s", e)
                    qcontext['error'] = _("Could not create a new account.")


        qcontext['target_audience_ids'] = request.env['target.audience'].search([])
        qcontext['interest_ids'] = request.env['interest'].search([])
        qcontext['parish_ids'] = request.env['res.parish'].search([])
        response = request.render('auth_signup.signup', qcontext)
        response.headers['X-Frame-Options'] = 'DENY'

        return response


    @http.route('/web/reset_password', type='http', auth='public', website=True, sitemap=False)
    def web_auth_reset_password(self, *args, **kw):
        reset_request = kw.get('request', False)
        qcontext = self.get_auth_signup_qcontext()
        qcontext["reset_request"] = reset_request

        if not qcontext.get('token') and not qcontext.get('reset_password_enabled'):
            raise werkzeug.exceptions.NotFound()

        if 'error' not in qcontext and request.httprequest.method == 'POST':
            try:
                if qcontext.get('token'):
                    self.do_signup(qcontext)
                    return self.web_login(*args, **kw)
                else:
                    login = qcontext.get('login')
                    assert login, _("No login provided.")
                    _logger.info(
                        "Password reset attempt for <%s> by user <%s> from %s",
                        login, request.env.user.login, request.httprequest.remote_addr)
                    request.env['res.users'].sudo().reset_password(login)
                    qcontext['message'] = _("An email has been sent with credentials to reset your password")
            except UserError as e:
                qcontext['error'] = e.name or e.value
            except SignupError:
                qcontext['error'] = _("Could not reset your password")
                _logger.exception('error when resetting password')
            except Exception as e:
                qcontext['error'] = str(e)

        response = request.render('auth_signup.reset_password', qcontext)
        response.headers['X-Frame-Options'] = 'DENY'
        return response