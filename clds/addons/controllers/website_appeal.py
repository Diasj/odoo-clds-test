# -*- coding: utf-8 -*-

import babel.dates
import base64
import re
import werkzeug

from datetime import datetime, timedelta
from dateutil.relativedelta import relativedelta

from odoo import fields, http, _
from odoo.addons.http_routing.models.ir_http import slug
from odoo.addons.website.controllers.main import QueryURL
from odoo.addons.website.models.ir_http import sitemap_qs2dom
from odoo.http import request


class WebsiteAppealController(http.Controller):
    @http.route('''/appeal''', type='http', auth="public", website=True)
    def appeal_all(self, **kwargs):
        content = request.env['appeal'].sudo().search([])
        print(len(content))
        print(len(request.env['appeal.assists'].sudo().search([])))
        values = {
            'appeals' : content,
            'total_appeals' : len(content),
            'total_assists' : len(request.env['appeal.assists'].sudo().search([]))
        }
        return request.render('clds.appeal', values)


    @http.route('''/appeal/submission/''', type='http', auth="public", website=True, csrf=False)
    def appeal_assist_submission(self, **kwargs):
        cr, uid, context = request.cr, request.uid, request.context
        print(kwargs)
        if kwargs.get('name') and kwargs.get('contact') and kwargs.get('nif') and kwargs.get('contact') and kwargs.get('address') and kwargs.get('email') and kwargs.get('transport_select') and kwargs.get('donation_select'):
            attributes={
                'name': kwargs.get('name'),
                'appeal_id': int(re.sub('\s+',' ',kwargs.get('appeal'))),
                'nif' : int(kwargs.get('nif')),
                'contact' : int(kwargs.get('contact')),
                'address' : kwargs.get('address'),
                'email' : kwargs.get('email'),
                'transport_request' : kwargs.get('transport_select'),
                'receipt' : kwargs.get('donation_select')}  
            if kwargs.get('observations'):
                attributes['observations'] = re.sub('\s+',' ',kwargs.get('observations'))
                request.env['appeal.assists'].create(attributes)
            else:
                request.env['appeal.assists'].create(attributes)

            print(request.env['appeal.assists'].search([]))
        else:
            print("NOT WORKING????")
        return request.redirect('/appeal')

    

