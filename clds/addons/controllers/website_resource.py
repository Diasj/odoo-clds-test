# -*- coding: utf-8 -*-

import babel.dates
import base64
import re
import werkzeug

from werkzeug.datastructures import OrderedMultiDict
from io import BytesIO  

from datetime import datetime, timedelta
from dateutil.relativedelta import relativedelta

from odoo import fields, http, _
from odoo.addons.http_routing.models.ir_http import slug
from odoo.addons.website.controllers.main import QueryURL
from odoo.addons.website.models.ir_http import sitemap_qs2dom
from odoo.http import request
from odoo.tools.misc import get_lang
from odoo.osv import expression


class WebsiteResourceController(http.Controller):
    def sitemap_shop(env, rule, qs):
        if not qs or qs.lower() in '/resources':
            yield {'loc': '/resources'}
        
    #     Category = env['fleet.vehicle.category']
    #     dom = sitemap_qs2dom(qs, '/resources/category', Category._rec_name)
    #     dom += env['website'].get_current_website().website_domain()
    #     for cat in Category.search(dom):
    #         loc = '/resources/category/%s' % slug(cat)
    #         if not qs or qs.lower() in loc:
    #             yield {'loc': loc}


    def _get_search_domain(self, search, transport=False, brand=False, model=False, search_in_description=True):
        domains = []
        if search:
            for srch in search.split(" "):
                subdomains = [
                    [('name', 'ilike', srch)]
                ]
                if search_in_description:
                    subdomains.append([('description', 'ilike', srch)])
                domains.append(expression.OR(subdomains))
        
        if transport and not brand and not model:
            domains.append([
                ('brand_id.resource_type', 'in', ['vehicle','vehicle_with_driver']),
                ('model_id.model_resource_type', 'in', ['light','heavy','tcc'])
                
            ])
        elif brand and model:
            domains.append([
                ('brand_id.resource_type', '=', brand),
                ('model_id.model_resource_type', '=', model)
            ])
        elif brand:
            domains.append([
                ('brand_id.resource_type', '=', brand)
            ])
        
        # domains.append([
        #     ('resource_status', '=', 'shared')
        # ])

        return expression.AND(domains)


    @http.route([
        '''/resources''', 
        '''/resources/all''', 
        '''/resources/page/<int:page>''',
        '''/resources/brand/<string:brand>''',
        '''/resources/brand/<string:brand>/model/<string:model>''',
        '''/resources/transport/<string:transport>''',
        '''/resources/transport/<string:transport>/brand/<string:brand>''',
        '''/resources/transport/<string:transport>/brand/<string:brand>/model/<string:model>''',
        '''/resources/brand/<string:brand>/page/<int:page>''',
        '''/resources/brand/<string:brand>/model/<string:model>/page/<int:page>''',
        '''/resources/transport/<string:transport>/page/<int:page>''',
        '''/resources/transport/<string:transport>/brand/<string:brand>/page/<int:page>''',
        '''/resources/transport/<string:transport>/brand/<string:brand>/model/<string:model>/page/<int:page>''',
    ], type='http', auth="public", website=True, sitemap=sitemap_shop)
    def resources_all(self, page=0, transport=None, brand=None, model=None, search='', **post):
        domain = self._get_search_domain(search, transport=transport, brand=brand, model=model)

        request.context = dict(request.context, partner=request.env.user.partner_id)
        
        url = "/resources"
        if search:
            post["search"] = search
        
        Resource = request.env['fleet.vehicle'].sudo()
        search_resource = Resource.search(domain)

        if transport and brand and model:
            url = "/resources/transport/{transport}/brand/{brand}/model/{model}".format(transport=transport, brand=brand, model=model)
        elif brand and model:
            url = "/resources/brand/{brand}/model/{model}".format(brand=brand, model=model)
        elif brand:
            url = "/resources/brand/{brand}".format(brand=brand)

        resource_count = len(search_resource)

        pager = request.website.pager(url=url, total=resource_count, page=page, step=12, scope=5, url_args=post)
        resources = Resource.search(domain, limit=12, offset=pager['offset'])

        values = {
            'user_id': request.env.user,
            'search': search,
            'pager': pager,
            'transport': transport,
            'model': model,
            'brand': brand,
            'resource_ids': resources,
            'search_count': resource_count,
            # 'search_categories_ids': search_categories.ids,
        }

        return request.render('clds.resource_index', values)


    @http.route('''/resource/details/<model("fleet.vehicle"):resource_id>''', type='http', auth="public", website=True)
    def resource_detail(self, resource_id, **kwargs):
        user = request.env.user
        return request.render("clds.resource_details", {
            'user_id': user,
            'resource_id': resource_id.sudo(),
            'main_object': resource_id.sudo(),
        })


    @http.route('''/social-programs''', type='http', auth='public', website=True)
    def all_social_programs(self, **kwargs):
        #Creating new obj for testing purpouse
        """
        import random
        rand_value = random.randint(0, 10000000)
        attributes={
            'program_id': str(rand_value),
            'name': 'test_object' + str(rand_value),
            'description' : 'test_object_description' + str(rand_value),
            'promoting_entity' : 'test_object_promoting_entity' + str(rand_value),
            'beneficiary_entity' : 'test_object_beneficiary_entity' + str(rand_value),
            'email' : 'email' + str(rand_value),
            'website' : 'website' + str(rand_value),
            'contact' : rand_value,
            'social_media' : 'social_media' + str(rand_value),
            'duration' : rand_value,
            'target_audience' : 'target_audience' + str(rand_value),
            'financial_source' : 'financial_source' + str(rand_value)}
        #request.env['resource.social.program'].create(attributes)
        """
        Res = request.env['resource.social.program'].sudo()
        content = Res.search([])
        values = {
            'programs' : content
        }

        return request.render('clds.social_programs', values)


    #<model("resource.social.program"):program_id>
    @http.route(['/social-programs/info'], type='json', auth="public", website=True)
    def social_program_info(self, **kwargs):
        get_data = post.get('obj_id')
        target = request.env['resource.social.program'].search([('program_id','=',get_data)], limit=1)
        return ('clds.social_programs', {'data' : target})

    
    @http.route('''/resource-request/''', type='http', auth="public", website=True, csrf=False)
    def resource_request(self, **kwargs):
        if kwargs.get('name') and kwargs.get('contact') and kwargs.get('resource') and kwargs.get('message'):
            attributes={
                'requestor': kwargs.get('name'),
                'contact': kwargs.get('contact'),
                'requested_resource' : kwargs.get('resource'),
                'message' : kwargs.get('message'),
                'request_state' : 'submitted'}
            request.env['resource.request'].create(attributes)
            print(kwargs)
            print(request.env['resource.request'].search([]))
            return request.render('clds.clds_aveiro_voluntario_form_submition')
        else:
            print('Form did not work!')
            print(kwargs)
            return request.render('clds.clds_aveiro_voluntario_form_submition_fail')


    @http.route('''/filtered-resources/''', type='http', auth="public", website=True, csrf=False)
    def resource_search(self, **kwargs):
        values = {}
        if kwargs.get('resource') and kwargs.get('location'):
            return request.redirect("/test-filtered-resources?name={name}&location={location}".format(
                name = kwargs.get('resource'),
                location = kwargs.get('location')
            ))

        elif kwargs.get('location'):
            return request.redirect("/resources-filter?location={location}".format(
                location = kwargs.get('location')
            ))
        else:
            return request.redirect('clds.clds_aveiro_voluntario_form_submition_fail')


    @http.route([
        '''/resources-filter''',
        '''/resources-filter/all''',
        '''/resources-filter/page/<int:page>''',
        '''/resources-filter/location/<string:location>''',
        '''/resources-filter/name/<string:name>''',
        '''/resources-filter/name/<string:name>/location/<string:location>''',
        '''/resources-filter/location/<string:location>/page/<int:page>''',
        '''/resources-filter/name/<string:name>/page/<int:page>''',
        '''/resources-filter/name/<string:name>/location/<string:location>/page/<int:page>'''
    ], type='http', auth="public", website=True, csrf=False)
    def filtered_resource_url(self, **kwargs):
        print(kwargs)
        values = {}
        if kwargs.get('resource') and kwargs.get('location'):
            print("Resource and Location fetched")
            content = request.env['fleet.vehicle'].search([('location','=',kwargs.get('location'), '&', 'license_plate', '=', kwargs.get('resource'))])
            values.update({
                'resource_ids' : content
            })
        elif kwargs.get('location'):
            print("Only location fetched")
            content = request.env['fleet.vehicle'].search([('location','=',kwargs.get('location'))])
            values.update({
                'resource_ids' : content
            })
        else:
            return request.redirect('clds.clds_aveiro_voluntario_form_submition_fail')

        return request.render('clds.clds_aveiro_voluntario_form_submition_fail', values)

