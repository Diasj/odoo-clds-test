# -*- coding: utf-8 -*-

import babel.dates
import base64
import re
import werkzeug

from werkzeug.datastructures import OrderedMultiDict
from io import BytesIO  

from datetime import datetime, timedelta
from dateutil.relativedelta import relativedelta

from odoo import fields, http, _
from odoo.addons.http_routing.models.ir_http import slug
from odoo.addons.website.controllers.main import QueryURL
from odoo.addons.website.models.ir_http import sitemap_qs2dom
from odoo.http import request
from odoo.tools.misc import get_lang
from odoo.osv import expression


class WebsiteResourceController(http.Controller):
    def sitemap_shop(env, rule, qs):
        if not qs or qs.lower() in '/social-response':
            yield {'loc': '/social-response'}


    def _get_search_domain(self, search, subcategory=False, search_in_description=True):
        domains = []
        if search:
            for srch in search.split(" "):
                subdomains = [
                    [('name', 'ilike', srch)]
                ]
                if search_in_description:
                    subdomains.append([('description', 'ilike', srch)])
                domains.append(expression.OR(subdomains))
        
        if subcategory:
            domains.append([
                ('', '=', subcategory)
            ])

        return expression.AND(domains)


    @http.route('''/resource/details/<model("fleet.vehicle"):resource_id>''', type='http', auth="public", website=True)
    def resource_detail(self, resource_id, **kwargs):
        user = request.env.user
        return request.render("clds.resource_details", {
            'user_id': user,
            'resource_id': resource_id.sudo(),
            'main_object': resource_id.sudo(),
        })

    
    @http.route('''/social-response''', type="http", auth="public", website=True)
    def all_social_responses(self, **kwargs):

        values = dict()
        content = request.env['resource.social.response'].sudo().search([])

        values['sub_babysitter'] = dict()
        sub_babysitter_ids = content.filtered(lambda self: self.sub_children_youth == 'babysitter')
        values['sub_babysitter']['company_ids'] = sub_babysitter_ids.mapped('company_id').ids if sub_babysitter_ids.mapped('company_id') else []
        for sub_babysitter_id in sub_babysitter_ids:
            company_ids = sub_babysitter_id.mapped('company_id')
            for company_id in company_ids:
                values['sub_babysitter']['company_id_{0}'.format(company_id.id)] = dict()
                # * Filtering content
                sub_babysitter_capacity = content.filtered(lambda self, company_id=company_id: self.company_id.id == company_id.id).mapped('capacity')
                sub_babysitter_reimbursed_vacancies = content.filtered(lambda self, company_id=company_id: self.company_id.id == company_id.id).mapped('reimbursed_vacancies')
                sub_babysitter_non_reimbursed_vacancies = content.filtered(lambda self, company_id=company_id: self.company_id.id == company_id.id).mapped('non_reimbursed_vacancies')
                company_name = content.filtered(lambda self, company_id=company_id: self.company_id.id == company_id.id).mapped('company_id.name')
                company_parish = content.filtered(lambda self, company_id=company_id: self.company_id.id == company_id.id).mapped('parish_id.name')
                # * Sums
                sub_babysitter_capacity = sum(sub_babysitter_capacity)
                sub_babysitter_reimbursed_vacancies = sum(sub_babysitter_reimbursed_vacancies)
                sub_babysitter_non_reimbursed_vacancies = sum(sub_babysitter_non_reimbursed_vacancies)
                # * Adding to dict
                values['sub_babysitter']['company_id_{0}'.format(company_id.id)]['capacity'] = sub_babysitter_capacity
                values['sub_babysitter']['company_id_{0}'.format(company_id.id)]['reimbursed_vacancies'] = sub_babysitter_reimbursed_vacancies
                values['sub_babysitter']['company_id_{0}'.format(company_id.id)]['non_reimbursed_vacancies'] = sub_babysitter_non_reimbursed_vacancies
                values['sub_babysitter']['company_id_{0}'.format(company_id.id)]['company_name'] = company_name
                values['sub_babysitter']['company_id_{0}'.format(company_id.id)]['parish_name'] = company_parish


        values['sub_family_day_care'] = dict()
        sub_family_day_care_ids = content.filtered(lambda self: self.sub_children_youth == 'family_day_care')
        values['sub_family_day_care']['company_ids'] = sub_family_day_care_ids.mapped('company_id').ids if sub_family_day_care_ids.mapped('company_id') else []
        for sub_family_day_care_id in sub_family_day_care_ids:
            company_ids = sub_family_day_care_id.mapped('company_id')
            for company_id in company_ids:
                values['sub_family_day_care']['company_id_{0}'.format(company_id.id)] = dict()
                # * Filtering content
                sub_capacity = content.filtered(lambda self, company_id=company_id: self.company_id.id == company_id.id).mapped('capacity')
                sub_reimbursed_vacancies = content.filtered(lambda self, company_id=company_id: self.company_id.id == company_id.id).mapped('reimbursed_vacancies')
                sub_non_reimbursed_vacancies = content.filtered(lambda self, company_id=company_id: self.company_id.id == company_id.id).mapped('non_reimbursed_vacancies')
                company_name = content.filtered(lambda self, company_id=company_id: self.company_id.id == company_id.id).mapped('company_id.name')
                company_parish = content.filtered(lambda self, company_id=company_id: self.company_id.id == company_id.id).mapped('parish_id.name')
                # * Sums
                sub_capacity = sum(sub_capacity)
                sub_reimbursed_vacancies = sum(sub_reimbursed_vacancies)
                sub_non_reimbursed_vacancies = sum(sub_non_reimbursed_vacancies)
                # * Adding to dict
                values['sub_family_day_care']['company_id_{0}'.format(company_id.id)]['capacity'] = sub_capacity
                values['sub_family_day_care']['company_id_{0}'.format(company_id.id)]['reimbursed_vacancies'] = sub_reimbursed_vacancies
                values['sub_family_day_care']['company_id_{0}'.format(company_id.id)]['non_reimbursed_vacancies'] = sub_non_reimbursed_vacancies
                values['sub_family_day_care']['company_id_{0}'.format(company_id.id)]['company_name'] = company_name
                values['sub_family_day_care']['company_id_{0}'.format(company_id.id)]['parish_name'] = company_parish

        
        values['sub_nursery'] = dict()
        sub_nursery_ids = content.filtered(lambda self: self.sub_children_youth == 'nursery')
        values['sub_nursery']['company_ids'] = sub_nursery_ids.mapped('company_id').ids if sub_nursery_ids.mapped('company_id') else []
        for sub_nursery_id in sub_nursery_ids:
            company_ids = sub_nursery_id.mapped('company_id')
            for company_id in company_ids:
                values['sub_nursery']['company_id_{0}'.format(company_id.id)] = dict()
                # * Filtering content
                sub_capacity = content.filtered(lambda self, company_id=company_id: self.company_id.id == company_id.id).mapped('capacity')
                sub_reimbursed_vacancies = content.filtered(lambda self, company_id=company_id: self.company_id.id == company_id.id).mapped('reimbursed_vacancies')
                sub_non_reimbursed_vacancies = content.filtered(lambda self, company_id=company_id: self.company_id.id == company_id.id).mapped('non_reimbursed_vacancies')
                company_name = content.filtered(lambda self, company_id=company_id: self.company_id.id == company_id.id).mapped('company_id.name')
                company_parish = content.filtered(lambda self, company_id=company_id: self.company_id.id == company_id.id).mapped('parish_id.name')
                # * Sums
                sub_capacity = sum(sub_capacity)
                sub_reimbursed_vacancies = sum(sub_reimbursed_vacancies)
                sub_non_reimbursed_vacancies = sum(sub_non_reimbursed_vacancies)
                # * Adding to dict
                values['sub_nursery']['company_id_{0}'.format(company_id.id)]['capacity'] = sub_capacity
                values['sub_nursery']['company_id_{0}'.format(company_id.id)]['reimbursed_vacancies'] = sub_reimbursed_vacancies
                values['sub_nursery']['company_id_{0}'.format(company_id.id)]['non_reimbursed_vacancies'] = sub_non_reimbursed_vacancies
                values['sub_nursery']['company_id_{0}'.format(company_id.id)]['company_name'] = company_name
                values['sub_nursery']['company_id_{0}'.format(company_id.id)]['parish_name'] = company_parish

        
        values['sub_eepe'] = dict()
        sub_eepe_ids = content.filtered(lambda self: self.sub_children_youth == 'eepe')
        values['sub_eepe']['company_ids'] = sub_eepe_ids.mapped('company_id').ids if sub_eepe_ids.mapped('company_id') else []
        for sub_eepe_id in sub_eepe_ids:
            company_ids = sub_eepe_id.mapped('company_id')
            for company_id in company_ids:
                values['sub_eepe']['company_id_{0}'.format(company_id.id)] = dict()
                # * Filtering content
                sub_capacity = content.filtered(lambda self, company_id=company_id: self.company_id.id == company_id.id).mapped('capacity')
                sub_reimbursed_vacancies = content.filtered(lambda self, company_id=company_id: self.company_id.id == company_id.id).mapped('reimbursed_vacancies')
                sub_non_reimbursed_vacancies = content.filtered(lambda self, company_id=company_id: self.company_id.id == company_id.id).mapped('non_reimbursed_vacancies')
                company_name = content.filtered(lambda self, company_id=company_id: self.company_id.id == company_id.id).mapped('company_id.name')
                company_parish = content.filtered(lambda self, company_id=company_id: self.company_id.id == company_id.id).mapped('parish_id.name')
                # * Sums
                sub_capacity = sum(sub_capacity)
                sub_reimbursed_vacancies = sum(sub_reimbursed_vacancies)
                sub_non_reimbursed_vacancies = sum(sub_non_reimbursed_vacancies)
                # * Adding to dict
                values['sub_eepe']['company_id_{0}'.format(company_id.id)]['capacity'] = sub_capacity
                values['sub_eepe']['company_id_{0}'.format(company_id.id)]['reimbursed_vacancies'] = sub_reimbursed_vacancies
                values['sub_eepe']['company_id_{0}'.format(company_id.id)]['non_reimbursed_vacancies'] = sub_non_reimbursed_vacancies
                values['sub_eepe']['company_id_{0}'.format(company_id.id)]['company_name'] = company_name
                values['sub_eepe']['company_id_{0}'.format(company_id.id)]['parish_name'] = company_parish
            
        
        values['sub_catl'] = dict()
        sub_catl_ids = content.filtered(lambda self: self.sub_children_youth == 'eepe')
        values['sub_catl']['company_ids'] = sub_catl_ids.mapped('company_id').ids if sub_catl_ids.mapped('company_id') else []
        for sub_catl_id in sub_catl_ids:
            company_ids = sub_catl_id.mapped('company_id')
            for company_id in company_ids:
                values['sub_catl']['company_id_{0}'.format(company_id.id)] = dict()
                # * Filtering content
                sub_capacity = content.filtered(lambda self, company_id=company_id: self.company_id.id == company_id.id).mapped('capacity')
                sub_reimbursed_vacancies = content.filtered(lambda self, company_id=company_id: self.company_id.id == company_id.id).mapped('reimbursed_vacancies')
                sub_non_reimbursed_vacancies = content.filtered(lambda self, company_id=company_id: self.company_id.id == company_id.id).mapped('non_reimbursed_vacancies')
                company_name = content.filtered(lambda self, company_id=company_id: self.company_id.id == company_id.id).mapped('company_id.name')
                company_parish = content.filtered(lambda self, company_id=company_id: self.company_id.id == company_id.id).mapped('parish_id.name')
                # * Sums
                sub_capacity = sum(sub_capacity)
                sub_reimbursed_vacancies = sum(sub_reimbursed_vacancies)
                sub_non_reimbursed_vacancies = sum(sub_non_reimbursed_vacancies)
                # * Adding to dict
                values['sub_catl']['company_id_{0}'.format(company_id.id)]['capacity'] = sub_capacity
                values['sub_catl']['company_id_{0}'.format(company_id.id)]['reimbursed_vacancies'] = sub_reimbursed_vacancies
                values['sub_catl']['company_id_{0}'.format(company_id.id)]['non_reimbursed_vacancies'] = sub_non_reimbursed_vacancies
                values['sub_catl']['company_id_{0}'.format(company_id.id)]['company_name'] = company_name
                values['sub_catl']['company_id_{0}'.format(company_id.id)]['parish_name'] = company_parish

        
        values['sub_ipi'] = dict()
        sub_ipi_ids = content.filtered(lambda self: self.sub_children_youth_dis == 'ipi')
        values['sub_ipi']['company_ids'] = sub_ipi_ids.mapped('company_id').ids if sub_ipi_ids.mapped('company_id') else []
        for sub_ipi_id in sub_ipi_ids:
            company_ids = sub_ipi_id.mapped('company_id')
            for company_id in company_ids:
                values['sub_ipi']['company_id_{0}'.format(company_id.id)] = dict()
                # * Filtering content
                sub_capacity = content.filtered(lambda self, company_id=company_id: self.company_id.id == company_id.id).mapped('capacity')
                sub_reimbursed_vacancies = content.filtered(lambda self, company_id=company_id: self.company_id.id == company_id.id).mapped('reimbursed_vacancies')
                sub_non_reimbursed_vacancies = content.filtered(lambda self, company_id=company_id: self.company_id.id == company_id.id).mapped('non_reimbursed_vacancies')
                company_name = content.filtered(lambda self, company_id=company_id: self.company_id.id == company_id.id).mapped('company_id.name')
                company_parish = content.filtered(lambda self, company_id=company_id: self.company_id.id == company_id.id).mapped('parish_id.name')
                # * Sums
                sub_capacity = sum(sub_capacity)
                sub_reimbursed_vacancies = sum(sub_reimbursed_vacancies)
                sub_non_reimbursed_vacancies = sum(sub_non_reimbursed_vacancies)
                # * Adding to dict
                values['sub_ipi']['company_id_{0}'.format(company_id.id)]['capacity'] = sub_capacity
                values['sub_ipi']['company_id_{0}'.format(company_id.id)]['reimbursed_vacancies'] = sub_reimbursed_vacancies
                values['sub_ipi']['company_id_{0}'.format(company_id.id)]['non_reimbursed_vacancies'] = sub_non_reimbursed_vacancies
                values['sub_ipi']['company_id_{0}'.format(company_id.id)]['company_name'] = company_name
                values['sub_ipi']['company_id_{0}'.format(company_id.id)]['parish_name'] = company_parish

        
        values['sub_support_home'] = dict()
        sub_support_home_ids = content.filtered(lambda self: self.sub_children_youth_dis == 'support_home')
        values['sub_support_home']['company_ids'] = sub_support_home_ids.mapped('company_id').ids if sub_support_home_ids.mapped('company_id') else []
        for sub_support_home_id in sub_support_home_ids:
            company_ids = sub_support_home_id.mapped('company_id')
            for company_id in company_ids:
                values['sub_support_home']['company_id_{0}'.format(company_id.id)] = dict()
                # * Filtering content
                sub_capacity = content.filtered(lambda self, company_id=company_id: self.company_id.id == company_id.id).mapped('capacity')
                sub_reimbursed_vacancies = content.filtered(lambda self, company_id=company_id: self.company_id.id == company_id.id).mapped('reimbursed_vacancies')
                sub_non_reimbursed_vacancies = content.filtered(lambda self, company_id=company_id: self.company_id.id == company_id.id).mapped('non_reimbursed_vacancies')
                company_name = content.filtered(lambda self, company_id=company_id: self.company_id.id == company_id.id).mapped('company_id.name')
                company_parish = content.filtered(lambda self, company_id=company_id: self.company_id.id == company_id.id).mapped('parish_id.name')
                # * Sums
                sub_capacity = sum(sub_capacity)
                sub_reimbursed_vacancies = sum(sub_reimbursed_vacancies)
                sub_non_reimbursed_vacancies = sum(sub_non_reimbursed_vacancies)
                # * Adding to dict
                values['sub_support_home']['company_id_{0}'.format(company_id.id)]['capacity'] = sub_capacity
                values['sub_support_home']['company_id_{0}'.format(company_id.id)]['reimbursed_vacancies'] = sub_reimbursed_vacancies
                values['sub_support_home']['company_id_{0}'.format(company_id.id)]['non_reimbursed_vacancies'] = sub_non_reimbursed_vacancies
                values['sub_support_home']['company_id_{0}'.format(company_id.id)]['company_name'] = company_name
                values['sub_support_home']['company_id_{0}'.format(company_id.id)]['parish_name'] = company_parish

        
        values['sub_dis_transport'] = dict()
        sub_dis_transport_ids = content.filtered(lambda self: self.sub_children_youth_dis == 'dis_transport')
        values['sub_dis_transport']['company_ids'] = sub_dis_transport_ids.mapped('company_id').ids if sub_dis_transport_ids.mapped('company_id') else []
        for sub_dis_transport_id in sub_dis_transport_ids:
            company_ids = sub_dis_transport_id.mapped('company_id')
            for company_id in company_ids:
                values['sub_dis_transport']['company_id_{0}'.format(company_id.id)] = dict()
                # * Filtering content
                sub_capacity = content.filtered(lambda self, company_id=company_id: self.company_id.id == company_id.id).mapped('capacity')
                sub_reimbursed_vacancies = content.filtered(lambda self, company_id=company_id: self.company_id.id == company_id.id).mapped('reimbursed_vacancies')
                sub_non_reimbursed_vacancies = content.filtered(lambda self, company_id=company_id: self.company_id.id == company_id.id).mapped('non_reimbursed_vacancies')
                company_name = content.filtered(lambda self, company_id=company_id: self.company_id.id == company_id.id).mapped('company_id.name')
                company_parish = content.filtered(lambda self, company_id=company_id: self.company_id.id == company_id.id).mapped('parish_id.name')
                # * Sums
                sub_capacity = sum(sub_capacity)
                sub_reimbursed_vacancies = sum(sub_reimbursed_vacancies)
                sub_non_reimbursed_vacancies = sum(sub_non_reimbursed_vacancies)
                # * Adding to dict
                values['sub_dis_transport']['company_id_{0}'.format(company_id.id)]['capacity'] = sub_capacity
                values['sub_dis_transport']['company_id_{0}'.format(company_id.id)]['reimbursed_vacancies'] = sub_reimbursed_vacancies
                values['sub_dis_transport']['company_id_{0}'.format(company_id.id)]['non_reimbursed_vacancies'] = sub_non_reimbursed_vacancies
                values['sub_dis_transport']['company_id_{0}'.format(company_id.id)]['company_name'] = company_name
                values['sub_dis_transport']['company_id_{0}'.format(company_id.id)]['parish_name'] = company_parish

        
        values['sub_cafap'] = dict()
        sub_cafap_ids = content.filtered(lambda self: self.sub_children_youth_danger == 'cafap')
        values['sub_cafap']['company_ids'] = sub_cafap_ids.mapped('company_id').ids if sub_cafap_ids.mapped('company_id') else []
        for sub_cafap_id in sub_cafap_ids:
            company_ids = sub_cafap_id.mapped('company_id')
            for company_id in company_ids:
                values['sub_cafap']['company_id_{0}'.format(company_id.id)] = dict()
                # * Filtering content
                sub_capacity = content.filtered(lambda self, company_id=company_id: self.company_id.id == company_id.id).mapped('capacity')
                sub_reimbursed_vacancies = content.filtered(lambda self, company_id=company_id: self.company_id.id == company_id.id).mapped('reimbursed_vacancies')
                sub_non_reimbursed_vacancies = content.filtered(lambda self, company_id=company_id: self.company_id.id == company_id.id).mapped('non_reimbursed_vacancies')
                company_name = content.filtered(lambda self, company_id=company_id: self.company_id.id == company_id.id).mapped('company_id.name')
                company_parish = content.filtered(lambda self, company_id=company_id: self.company_id.id == company_id.id).mapped('parish_id.name')
                # * Sums
                sub_capacity = sum(sub_capacity)
                sub_reimbursed_vacancies = sum(sub_reimbursed_vacancies)
                sub_non_reimbursed_vacancies = sum(sub_non_reimbursed_vacancies)
                # * Adding to dict
                values['sub_cafap']['company_id_{0}'.format(company_id.id)]['capacity'] = sub_capacity
                values['sub_cafap']['company_id_{0}'.format(company_id.id)]['reimbursed_vacancies'] = sub_reimbursed_vacancies
                values['sub_cafap']['company_id_{0}'.format(company_id.id)]['non_reimbursed_vacancies'] = sub_non_reimbursed_vacancies
                values['sub_cafap']['company_id_{0}'.format(company_id.id)]['company_name'] = company_name
                values['sub_cafap']['company_id_{0}'.format(company_id.id)]['parish_name'] = company_parish

        
        values['sub_street_youth_support'] = dict()
        sub_street_youth_support_ids = content.filtered(lambda self: self.sub_children_youth_danger == 'street_youth_support')
        values['sub_street_youth_support']['company_ids'] = sub_street_youth_support_ids.mapped('company_id').ids if sub_street_youth_support_ids.mapped('company_id') else []
        for sub_street_youth_support_id in sub_street_youth_support_ids:
            company_ids = sub_street_youth_support_id.mapped('company_id')
            for company_id in company_ids:
                values['sub_cafap']['company_id_{0}'.format(company_id.id)] = dict()
                # * Filtering content
                sub_capacity = content.filtered(lambda self, company_id=company_id: self.company_id.id == company_id.id).mapped('capacity')
                sub_reimbursed_vacancies = content.filtered(lambda self, company_id=company_id: self.company_id.id == company_id.id).mapped('reimbursed_vacancies')
                sub_non_reimbursed_vacancies = content.filtered(lambda self, company_id=company_id: self.company_id.id == company_id.id).mapped('non_reimbursed_vacancies')
                company_name = content.filtered(lambda self, company_id=company_id: self.company_id.id == company_id.id).mapped('company_id.name')
                company_parish = content.filtered(lambda self, company_id=company_id: self.company_id.id == company_id.id).mapped('parish_id.name')
                # * Sums
                sub_capacity = sum(sub_capacity)
                sub_reimbursed_vacancies = sum(sub_reimbursed_vacancies)
                sub_non_reimbursed_vacancies = sum(sub_non_reimbursed_vacancies)
                # * Adding to dict
                values['sub_street_youth_support']['company_id_{0}'.format(company_id.id)]['capacity'] = sub_capacity
                values['sub_street_youth_support']['company_id_{0}'.format(company_id.id)]['reimbursed_vacancies'] = sub_reimbursed_vacancies
                values['sub_street_youth_support']['company_id_{0}'.format(company_id.id)]['non_reimbursed_vacancies'] = sub_non_reimbursed_vacancies
                values['sub_street_youth_support']['company_id_{0}'.format(company_id.id)]['company_name'] = company_name
                values['sub_street_youth_support']['company_id_{0}'.format(company_id.id)]['parish_name'] = company_parish


        values['sub_family_reception'] = dict()
        sub_family_reception_ids = content.filtered(lambda self: self.sub_children_youth_danger == 'family_reception')
        values['sub_family_reception']['company_ids'] = sub_family_reception_ids.mapped('company_id').ids if sub_family_reception_ids.mapped('company_id') else []
        for sub_family_reception_id in sub_family_reception_ids:
            company_ids = sub_family_reception_id.mapped('company_id')
            for company_id in company_ids:
                values['sub_family_reception']['company_id_{0}'.format(company_id.id)] = dict()
                # * Filtering content
                sub_capacity = content.filtered(lambda self, company_id=company_id: self.company_id.id == company_id.id).mapped('capacity')
                sub_reimbursed_vacancies = content.filtered(lambda self, company_id=company_id: self.company_id.id == company_id.id).mapped('reimbursed_vacancies')
                sub_non_reimbursed_vacancies = content.filtered(lambda self, company_id=company_id: self.company_id.id == company_id.id).mapped('non_reimbursed_vacancies')
                company_name = content.filtered(lambda self, company_id=company_id: self.company_id.id == company_id.id).mapped('company_id.name')
                company_parish = content.filtered(lambda self, company_id=company_id: self.company_id.id == company_id.id).mapped('parish_id.name')
                # * Sums
                sub_capacity = sum(sub_capacity)
                sub_reimbursed_vacancies = sum(sub_reimbursed_vacancies)
                sub_non_reimbursed_vacancies = sum(sub_non_reimbursed_vacancies)
                # * Adding to dict
                values['sub_family_reception']['company_id_{0}'.format(company_id.id)]['capacity'] = sub_capacity
                values['sub_family_reception']['company_id_{0}'.format(company_id.id)]['reimbursed_vacancies'] = sub_reimbursed_vacancies
                values['sub_family_reception']['company_id_{0}'.format(company_id.id)]['non_reimbursed_vacancies'] = sub_non_reimbursed_vacancies
                values['sub_family_reception']['company_id_{0}'.format(company_id.id)]['company_name'] = company_name
                values['sub_family_reception']['company_id_{0}'.format(company_id.id)]['parish_name'] = company_parish


        values['sub_residential_home'] = dict()
        sub_residential_home_ids = content.filtered(lambda self: self.sub_children_youth_danger == 'residential_home')
        values['sub_residential_home']['company_ids'] = sub_residential_home_ids.mapped('company_id').ids if sub_residential_home_ids.mapped('company_id') else []
        for sub_residential_home_id in sub_residential_home_ids:
            company_ids = sub_residential_home_id.mapped('company_id')
            for company_id in company_ids:
                values['sub_residential_home']['company_id_{0}'.format(company_id.id)] = dict()
                # * Filtering content
                sub_capacity = content.filtered(lambda self, company_id=company_id: self.company_id.id == company_id.id).mapped('capacity')
                sub_reimbursed_vacancies = content.filtered(lambda self, company_id=company_id: self.company_id.id == company_id.id).mapped('reimbursed_vacancies')
                sub_non_reimbursed_vacancies = content.filtered(lambda self, company_id=company_id: self.company_id.id == company_id.id).mapped('non_reimbursed_vacancies')
                company_name = content.filtered(lambda self, company_id=company_id: self.company_id.id == company_id.id).mapped('company_id.name')
                company_parish = content.filtered(lambda self, company_id=company_id: self.company_id.id == company_id.id).mapped('parish_id.name')
                # * Sums
                sub_capacity = sum(sub_capacity)
                sub_reimbursed_vacancies = sum(sub_reimbursed_vacancies)
                sub_non_reimbursed_vacancies = sum(sub_non_reimbursed_vacancies)
                # * Adding to dict
                values['sub_residential_home']['company_id_{0}'.format(company_id.id)]['capacity'] = sub_capacity
                values['sub_residential_home']['company_id_{0}'.format(company_id.id)]['reimbursed_vacancies'] = sub_reimbursed_vacancies
                values['sub_residential_home']['company_id_{0}'.format(company_id.id)]['non_reimbursed_vacancies'] = sub_non_reimbursed_vacancies
                values['sub_residential_home']['company_id_{0}'.format(company_id.id)]['company_name'] = company_name
                values['sub_residential_home']['company_id_{0}'.format(company_id.id)]['parish_name'] = company_parish

        
        values['sub_autonomy_apartment'] = dict()
        sub_autonomy_apartment_ids = content.filtered(lambda self: self.sub_children_youth_danger == 'autonomy_apartment')
        values['sub_autonomy_apartment']['company_ids'] = sub_autonomy_apartment_ids.mapped('company_id').ids if sub_autonomy_apartment_ids.mapped('company_id') else []
        for sub_autonomy_apartment_id in sub_autonomy_apartment_ids:
            company_ids = sub_autonomy_apartment_id.mapped('company_id')
            for company_id in company_ids:
                values['sub_autonomy_apartment']['company_id_{0}'.format(company_id.id)] = dict()
                # * Filtering content
                sub_capacity = content.filtered(lambda self, company_id=company_id: self.company_id.id == company_id.id).mapped('capacity')
                sub_reimbursed_vacancies = content.filtered(lambda self, company_id=company_id: self.company_id.id == company_id.id).mapped('reimbursed_vacancies')
                sub_non_reimbursed_vacancies = content.filtered(lambda self, company_id=company_id: self.company_id.id == company_id.id).mapped('non_reimbursed_vacancies')
                company_name = content.filtered(lambda self, company_id=company_id: self.company_id.id == company_id.id).mapped('company_id.name')
                company_parish = content.filtered(lambda self, company_id=company_id: self.company_id.id == company_id.id).mapped('parish_id.name')
                # * Sums
                sub_capacity = sum(sub_capacity)
                sub_reimbursed_vacancies = sum(sub_reimbursed_vacancies)
                sub_non_reimbursed_vacancies = sum(sub_non_reimbursed_vacancies)
                # * Adding to dict
                values['sub_autonomy_apartment']['company_id_{0}'.format(company_id.id)]['capacity'] = sub_capacity
                values['sub_autonomy_apartment']['company_id_{0}'.format(company_id.id)]['reimbursed_vacancies'] = sub_reimbursed_vacancies
                values['sub_autonomy_apartment']['company_id_{0}'.format(company_id.id)]['non_reimbursed_vacancies'] = sub_non_reimbursed_vacancies
                values['sub_autonomy_apartment']['company_id_{0}'.format(company_id.id)]['company_name'] = company_name
                values['sub_autonomy_apartment']['company_id_{0}'.format(company_id.id)]['parish_name'] = company_parish

        
        values['sub_old_sad'] = dict()
        sub_old_sad_ids = content.filtered(lambda self: self.sub_old == 'sad')
        values['sub_old_sad']['company_ids'] = sub_old_sad_ids.mapped('company_id').ids if sub_old_sad_ids.mapped('company_id') else []
        for sub_old_sad_id in sub_old_sad_ids:
            company_ids = sub_old_sad_id.mapped('company_id')
            for company_id in company_ids:
                values['sub_old_sad']['company_id_{0}'.format(company_id.id)] = dict()
                # * Filtering content
                sub_capacity = content.filtered(lambda self, company_id=company_id: self.company_id.id == company_id.id).mapped('capacity')
                sub_reimbursed_vacancies = content.filtered(lambda self, company_id=company_id: self.company_id.id == company_id.id).mapped('reimbursed_vacancies')
                sub_non_reimbursed_vacancies = content.filtered(lambda self, company_id=company_id: self.company_id.id == company_id.id).mapped('non_reimbursed_vacancies')
                company_name = content.filtered(lambda self, company_id=company_id: self.company_id.id == company_id.id).mapped('company_id.name')
                company_parish = content.filtered(lambda self, company_id=company_id: self.company_id.id == company_id.id).mapped('parish_id.name')
                # * Sums
                sub_capacity = sum(sub_capacity)
                sub_reimbursed_vacancies = sum(sub_reimbursed_vacancies)
                sub_non_reimbursed_vacancies = sum(sub_non_reimbursed_vacancies)
                # * Adding to dict
                values['sub_old_sad']['company_id_{0}'.format(company_id.id)]['capacity'] = sub_capacity
                values['sub_old_sad']['company_id_{0}'.format(company_id.id)]['reimbursed_vacancies'] = sub_reimbursed_vacancies
                values['sub_old_sad']['company_id_{0}'.format(company_id.id)]['non_reimbursed_vacancies'] = sub_non_reimbursed_vacancies
                values['sub_old_sad']['company_id_{0}'.format(company_id.id)]['company_name'] = company_name
                values['sub_old_sad']['company_id_{0}'.format(company_id.id)]['parish_name'] = company_parish

        
        values['sub_social_center'] = dict()
        sub_social_center_ids = content.filtered(lambda self: self.sub_old == 'social_center')
        values['sub_social_center']['company_ids'] = sub_social_center_ids.mapped('company_id').ids if sub_social_center_ids.mapped('company_id') else []
        for sub_social_center_id in sub_social_center_ids:
            company_ids = sub_social_center_id.mapped('company_id')
            for company_id in company_ids:
                values['sub_social_center']['company_id_{0}'.format(company_id.id)] = dict()
                # * Filtering content
                sub_capacity = content.filtered(lambda self, company_id=company_id: self.company_id.id == company_id.id).mapped('capacity')
                sub_reimbursed_vacancies = content.filtered(lambda self, company_id=company_id: self.company_id.id == company_id.id).mapped('reimbursed_vacancies')
                sub_non_reimbursed_vacancies = content.filtered(lambda self, company_id=company_id: self.company_id.id == company_id.id).mapped('non_reimbursed_vacancies')
                company_name = content.filtered(lambda self, company_id=company_id: self.company_id.id == company_id.id).mapped('company_id.name')
                company_parish = content.filtered(lambda self, company_id=company_id: self.company_id.id == company_id.id).mapped('parish_id.name')
                # * Sums
                sub_capacity = sum(sub_capacity)
                sub_reimbursed_vacancies = sum(sub_reimbursed_vacancies)
                sub_non_reimbursed_vacancies = sum(sub_non_reimbursed_vacancies)
                # * Adding to dict
                values['sub_social_center']['company_id_{0}'.format(company_id.id)]['capacity'] = sub_capacity
                values['sub_social_center']['company_id_{0}'.format(company_id.id)]['reimbursed_vacancies'] = sub_reimbursed_vacancies
                values['sub_social_center']['company_id_{0}'.format(company_id.id)]['non_reimbursed_vacancies'] = sub_non_reimbursed_vacancies
                values['sub_social_center']['company_id_{0}'.format(company_id.id)]['company_name'] = company_name
                values['sub_social_center']['company_id_{0}'.format(company_id.id)]['parish_name'] = company_parish

        
        values['sub_day_care'] = dict()
        sub_day_care_ids = content.filtered(lambda self: self.sub_old == 'day_care')
        values['sub_day_care']['company_ids'] = sub_day_care_ids.mapped('company_id').ids if sub_day_care_ids.mapped('company_id') else []
        for sub_day_care_id in sub_day_care_ids:
            company_ids = sub_day_care_id.mapped('company_id')
            for company_id in company_ids:
                values['sub_day_care']['company_id_{0}'.format(company_id.id)] = dict()
                # * Filtering content
                sub_capacity = content.filtered(lambda self, company_id=company_id: self.company_id.id == company_id.id).mapped('capacity')
                sub_reimbursed_vacancies = content.filtered(lambda self, company_id=company_id: self.company_id.id == company_id.id).mapped('reimbursed_vacancies')
                sub_non_reimbursed_vacancies = content.filtered(lambda self, company_id=company_id: self.company_id.id == company_id.id).mapped('non_reimbursed_vacancies')
                company_name = content.filtered(lambda self, company_id=company_id: self.company_id.id == company_id.id).mapped('company_id.name')
                company_parish = content.filtered(lambda self, company_id=company_id: self.company_id.id == company_id.id).mapped('parish_id.name')
                # * Sums
                sub_capacity = sum(sub_capacity)
                sub_reimbursed_vacancies = sum(sub_reimbursed_vacancies)
                sub_non_reimbursed_vacancies = sum(sub_non_reimbursed_vacancies)
                # * Adding to dict
                values['sub_day_care']['company_id_{0}'.format(company_id.id)]['capacity'] = sub_capacity
                values['sub_day_care']['company_id_{0}'.format(company_id.id)]['reimbursed_vacancies'] = sub_reimbursed_vacancies
                values['sub_day_care']['company_id_{0}'.format(company_id.id)]['non_reimbursed_vacancies'] = sub_non_reimbursed_vacancies
                values['sub_day_care']['company_id_{0}'.format(company_id.id)]['company_name'] = company_name
                values['sub_day_care']['company_id_{0}'.format(company_id.id)]['parish_name'] = company_parish


        values['sub_night_center'] = dict()
        sub_night_center_ids = content.filtered(lambda self: self.sub_old == 'night_center')
        values['sub_night_center']['company_ids'] = sub_night_center_ids.mapped('company_id').ids if sub_night_center_ids.mapped('company_id') else []
        for sub_night_center_id in sub_night_center_ids:
            company_ids = sub_night_center_id.mapped('company_id')
            for company_id in company_ids:
                values['sub_night_center']['company_id_{0}'.format(company_id.id)] = dict()
                # * Filtering content
                sub_capacity = content.filtered(lambda self, company_id=company_id: self.company_id.id == company_id.id).mapped('capacity')
                sub_reimbursed_vacancies = content.filtered(lambda self, company_id=company_id: self.company_id.id == company_id.id).mapped('reimbursed_vacancies')
                sub_non_reimbursed_vacancies = content.filtered(lambda self, company_id=company_id: self.company_id.id == company_id.id).mapped('non_reimbursed_vacancies')
                company_name = content.filtered(lambda self, company_id=company_id: self.company_id.id == company_id.id).mapped('company_id.name')
                company_parish = content.filtered(lambda self, company_id=company_id: self.company_id.id == company_id.id).mapped('parish_id.name')
                # * Sums
                sub_capacity = sum(sub_capacity)
                sub_reimbursed_vacancies = sum(sub_reimbursed_vacancies)
                sub_non_reimbursed_vacancies = sum(sub_non_reimbursed_vacancies)
                # * Adding to dict
                values['sub_night_center']['company_id_{0}'.format(company_id.id)]['capacity'] = sub_capacity
                values['sub_night_center']['company_id_{0}'.format(company_id.id)]['reimbursed_vacancies'] = sub_reimbursed_vacancies
                values['sub_night_center']['company_id_{0}'.format(company_id.id)]['non_reimbursed_vacancies'] = sub_non_reimbursed_vacancies
                values['sub_night_center']['company_id_{0}'.format(company_id.id)]['company_name'] = company_name
                values['sub_night_center']['company_id_{0}'.format(company_id.id)]['parish_name'] = company_parish


        values['sub_care_for_elderly'] = dict()
        sub_care_for_elderly_ids = content.filtered(lambda self: self.sub_old == 'care_for_elderly')
        values['sub_care_for_elderly']['company_ids'] = sub_care_for_elderly_ids.mapped('company_id').ids if sub_care_for_elderly_ids.mapped('company_id') else []
        for sub_care_for_elderly_id in sub_care_for_elderly_ids:
            company_ids = sub_care_for_elderly_id.mapped('company_id')
            for company_id in company_ids:
                values['sub_care_for_elderly']['company_id_{0}'.format(company_id.id)] = dict()
                # * Filtering content
                sub_capacity = content.filtered(lambda self, company_id=company_id: self.company_id.id == company_id.id).mapped('capacity')
                sub_reimbursed_vacancies = content.filtered(lambda self, company_id=company_id: self.company_id.id == company_id.id).mapped('reimbursed_vacancies')
                sub_non_reimbursed_vacancies = content.filtered(lambda self, company_id=company_id: self.company_id.id == company_id.id).mapped('non_reimbursed_vacancies')
                company_name = content.filtered(lambda self, company_id=company_id: self.company_id.id == company_id.id).mapped('company_id.name')
                company_parish = content.filtered(lambda self, company_id=company_id: self.company_id.id == company_id.id).mapped('parish_id.name')
                # * Sums
                sub_capacity = sum(sub_capacity)
                sub_reimbursed_vacancies = sum(sub_reimbursed_vacancies)
                sub_non_reimbursed_vacancies = sum(sub_non_reimbursed_vacancies)
                # * Adding to dict
                values['sub_care_for_elderly']['company_id_{0}'.format(company_id.id)]['capacity'] = sub_capacity
                values['sub_care_for_elderly']['company_id_{0}'.format(company_id.id)]['reimbursed_vacancies'] = sub_reimbursed_vacancies
                values['sub_care_for_elderly']['company_id_{0}'.format(company_id.id)]['non_reimbursed_vacancies'] = sub_non_reimbursed_vacancies
                values['sub_care_for_elderly']['company_id_{0}'.format(company_id.id)]['company_name'] = company_name
                values['sub_care_for_elderly']['company_id_{0}'.format(company_id.id)]['parish_name'] = company_parish


        values['sub_erpi'] = dict()
        sub_erpi_ids = content.filtered(lambda self: self.sub_old == 'erpi')
        values['sub_erpi']['company_ids'] = sub_erpi_ids.mapped('company_id').ids if sub_erpi_ids.mapped('company_id') else []
        for sub_erpi_id in sub_erpi_ids:
            company_ids = sub_erpi_id.mapped('company_id')
            for company_id in company_ids:
                values['sub_erpi']['company_id_{0}'.format(company_id.id)] = dict()
                # * Filtering content
                sub_capacity = content.filtered(lambda self, company_id=company_id: self.company_id.id == company_id.id).mapped('capacity')
                sub_reimbursed_vacancies = content.filtered(lambda self, company_id=company_id: self.company_id.id == company_id.id).mapped('reimbursed_vacancies')
                sub_non_reimbursed_vacancies = content.filtered(lambda self, company_id=company_id: self.company_id.id == company_id.id).mapped('non_reimbursed_vacancies')
                company_name = content.filtered(lambda self, company_id=company_id: self.company_id.id == company_id.id).mapped('company_id.name')
                company_parish = content.filtered(lambda self, company_id=company_id: self.company_id.id == company_id.id).mapped('parish_id.name')
                # * Sums
                sub_capacity = sum(sub_capacity)
                sub_reimbursed_vacancies = sum(sub_reimbursed_vacancies)
                sub_non_reimbursed_vacancies = sum(sub_non_reimbursed_vacancies)
                # * Adding to dict
                values['sub_erpi']['company_id_{0}'.format(company_id.id)]['capacity'] = sub_capacity
                values['sub_erpi']['company_id_{0}'.format(company_id.id)]['reimbursed_vacancies'] = sub_reimbursed_vacancies
                values['sub_erpi']['company_id_{0}'.format(company_id.id)]['non_reimbursed_vacancies'] = sub_non_reimbursed_vacancies
                values['sub_erpi']['company_id_{0}'.format(company_id.id)]['company_name'] = company_name
                values['sub_erpi']['company_id_{0}'.format(company_id.id)]['parish_name'] = company_parish


        values['sub_service_center_dis'] = dict()
        sub_service_center_dis_ids = content.filtered(lambda self: self.sub_dis_adult == 'service_center_dis')
        values['sub_service_center_dis']['company_ids'] = sub_service_center_dis_ids.mapped('company_id').ids if sub_service_center_dis_ids.mapped('company_id') else []
        for sub_service_center_dis_id in sub_service_center_dis_ids:
            company_ids = sub_service_center_dis_id.mapped('company_id')
            for company_id in company_ids:
                values['sub_service_center_dis']['company_id_{0}'.format(company_id.id)] = dict()
                # * Filtering content
                sub_capacity = content.filtered(lambda self, company_id=company_id: self.company_id.id == company_id.id).mapped('capacity')
                sub_reimbursed_vacancies = content.filtered(lambda self, company_id=company_id: self.company_id.id == company_id.id).mapped('reimbursed_vacancies')
                sub_non_reimbursed_vacancies = content.filtered(lambda self, company_id=company_id: self.company_id.id == company_id.id).mapped('non_reimbursed_vacancies')
                company_name = content.filtered(lambda self, company_id=company_id: self.company_id.id == company_id.id).mapped('company_id.name')
                company_parish = content.filtered(lambda self, company_id=company_id: self.company_id.id == company_id.id).mapped('parish_id.name')
                # * Sums
                sub_capacity = sum(sub_capacity)
                sub_reimbursed_vacancies = sum(sub_reimbursed_vacancies)
                sub_non_reimbursed_vacancies = sum(sub_non_reimbursed_vacancies)
                # * Adding to dict
                values['sub_service_center_dis']['company_id_{0}'.format(company_id.id)]['capacity'] = sub_capacity
                values['sub_service_center_dis']['company_id_{0}'.format(company_id.id)]['reimbursed_vacancies'] = sub_reimbursed_vacancies
                values['sub_service_center_dis']['company_id_{0}'.format(company_id.id)]['non_reimbursed_vacancies'] = sub_non_reimbursed_vacancies
                values['sub_service_center_dis']['company_id_{0}'.format(company_id.id)]['company_name'] = company_name
                values['sub_service_center_dis']['company_id_{0}'.format(company_id.id)]['parish_name'] = company_parish


        values['sub_service_center_rehab'] = dict()
        sub_service_center_rehab_ids = content.filtered(lambda self: self.sub_dis_adult == 'service_center_rehab')
        values['sub_service_center_rehab']['company_ids'] = sub_service_center_rehab_ids.mapped('company_id').ids if sub_service_center_rehab_ids.mapped('company_id') else []
        for sub_service_center_rehab_id in sub_service_center_rehab_ids:
            company_ids = sub_service_center_rehab_id.mapped('company_id')
            for company_id in company_ids:
                values['sub_service_center_rehab']['company_id_{0}'.format(company_id.id)] = dict()
                # * Filtering content
                sub_capacity = content.filtered(lambda self, company_id=company_id: self.company_id.id == company_id.id).mapped('capacity')
                sub_reimbursed_vacancies = content.filtered(lambda self, company_id=company_id: self.company_id.id == company_id.id).mapped('reimbursed_vacancies')
                sub_non_reimbursed_vacancies = content.filtered(lambda self, company_id=company_id: self.company_id.id == company_id.id).mapped('non_reimbursed_vacancies')
                company_name = content.filtered(lambda self, company_id=company_id: self.company_id.id == company_id.id).mapped('company_id.name')
                company_parish = content.filtered(lambda self, company_id=company_id: self.company_id.id == company_id.id).mapped('parish_id.name')
                # * Sums
                sub_capacity = sum(sub_capacity)
                sub_reimbursed_vacancies = sum(sub_reimbursed_vacancies)
                sub_non_reimbursed_vacancies = sum(sub_non_reimbursed_vacancies)
                # * Adding to dict
                values['sub_service_center_rehab']['company_id_{0}'.format(company_id.id)]['capacity'] = sub_capacity
                values['sub_service_center_rehab']['company_id_{0}'.format(company_id.id)]['reimbursed_vacancies'] = sub_reimbursed_vacancies
                values['sub_service_center_rehab']['company_id_{0}'.format(company_id.id)]['non_reimbursed_vacancies'] = sub_non_reimbursed_vacancies
                values['sub_service_center_rehab']['company_id_{0}'.format(company_id.id)]['company_name'] = company_name
                values['sub_service_center_rehab']['company_id_{0}'.format(company_id.id)]['parish_name'] = company_parish


        values['sub_disabled_sad'] = dict()
        sub_disabled_sad_ids = content.filtered(lambda self: self.sub_dis_adult == 'sad')
        values['sub_disabled_sad']['company_ids'] = sub_disabled_sad_ids.mapped('company_id').ids if sub_disabled_sad_ids.mapped('company_id') else []
        for sub_disabled_sad_id in sub_disabled_sad_ids:
            company_ids = sub_disabled_sad_id.mapped('company_id')
            for company_id in company_ids:
                values['sub_disabled_sad']['company_id_{0}'.format(company_id.id)] = dict()
                # * Filtering content
                sub_capacity = content.filtered(lambda self, company_id=company_id: self.company_id.id == company_id.id).mapped('capacity')
                sub_reimbursed_vacancies = content.filtered(lambda self, company_id=company_id: self.company_id.id == company_id.id).mapped('reimbursed_vacancies')
                sub_non_reimbursed_vacancies = content.filtered(lambda self, company_id=company_id: self.company_id.id == company_id.id).mapped('non_reimbursed_vacancies')
                company_name = content.filtered(lambda self, company_id=company_id: self.company_id.id == company_id.id).mapped('company_id.name')
                company_parish = content.filtered(lambda self, company_id=company_id: self.company_id.id == company_id.id).mapped('parish_id.name')
                # * Sums
                sub_capacity = sum(sub_capacity)
                sub_reimbursed_vacancies = sum(sub_reimbursed_vacancies)
                sub_non_reimbursed_vacancies = sum(sub_non_reimbursed_vacancies)
                # * Adding to dict
                values['sub_disabled_sad']['company_id_{0}'.format(company_id.id)]['capacity'] = sub_capacity
                values['sub_disabled_sad']['company_id_{0}'.format(company_id.id)]['reimbursed_vacancies'] = sub_reimbursed_vacancies
                values['sub_disabled_sad']['company_id_{0}'.format(company_id.id)]['non_reimbursed_vacancies'] = sub_non_reimbursed_vacancies
                values['sub_disabled_sad']['company_id_{0}'.format(company_id.id)]['company_name'] = company_name
                values['sub_disabled_sad']['company_id_{0}'.format(company_id.id)]['parish_name'] = company_parish


        values['sub_cao'] = dict()
        sub_cao_ids = content.filtered(lambda self: self.sub_dis_adult == 'cao')
        values['sub_cao']['company_ids'] = sub_cao_ids.mapped('company_id').ids if sub_cao_ids.mapped('company_id') else []
        for sub_cao_id in sub_cao_ids:
            company_ids = sub_cao_id.mapped('company_id')
            for company_id in company_ids:
                values['sub_cao']['company_id_{0}'.format(company_id.id)] = dict()
                # * Filtering content
                sub_capacity = content.filtered(lambda self, company_id=company_id: self.company_id.id == company_id.id).mapped('capacity')
                sub_reimbursed_vacancies = content.filtered(lambda self, company_id=company_id: self.company_id.id == company_id.id).mapped('reimbursed_vacancies')
                sub_non_reimbursed_vacancies = content.filtered(lambda self, company_id=company_id: self.company_id.id == company_id.id).mapped('non_reimbursed_vacancies')
                company_name = content.filtered(lambda self, company_id=company_id: self.company_id.id == company_id.id).mapped('company_id.name')
                company_parish = content.filtered(lambda self, company_id=company_id: self.company_id.id == company_id.id).mapped('parish_id.name')
                # * Sums
                sub_capacity = sum(sub_capacity)
                sub_reimbursed_vacancies = sum(sub_reimbursed_vacancies)
                sub_non_reimbursed_vacancies = sum(sub_non_reimbursed_vacancies)
                # * Adding to dict
                values['sub_cao']['company_id_{0}'.format(company_id.id)]['capacity'] = sub_capacity
                values['sub_cao']['company_id_{0}'.format(company_id.id)]['reimbursed_vacancies'] = sub_reimbursed_vacancies
                values['sub_cao']['company_id_{0}'.format(company_id.id)]['non_reimbursed_vacancies'] = sub_non_reimbursed_vacancies
                values['sub_cao']['company_id_{0}'.format(company_id.id)]['company_name'] = company_name
                values['sub_cao']['company_id_{0}'.format(company_id.id)]['parish_name'] = company_parish


        values['sub_reception_dis'] = dict()
        sub_reception_dis_ids = content.filtered(lambda self: self.sub_dis_adult == 'reception_dis')
        values['sub_reception_dis']['company_ids'] = sub_reception_dis_ids.mapped('company_id').ids if sub_reception_dis_ids.mapped('company_id') else []
        for sub_reception_dis_id in sub_reception_dis_ids:
            company_ids = sub_reception_dis_id.mapped('company_id')
            for company_id in company_ids:
                values['sub_reception_dis']['company_id_{0}'.format(company_id.id)] = dict()
                # * Filtering content
                sub_capacity = content.filtered(lambda self, company_id=company_id: self.company_id.id == company_id.id).mapped('capacity')
                sub_reimbursed_vacancies = content.filtered(lambda self, company_id=company_id: self.company_id.id == company_id.id).mapped('reimbursed_vacancies')
                sub_non_reimbursed_vacancies = content.filtered(lambda self, company_id=company_id: self.company_id.id == company_id.id).mapped('non_reimbursed_vacancies')
                company_name = content.filtered(lambda self, company_id=company_id: self.company_id.id == company_id.id).mapped('company_id.name')
                company_parish = content.filtered(lambda self, company_id=company_id: self.company_id.id == company_id.id).mapped('parish_id.name')
                # * Sums
                sub_capacity = sum(sub_capacity)
                sub_reimbursed_vacancies = sum(sub_reimbursed_vacancies)
                sub_non_reimbursed_vacancies = sum(sub_non_reimbursed_vacancies)
                # * Adding to dict
                values['sub_reception_dis']['company_id_{0}'.format(company_id.id)]['capacity'] = sub_capacity
                values['sub_reception_dis']['company_id_{0}'.format(company_id.id)]['reimbursed_vacancies'] = sub_reimbursed_vacancies
                values['sub_reception_dis']['company_id_{0}'.format(company_id.id)]['non_reimbursed_vacancies'] = sub_non_reimbursed_vacancies
                values['sub_reception_dis']['company_id_{0}'.format(company_id.id)]['company_name'] = company_name
                values['sub_reception_dis']['company_id_{0}'.format(company_id.id)]['parish_name'] = company_parish


        values['sub_residential_home'] = dict()
        sub_residential_home_ids = content.filtered(lambda self: self.sub_dis_adult == 'residential_home')
        values['sub_residential_home']['company_ids'] = sub_residential_home_ids.mapped('company_id').ids if sub_residential_home_ids.mapped('company_id') else []
        for sub_residential_home_id in sub_residential_home_ids:
            company_ids = sub_residential_home_id.mapped('company_id')
            for company_id in company_ids:
                values['sub_residential_home']['company_id_{0}'.format(company_id.id)] = dict()
                # * Filtering content
                sub_capacity = content.filtered(lambda self, company_id=company_id: self.company_id.id == company_id.id).mapped('capacity')
                sub_reimbursed_vacancies = content.filtered(lambda self, company_id=company_id: self.company_id.id == company_id.id).mapped('reimbursed_vacancies')
                sub_non_reimbursed_vacancies = content.filtered(lambda self, company_id=company_id: self.company_id.id == company_id.id).mapped('non_reimbursed_vacancies')
                company_name = content.filtered(lambda self, company_id=company_id: self.company_id.id == company_id.id).mapped('company_id.name')
                company_parish = content.filtered(lambda self, company_id=company_id: self.company_id.id == company_id.id).mapped('parish_id.name')
                # * Sums
                sub_capacity = sum(sub_capacity)
                sub_reimbursed_vacancies = sum(sub_reimbursed_vacancies)
                sub_non_reimbursed_vacancies = sum(sub_non_reimbursed_vacancies)
                # * Adding to dict
                values['sub_residential_home']['company_id_{0}'.format(company_id.id)]['capacity'] = sub_capacity
                values['sub_residential_home']['company_id_{0}'.format(company_id.id)]['reimbursed_vacancies'] = sub_reimbursed_vacancies
                values['sub_residential_home']['company_id_{0}'.format(company_id.id)]['non_reimbursed_vacancies'] = sub_non_reimbursed_vacancies
                values['sub_residential_home']['company_id_{0}'.format(company_id.id)]['company_name'] = company_name
                values['sub_residential_home']['company_id_{0}'.format(company_id.id)]['parish_name'] = company_parish


        values['sub_autonomous_residence'] = dict()
        sub_autonomous_residence_ids = content.filtered(lambda self: self.sub_dis_adult == 'autonomous_residence')
        values['sub_autonomous_residence']['company_ids'] = sub_autonomous_residence_ids.mapped('company_id').ids if sub_autonomous_residence_ids.mapped('company_id') else []
        for sub_autonomous_residence_id in sub_autonomous_residence_ids:
            company_ids = sub_autonomous_residence_id.mapped('company_id')
            for company_id in company_ids:
                values['sub_autonomous_residence']['company_id_{0}'.format(company_id.id)] = dict()
                # * Filtering content
                sub_capacity = content.filtered(lambda self, company_id=company_id: self.company_id.id == company_id.id).mapped('capacity')
                sub_reimbursed_vacancies = content.filtered(lambda self, company_id=company_id: self.company_id.id == company_id.id).mapped('reimbursed_vacancies')
                sub_non_reimbursed_vacancies = content.filtered(lambda self, company_id=company_id: self.company_id.id == company_id.id).mapped('non_reimbursed_vacancies')
                company_name = content.filtered(lambda self, company_id=company_id: self.company_id.id == company_id.id).mapped('company_id.name')
                company_parish = content.filtered(lambda self, company_id=company_id: self.company_id.id == company_id.id).mapped('parish_id.name')
                # * Sums
                sub_capacity = sum(sub_capacity)
                sub_reimbursed_vacancies = sum(sub_reimbursed_vacancies)
                sub_non_reimbursed_vacancies = sum(sub_non_reimbursed_vacancies)
                # * Adding to dict
                values['sub_autonomous_residence']['company_id_{0}'.format(company_id.id)]['capacity'] = sub_capacity
                values['sub_autonomous_residence']['company_id_{0}'.format(company_id.id)]['reimbursed_vacancies'] = sub_reimbursed_vacancies
                values['sub_autonomous_residence']['company_id_{0}'.format(company_id.id)]['non_reimbursed_vacancies'] = sub_non_reimbursed_vacancies
                values['sub_autonomous_residence']['company_id_{0}'.format(company_id.id)]['company_name'] = company_name
                values['sub_autonomous_residence']['company_id_{0}'.format(company_id.id)]['parish_name'] = company_parish


        values['sub_transport_dis'] = dict()
        sub_transport_dis_ids = content.filtered(lambda self: self.sub_dis_adult == 'transport_dis')
        values['sub_transport_dis']['company_ids'] = sub_transport_dis_ids.mapped('company_id').ids if sub_transport_dis_ids.mapped('company_id') else []
        for sub_transport_dis_id in sub_transport_dis_ids:
            company_ids = sub_transport_dis_id.mapped('company_id')
            for company_id in company_ids:
                values['sub_transport_dis']['company_id_{0}'.format(company_id.id)] = dict()
                # * Filtering content
                sub_capacity = content.filtered(lambda self, company_id=company_id: self.company_id.id == company_id.id).mapped('capacity')
                sub_reimbursed_vacancies = content.filtered(lambda self, company_id=company_id: self.company_id.id == company_id.id).mapped('reimbursed_vacancies')
                sub_non_reimbursed_vacancies = content.filtered(lambda self, company_id=company_id: self.company_id.id == company_id.id).mapped('non_reimbursed_vacancies')
                company_name = content.filtered(lambda self, company_id=company_id: self.company_id.id == company_id.id).mapped('company_id.name')
                company_parish = content.filtered(lambda self, company_id=company_id: self.company_id.id == company_id.id).mapped('parish_id.name')
                # * Sums
                sub_capacity = sum(sub_capacity)
                sub_reimbursed_vacancies = sum(sub_reimbursed_vacancies)
                sub_non_reimbursed_vacancies = sum(sub_non_reimbursed_vacancies)
                # * Adding to dict
                values['sub_transport_dis']['company_id_{0}'.format(company_id.id)]['capacity'] = sub_capacity
                values['sub_transport_dis']['company_id_{0}'.format(company_id.id)]['reimbursed_vacancies'] = sub_reimbursed_vacancies
                values['sub_transport_dis']['company_id_{0}'.format(company_id.id)]['non_reimbursed_vacancies'] = sub_non_reimbursed_vacancies
                values['sub_transport_dis']['company_id_{0}'.format(company_id.id)]['company_name'] = company_name
                values['sub_transport_dis']['company_id_{0}'.format(company_id.id)]['parish_name'] = company_parish


        values['sub_dependent_sad'] = dict()
        sub_dependent_sad_ids = content.filtered(lambda self: self.sub_dependent == 'sad')
        values['sub_dependent_sad']['company_ids'] = sub_dependent_sad_ids.mapped('company_id').ids if sub_dependent_sad_ids.mapped('company_id') else []
        for sub_dependent_sad_id in sub_dependent_sad_ids:
            company_ids = sub_dependent_sad_id.mapped('company_id')
            for company_id in company_ids:
                values['sub_dependent_sad']['company_id_{0}'.format(company_id.id)] = dict()
                # * Filtering content
                sub_capacity = content.filtered(lambda self, company_id=company_id: self.company_id.id == company_id.id).mapped('capacity')
                sub_reimbursed_vacancies = content.filtered(lambda self, company_id=company_id: self.company_id.id == company_id.id).mapped('reimbursed_vacancies')
                sub_non_reimbursed_vacancies = content.filtered(lambda self, company_id=company_id: self.company_id.id == company_id.id).mapped('non_reimbursed_vacancies')
                company_name = content.filtered(lambda self, company_id=company_id: self.company_id.id == company_id.id).mapped('company_id.name')
                company_parish = content.filtered(lambda self, company_id=company_id: self.company_id.id == company_id.id).mapped('parish_id.name')
                # * Sums
                sub_capacity = sum(sub_capacity)
                sub_reimbursed_vacancies = sum(sub_reimbursed_vacancies)
                sub_non_reimbursed_vacancies = sum(sub_non_reimbursed_vacancies)
                # * Adding to dict
                values['sub_dependent_sad']['company_id_{0}'.format(company_id.id)]['capacity'] = sub_capacity
                values['sub_dependent_sad']['company_id_{0}'.format(company_id.id)]['reimbursed_vacancies'] = sub_reimbursed_vacancies
                values['sub_dependent_sad']['company_id_{0}'.format(company_id.id)]['non_reimbursed_vacancies'] = sub_non_reimbursed_vacancies
                values['sub_dependent_sad']['company_id_{0}'.format(company_id.id)]['company_name'] = company_name
                values['sub_dependent_sad']['company_id_{0}'.format(company_id.id)]['parish_name'] = company_parish


        values['sub_adi'] = dict()
        sub_adi_ids = content.filtered(lambda self: self.sub_dependent == 'adi')
        values['sub_adi']['company_ids'] = sub_adi_ids.mapped('company_id').ids if sub_adi_ids.mapped('company_id') else []
        for sub_adi_id in sub_adi_ids:
            company_ids = sub_adi_id.mapped('company_id')
            for company_id in company_ids:
                values['sub_adi']['company_id_{0}'.format(company_id.id)] = dict()
                # * Filtering content
                sub_capacity = content.filtered(lambda self, company_id=company_id: self.company_id.id == company_id.id).mapped('capacity')
                sub_reimbursed_vacancies = content.filtered(lambda self, company_id=company_id: self.company_id.id == company_id.id).mapped('reimbursed_vacancies')
                sub_non_reimbursed_vacancies = content.filtered(lambda self, company_id=company_id: self.company_id.id == company_id.id).mapped('non_reimbursed_vacancies')
                company_name = content.filtered(lambda self, company_id=company_id: self.company_id.id == company_id.id).mapped('company_id.name')
                company_parish = content.filtered(lambda self, company_id=company_id: self.company_id.id == company_id.id).mapped('parish_id.name')
                # * Sums
                sub_capacity = sum(sub_capacity)
                sub_reimbursed_vacancies = sum(sub_reimbursed_vacancies)
                sub_non_reimbursed_vacancies = sum(sub_non_reimbursed_vacancies)
                # * Adding to dict
                values['sub_adi']['company_id_{0}'.format(company_id.id)]['capacity'] = sub_capacity
                values['sub_adi']['company_id_{0}'.format(company_id.id)]['reimbursed_vacancies'] = sub_reimbursed_vacancies
                values['sub_adi']['company_id_{0}'.format(company_id.id)]['non_reimbursed_vacancies'] = sub_non_reimbursed_vacancies
                values['sub_adi']['company_id_{0}'.format(company_id.id)]['company_name'] = company_name
                values['sub_adi']['company_id_{0}'.format(company_id.id)]['parish_name'] = company_parish


        values['sub_uai'] = dict()
        sub_uai_ids = content.filtered(lambda self: self.sub_dependent == 'uai')
        values['sub_uai']['company_ids'] = sub_uai_ids.mapped('company_id').ids if sub_uai_ids.mapped('company_id') else []
        for sub_uai_id in sub_uai_ids:
            company_ids = sub_uai_id.mapped('company_id')
            for company_id in company_ids:
                values['sub_uai']['company_id_{0}'.format(company_id.id)] = dict()
                # * Filtering content
                sub_capacity = content.filtered(lambda self, company_id=company_id: self.company_id.id == company_id.id).mapped('capacity')
                sub_reimbursed_vacancies = content.filtered(lambda self, company_id=company_id: self.company_id.id == company_id.id).mapped('reimbursed_vacancies')
                sub_non_reimbursed_vacancies = content.filtered(lambda self, company_id=company_id: self.company_id.id == company_id.id).mapped('non_reimbursed_vacancies')
                company_name = content.filtered(lambda self, company_id=company_id: self.company_id.id == company_id.id).mapped('company_id.name')
                company_parish = content.filtered(lambda self, company_id=company_id: self.company_id.id == company_id.id).mapped('parish_id.name')
                # * Sums
                sub_capacity = sum(sub_capacity)
                sub_reimbursed_vacancies = sum(sub_reimbursed_vacancies)
                sub_non_reimbursed_vacancies = sum(sub_non_reimbursed_vacancies)
                # * Adding to dict
                values['sub_uai']['company_id_{0}'.format(company_id.id)]['capacity'] = sub_capacity
                values['sub_uai']['company_id_{0}'.format(company_id.id)]['reimbursed_vacancies'] = sub_reimbursed_vacancies
                values['sub_uai']['company_id_{0}'.format(company_id.id)]['non_reimbursed_vacancies'] = sub_non_reimbursed_vacancies
                values['sub_uai']['company_id_{0}'.format(company_id.id)]['company_name'] = company_name
                values['sub_uai']['company_id_{0}'.format(company_id.id)]['parish_name'] = company_parish


        values['sub_supported_life'] = dict()
        sub_supported_life_ids = content.filtered(lambda self: self.sub_dependent == 'supported_life')
        values['sub_supported_life']['company_ids'] = sub_supported_life_ids.mapped('company_id').ids if sub_supported_life_ids.mapped('company_id') else []
        for sub_supported_life_id in sub_supported_life_ids:
            company_ids = sub_supported_life_id.mapped('company_id')
            for company_id in company_ids:
                values['sub_supported_life']['company_id_{0}'.format(company_id.id)] = dict()
                # * Filtering content
                sub_capacity = content.filtered(lambda self, company_id=company_id: self.company_id.id == company_id.id).mapped('capacity')
                sub_reimbursed_vacancies = content.filtered(lambda self, company_id=company_id: self.company_id.id == company_id.id).mapped('reimbursed_vacancies')
                sub_non_reimbursed_vacancies = content.filtered(lambda self, company_id=company_id: self.company_id.id == company_id.id).mapped('non_reimbursed_vacancies')
                company_name = content.filtered(lambda self, company_id=company_id: self.company_id.id == company_id.id).mapped('company_id.name')
                company_parish = content.filtered(lambda self, company_id=company_id: self.company_id.id == company_id.id).mapped('parish_id.name')
                # * Sums
                sub_capacity = sum(sub_capacity)
                sub_reimbursed_vacancies = sum(sub_reimbursed_vacancies)
                sub_non_reimbursed_vacancies = sum(sub_non_reimbursed_vacancies)
                # * Adding to dict
                values['sub_supported_life']['company_id_{0}'.format(company_id.id)]['capacity'] = sub_capacity
                values['sub_supported_life']['company_id_{0}'.format(company_id.id)]['reimbursed_vacancies'] = sub_reimbursed_vacancies
                values['sub_supported_life']['company_id_{0}'.format(company_id.id)]['non_reimbursed_vacancies'] = sub_non_reimbursed_vacancies
                values['sub_supported_life']['company_id_{0}'.format(company_id.id)]['company_name'] = company_name
                values['sub_supported_life']['company_id_{0}'.format(company_id.id)]['parish_name'] = company_parish


        values['sub_street_team'] = dict()
        sub_street_team_ids = content.filtered(lambda self: self.sub_homeless == 'street_team')
        values['sub_street_team']['company_ids'] = sub_street_team_ids.mapped('company_id').ids if sub_street_team_ids.mapped('company_id') else []
        for sub_street_team_id in sub_street_team_ids:
            company_ids = sub_street_team_id.mapped('company_id')
            for company_id in company_ids:
                values['sub_street_team']['company_id_{0}'.format(company_id.id)] = dict()
                # * Filtering content
                sub_capacity = content.filtered(lambda self, company_id=company_id: self.company_id.id == company_id.id).mapped('capacity')
                sub_reimbursed_vacancies = content.filtered(lambda self, company_id=company_id: self.company_id.id == company_id.id).mapped('reimbursed_vacancies')
                sub_non_reimbursed_vacancies = content.filtered(lambda self, company_id=company_id: self.company_id.id == company_id.id).mapped('non_reimbursed_vacancies')
                company_name = content.filtered(lambda self, company_id=company_id: self.company_id.id == company_id.id).mapped('company_id.name')
                company_parish = content.filtered(lambda self, company_id=company_id: self.company_id.id == company_id.id).mapped('parish_id.name')
                # * Sums
                sub_capacity = sum(sub_capacity)
                sub_reimbursed_vacancies = sum(sub_reimbursed_vacancies)
                sub_non_reimbursed_vacancies = sum(sub_non_reimbursed_vacancies)
                # * Adding to dict
                values['sub_street_team']['company_id_{0}'.format(company_id.id)]['capacity'] = sub_capacity
                values['sub_street_team']['company_id_{0}'.format(company_id.id)]['reimbursed_vacancies'] = sub_reimbursed_vacancies
                values['sub_street_team']['company_id_{0}'.format(company_id.id)]['non_reimbursed_vacancies'] = sub_non_reimbursed_vacancies
                values['sub_street_team']['company_id_{0}'.format(company_id.id)]['company_name'] = company_name
                values['sub_street_team']['company_id_{0}'.format(company_id.id)]['parish_name'] = company_parish


        values['sub_occupational_workshop'] = dict()
        sub_occupational_workshop_ids = content.filtered(lambda self: self.sub_homeless == 'occupational_workshop')
        values['sub_occupational_workshop']['company_ids'] = sub_occupational_workshop_ids.mapped('company_id').ids if sub_occupational_workshop_ids.mapped('company_id') else []
        for sub_occupational_workshop_id in sub_occupational_workshop_ids:
            company_ids = sub_occupational_workshop_id.mapped('company_id')
            for company_id in company_ids:
                values['sub_occupational_workshop']['company_id_{0}'.format(company_id.id)] = dict()
                # * Filtering content
                sub_capacity = content.filtered(lambda self, company_id=company_id: self.company_id.id == company_id.id).mapped('capacity')
                sub_reimbursed_vacancies = content.filtered(lambda self, company_id=company_id: self.company_id.id == company_id.id).mapped('reimbursed_vacancies')
                sub_non_reimbursed_vacancies = content.filtered(lambda self, company_id=company_id: self.company_id.id == company_id.id).mapped('non_reimbursed_vacancies')
                company_name = content.filtered(lambda self, company_id=company_id: self.company_id.id == company_id.id).mapped('company_id.name')
                company_parish = content.filtered(lambda self, company_id=company_id: self.company_id.id == company_id.id).mapped('parish_id.name')
                # * Sums
                sub_capacity = sum(sub_capacity)
                sub_reimbursed_vacancies = sum(sub_reimbursed_vacancies)
                sub_non_reimbursed_vacancies = sum(sub_non_reimbursed_vacancies)
                # * Adding to dict
                values['sub_occupational_workshop']['company_id_{0}'.format(company_id.id)]['capacity'] = sub_capacity
                values['sub_occupational_workshop']['company_id_{0}'.format(company_id.id)]['reimbursed_vacancies'] = sub_reimbursed_vacancies
                values['sub_occupational_workshop']['company_id_{0}'.format(company_id.id)]['non_reimbursed_vacancies'] = sub_non_reimbursed_vacancies
                values['sub_occupational_workshop']['company_id_{0}'.format(company_id.id)]['company_name'] = company_name
                values['sub_occupational_workshop']['company_id_{0}'.format(company_id.id)]['parish_name'] = company_parish

        
        values['sub_saas'] = dict()
        sub_saas_ids = content.filtered(lambda self: self.sub_family_community == 'saas')
        values['sub_saas']['company_ids'] = sub_saas_ids.mapped('company_id').ids if sub_saas_ids.mapped('company_id') else []
        for sub_saas_id in sub_saas_ids:
            company_ids = sub_saas_id.mapped('company_id')
            for company_id in company_ids:
                values['sub_saas']['company_id_{0}'.format(company_id.id)] = dict()
                # * Filtering content
                sub_capacity = content.filtered(lambda self, company_id=company_id: self.company_id.id == company_id.id).mapped('capacity')
                sub_reimbursed_vacancies = content.filtered(lambda self, company_id=company_id: self.company_id.id == company_id.id).mapped('reimbursed_vacancies')
                sub_non_reimbursed_vacancies = content.filtered(lambda self, company_id=company_id: self.company_id.id == company_id.id).mapped('non_reimbursed_vacancies')
                company_name = content.filtered(lambda self, company_id=company_id: self.company_id.id == company_id.id).mapped('company_id.name')
                company_parish = content.filtered(lambda self, company_id=company_id: self.company_id.id == company_id.id).mapped('parish_id.name')
                # * Sums
                sub_capacity = sum(sub_capacity)
                sub_reimbursed_vacancies = sum(sub_reimbursed_vacancies)
                sub_non_reimbursed_vacancies = sum(sub_non_reimbursed_vacancies)
                # * Adding to dict
                values['sub_saas']['company_id_{0}'.format(company_id.id)]['capacity'] = sub_capacity
                values['sub_saas']['company_id_{0}'.format(company_id.id)]['reimbursed_vacancies'] = sub_reimbursed_vacancies
                values['sub_saas']['company_id_{0}'.format(company_id.id)]['non_reimbursed_vacancies'] = sub_non_reimbursed_vacancies
                values['sub_saas']['company_id_{0}'.format(company_id.id)]['company_name'] = company_name
                values['sub_saas']['company_id_{0}'.format(company_id.id)]['parish_name'] = company_parish


        values['sub_self_help'] = dict()
        sub_self_help_ids = content.filtered(lambda self: self.sub_family_community == 'self_help')
        values['sub_self_help']['company_ids'] = sub_self_help_ids.mapped('company_id').ids if sub_self_help_ids.mapped('company_id') else []
        for sub_self_help_id in sub_saas_ids:
            company_ids = sub_self_help_id.mapped('company_id')
            for company_id in company_ids:
                values['sub_self_help']['company_id_{0}'.format(company_id.id)] = dict()
                # * Filtering content
                sub_capacity = content.filtered(lambda self, company_id=company_id: self.company_id.id == company_id.id).mapped('capacity')
                sub_reimbursed_vacancies = content.filtered(lambda self, company_id=company_id: self.company_id.id == company_id.id).mapped('reimbursed_vacancies')
                sub_non_reimbursed_vacancies = content.filtered(lambda self, company_id=company_id: self.company_id.id == company_id.id).mapped('non_reimbursed_vacancies')
                company_name = content.filtered(lambda self, company_id=company_id: self.company_id.id == company_id.id).mapped('company_id.name')
                company_parish = content.filtered(lambda self, company_id=company_id: self.company_id.id == company_id.id).mapped('parish_id.name')
                # * Sums
                sub_capacity = sum(sub_capacity)
                sub_reimbursed_vacancies = sum(sub_reimbursed_vacancies)
                sub_non_reimbursed_vacancies = sum(sub_non_reimbursed_vacancies)
                # * Adding to dict
                values['sub_self_help']['company_id_{0}'.format(company_id.id)]['capacity'] = sub_capacity
                values['sub_self_help']['company_id_{0}'.format(company_id.id)]['reimbursed_vacancies'] = sub_reimbursed_vacancies
                values['sub_self_help']['company_id_{0}'.format(company_id.id)]['non_reimbursed_vacancies'] = sub_non_reimbursed_vacancies
                values['sub_self_help']['company_id_{0}'.format(company_id.id)]['company_name'] = company_name
                values['sub_self_help']['company_id_{0}'.format(company_id.id)]['parish_name'] = company_parish


        values['sub_community_center'] = dict()
        sub_community_center_ids = content.filtered(lambda self: self.sub_family_community == 'community_center')
        values['sub_community_center']['company_ids'] = sub_community_center_ids.mapped('company_id').ids if sub_community_center_ids.mapped('company_id') else []
        for sub_community_center_id in sub_community_center_ids:
            company_ids = sub_community_center_id.mapped('company_id')
            for company_id in company_ids:
                values['sub_community_center']['company_id_{0}'.format(company_id.id)] = dict()
                # * Filtering content
                sub_capacity = content.filtered(lambda self, company_id=company_id: self.company_id.id == company_id.id).mapped('capacity')
                sub_reimbursed_vacancies = content.filtered(lambda self, company_id=company_id: self.company_id.id == company_id.id).mapped('reimbursed_vacancies')
                sub_non_reimbursed_vacancies = content.filtered(lambda self, company_id=company_id: self.company_id.id == company_id.id).mapped('non_reimbursed_vacancies')
                company_name = content.filtered(lambda self, company_id=company_id: self.company_id.id == company_id.id).mapped('company_id.name')
                company_parish = content.filtered(lambda self, company_id=company_id: self.company_id.id == company_id.id).mapped('parish_id.name')
                # * Sums
                sub_capacity = sum(sub_capacity)
                sub_reimbursed_vacancies = sum(sub_reimbursed_vacancies)
                sub_non_reimbursed_vacancies = sum(sub_non_reimbursed_vacancies)
                # * Adding to dict
                values['sub_community_center']['company_id_{0}'.format(company_id.id)]['capacity'] = sub_capacity
                values['sub_community_center']['company_id_{0}'.format(company_id.id)]['reimbursed_vacancies'] = sub_reimbursed_vacancies
                values['sub_community_center']['company_id_{0}'.format(company_id.id)]['non_reimbursed_vacancies'] = sub_non_reimbursed_vacancies
                values['sub_community_center']['company_id_{0}'.format(company_id.id)]['company_name'] = company_name
                values['sub_community_center']['company_id_{0}'.format(company_id.id)]['parish_name'] = company_parish


        values['sub_vacation_center'] = dict()
        sub_vacation_center_ids = content.filtered(lambda self: self.sub_family_community == 'vacation_center')
        values['sub_vacation_center']['company_ids'] = sub_vacation_center_ids.mapped('company_id').ids if sub_vacation_center_ids.mapped('company_id') else []
        for sub_vacation_center_id in sub_vacation_center_ids:
            company_ids = sub_vacation_center_id.mapped('company_id')
            for company_id in company_ids:
                values['sub_vacation_center']['company_id_{0}'.format(company_id.id)] = dict()
                # * Filtering content
                sub_capacity = content.filtered(lambda self, company_id=company_id: self.company_id.id == company_id.id).mapped('capacity')
                sub_reimbursed_vacancies = content.filtered(lambda self, company_id=company_id: self.company_id.id == company_id.id).mapped('reimbursed_vacancies')
                sub_non_reimbursed_vacancies = content.filtered(lambda self, company_id=company_id: self.company_id.id == company_id.id).mapped('non_reimbursed_vacancies')
                company_name = content.filtered(lambda self, company_id=company_id: self.company_id.id == company_id.id).mapped('company_id.name')
                company_parish = content.filtered(lambda self, company_id=company_id: self.company_id.id == company_id.id).mapped('parish_id.name')
                # * Sums
                sub_capacity = sum(sub_capacity)
                sub_reimbursed_vacancies = sum(sub_reimbursed_vacancies)
                sub_non_reimbursed_vacancies = sum(sub_non_reimbursed_vacancies)
                # * Adding to dict
                values['sub_vacation_center']['company_id_{0}'.format(company_id.id)]['capacity'] = sub_capacity
                values['sub_vacation_center']['company_id_{0}'.format(company_id.id)]['reimbursed_vacancies'] = sub_reimbursed_vacancies
                values['sub_vacation_center']['company_id_{0}'.format(company_id.id)]['non_reimbursed_vacancies'] = sub_non_reimbursed_vacancies
                values['sub_vacation_center']['company_id_{0}'.format(company_id.id)]['company_name'] = company_name
                values['sub_vacation_center']['company_id_{0}'.format(company_id.id)]['parish_name'] = company_parish


        values['sub_caf_canteen'] = dict()
        sub_caf_canteen_ids = content.filtered(lambda self: self.sub_family_community == 'caf_canteen')
        values['sub_caf_canteen']['company_ids'] = sub_caf_canteen_ids.mapped('company_id').ids if sub_caf_canteen_ids.mapped('company_id') else []
        for sub_caf_canteen_id in sub_caf_canteen_ids:
            company_ids = sub_caf_canteen_id.mapped('company_id')
            for company_id in company_ids:
                values['sub_caf_canteen']['company_id_{0}'.format(company_id.id)] = dict()
                # * Filtering content
                sub_capacity = content.filtered(lambda self, company_id=company_id: self.company_id.id == company_id.id).mapped('capacity')
                sub_reimbursed_vacancies = content.filtered(lambda self, company_id=company_id: self.company_id.id == company_id.id).mapped('reimbursed_vacancies')
                sub_non_reimbursed_vacancies = content.filtered(lambda self, company_id=company_id: self.company_id.id == company_id.id).mapped('non_reimbursed_vacancies')
                company_name = content.filtered(lambda self, company_id=company_id: self.company_id.id == company_id.id).mapped('company_id.name')
                company_parish = content.filtered(lambda self, company_id=company_id: self.company_id.id == company_id.id).mapped('parish_id.name')
                # * Sums
                sub_capacity = sum(sub_capacity)
                sub_reimbursed_vacancies = sum(sub_reimbursed_vacancies)
                sub_non_reimbursed_vacancies = sum(sub_non_reimbursed_vacancies)
                # * Adding to dict
                values['sub_caf_canteen']['company_id_{0}'.format(company_id.id)]['capacity'] = sub_capacity
                values['sub_caf_canteen']['company_id_{0}'.format(company_id.id)]['reimbursed_vacancies'] = sub_reimbursed_vacancies
                values['sub_caf_canteen']['company_id_{0}'.format(company_id.id)]['non_reimbursed_vacancies'] = sub_non_reimbursed_vacancies
                values['sub_caf_canteen']['company_id_{0}'.format(company_id.id)]['company_name'] = company_name
                values['sub_caf_canteen']['company_id_{0}'.format(company_id.id)]['parish_name'] = company_parish


        values['sub_life_support'] = dict()
        sub_life_support_ids = content.filtered(lambda self: self.sub_family_community == 'life_support')
        values['sub_life_support']['company_ids'] = sub_life_support_ids.mapped('company_id').ids if sub_life_support_ids.mapped('company_id') else []
        for sub_life_support_id in sub_life_support_ids:
            company_ids = sub_life_support_id.mapped('company_id')
            for company_id in company_ids:
                values['sub_life_support']['company_id_{0}'.format(company_id.id)] = dict()
                # * Filtering content
                sub_capacity = content.filtered(lambda self, company_id=company_id: self.company_id.id == company_id.id).mapped('capacity')
                sub_reimbursed_vacancies = content.filtered(lambda self, company_id=company_id: self.company_id.id == company_id.id).mapped('reimbursed_vacancies')
                sub_non_reimbursed_vacancies = content.filtered(lambda self, company_id=company_id: self.company_id.id == company_id.id).mapped('non_reimbursed_vacancies')
                company_name = content.filtered(lambda self, company_id=company_id: self.company_id.id == company_id.id).mapped('company_id.name')
                company_parish = content.filtered(lambda self, company_id=company_id: self.company_id.id == company_id.id).mapped('parish_id.name')
                # * Sums
                sub_capacity = sum(sub_capacity)
                sub_reimbursed_vacancies = sum(sub_reimbursed_vacancies)
                sub_non_reimbursed_vacancies = sum(sub_non_reimbursed_vacancies)
                # * Adding to dict
                values['sub_life_support']['company_id_{0}'.format(company_id.id)]['capacity'] = sub_capacity
                values['sub_life_support']['company_id_{0}'.format(company_id.id)]['reimbursed_vacancies'] = sub_reimbursed_vacancies
                values['sub_life_support']['company_id_{0}'.format(company_id.id)]['non_reimbursed_vacancies'] = sub_non_reimbursed_vacancies
                values['sub_life_support']['company_id_{0}'.format(company_id.id)]['company_name'] = company_name
                values['sub_life_support']['company_id_{0}'.format(company_id.id)]['parish_name'] = company_parish


        values['sub_insert_community'] = dict()
        sub_insert_community_ids = content.filtered(lambda self: self.sub_family_community == 'insert_community')
        values['sub_insert_community']['company_ids'] = sub_insert_community_ids.mapped('company_id').ids if sub_insert_community_ids.mapped('company_id') else []
        for sub_insert_community_id in sub_insert_community_ids:
            company_ids = sub_insert_community_id.mapped('company_id')
            for company_id in company_ids:
                values['sub_insert_community']['company_id_{0}'.format(company_id.id)] = dict()
                # * Filtering content
                sub_capacity = content.filtered(lambda self, company_id=company_id: self.company_id.id == company_id.id).mapped('capacity')
                sub_reimbursed_vacancies = content.filtered(lambda self, company_id=company_id: self.company_id.id == company_id.id).mapped('reimbursed_vacancies')
                sub_non_reimbursed_vacancies = content.filtered(lambda self, company_id=company_id: self.company_id.id == company_id.id).mapped('non_reimbursed_vacancies')
                company_name = content.filtered(lambda self, company_id=company_id: self.company_id.id == company_id.id).mapped('company_id.name')
                company_parish = content.filtered(lambda self, company_id=company_id: self.company_id.id == company_id.id).mapped('parish_id.name')
                # * Sums
                sub_capacity = sum(sub_capacity)
                sub_reimbursed_vacancies = sum(sub_reimbursed_vacancies)
                sub_non_reimbursed_vacancies = sum(sub_non_reimbursed_vacancies)
                # * Adding to dict
                values['sub_insert_community']['company_id_{0}'.format(company_id.id)]['capacity'] = sub_capacity
                values['sub_insert_community']['company_id_{0}'.format(company_id.id)]['reimbursed_vacancies'] = sub_reimbursed_vacancies
                values['sub_insert_community']['company_id_{0}'.format(company_id.id)]['non_reimbursed_vacancies'] = sub_non_reimbursed_vacancies
                values['sub_insert_community']['company_id_{0}'.format(company_id.id)]['company_name'] = company_name
                values['sub_insert_community']['company_id_{0}'.format(company_id.id)]['parish_name'] = company_parish


        values['sub_cat'] = dict()
        sub_cat_ids = content.filtered(lambda self: self.sub_family_community == 'cat')
        values['sub_cat']['company_ids'] = sub_cat_ids.mapped('company_id').ids if sub_cat_ids.mapped('company_id') else []
        for sub_cat_id in sub_cat_ids:
            company_ids = sub_cat_id.mapped('company_id')
            for company_id in company_ids:
                values['sub_cat']['company_id_{0}'.format(company_id.id)] = dict()
                # * Filtering content
                sub_capacity = content.filtered(lambda self, company_id=company_id: self.company_id.id == company_id.id).mapped('capacity')
                sub_reimbursed_vacancies = content.filtered(lambda self, company_id=company_id: self.company_id.id == company_id.id).mapped('reimbursed_vacancies')
                sub_non_reimbursed_vacancies = content.filtered(lambda self, company_id=company_id: self.company_id.id == company_id.id).mapped('non_reimbursed_vacancies')
                company_name = content.filtered(lambda self, company_id=company_id: self.company_id.id == company_id.id).mapped('company_id.name')
                company_parish = content.filtered(lambda self, company_id=company_id: self.company_id.id == company_id.id).mapped('parish_id.name')
                # * Sums
                sub_capacity = sum(sub_capacity)
                sub_reimbursed_vacancies = sum(sub_reimbursed_vacancies)
                sub_non_reimbursed_vacancies = sum(sub_non_reimbursed_vacancies)
                # * Adding to dict
                values['sub_cat']['company_id_{0}'.format(company_id.id)]['capacity'] = sub_capacity
                values['sub_cat']['company_id_{0}'.format(company_id.id)]['reimbursed_vacancies'] = sub_reimbursed_vacancies
                values['sub_cat']['company_id_{0}'.format(company_id.id)]['non_reimbursed_vacancies'] = sub_non_reimbursed_vacancies
                values['sub_cat']['company_id_{0}'.format(company_id.id)]['company_name'] = company_name
                values['sub_cat']['company_id_{0}'.format(company_id.id)]['parish_name'] = company_parish


        values['sub_food_aid'] = dict()
        sub_food_aid_ids = content.filtered(lambda self: self.sub_family_community == 'food_aid')
        values['sub_food_aid']['company_ids'] = sub_food_aid_ids.mapped('company_id').ids if sub_food_aid_ids.mapped('company_id') else []
        for sub_food_aid_id in sub_food_aid_ids:
            company_ids = sub_food_aid_id.mapped('company_id')
            for company_id in company_ids:
                values['sub_food_aid']['company_id_{0}'.format(company_id.id)] = dict()
                # * Filtering content
                sub_capacity = content.filtered(lambda self, company_id=company_id: self.company_id.id == company_id.id).mapped('capacity')
                sub_reimbursed_vacancies = content.filtered(lambda self, company_id=company_id: self.company_id.id == company_id.id).mapped('reimbursed_vacancies')
                sub_non_reimbursed_vacancies = content.filtered(lambda self, company_id=company_id: self.company_id.id == company_id.id).mapped('non_reimbursed_vacancies')
                company_name = content.filtered(lambda self, company_id=company_id: self.company_id.id == company_id.id).mapped('company_id.name')
                company_parish = content.filtered(lambda self, company_id=company_id: self.company_id.id == company_id.id).mapped('parish_id.name')
                # * Sums
                sub_capacity = sum(sub_capacity)
                sub_reimbursed_vacancies = sum(sub_reimbursed_vacancies)
                sub_non_reimbursed_vacancies = sum(sub_non_reimbursed_vacancies)
                # * Adding to dict
                values['sub_food_aid']['company_id_{0}'.format(company_id.id)]['capacity'] = sub_capacity
                values['sub_food_aid']['company_id_{0}'.format(company_id.id)]['reimbursed_vacancies'] = sub_reimbursed_vacancies
                values['sub_food_aid']['company_id_{0}'.format(company_id.id)]['non_reimbursed_vacancies'] = sub_non_reimbursed_vacancies
                values['sub_food_aid']['company_id_{0}'.format(company_id.id)]['company_name'] = company_name
                values['sub_food_aid']['company_id_{0}'.format(company_id.id)]['parish_name'] = company_parish


        values['sub_caap'] = dict()
        sub_caap_ids = content.filtered(lambda self: self.sub_family_aids == 'caap')
        values['sub_caap']['company_ids'] = sub_caap_ids.mapped('company_id').ids if sub_caap_ids.mapped('company_id') else []
        for sub_caap_id in sub_caap_ids:
            company_ids = sub_caap_id.mapped('company_id')
            for company_id in company_ids:
                values['sub_caap']['company_id_{0}'.format(company_id.id)] = dict()
                # * Filtering content
                sub_capacity = content.filtered(lambda self, company_id=company_id: self.company_id.id == company_id.id).mapped('capacity')
                sub_reimbursed_vacancies = content.filtered(lambda self, company_id=company_id: self.company_id.id == company_id.id).mapped('reimbursed_vacancies')
                sub_non_reimbursed_vacancies = content.filtered(lambda self, company_id=company_id: self.company_id.id == company_id.id).mapped('non_reimbursed_vacancies')
                company_name = content.filtered(lambda self, company_id=company_id: self.company_id.id == company_id.id).mapped('company_id.name')
                company_parish = content.filtered(lambda self, company_id=company_id: self.company_id.id == company_id.id).mapped('parish_id.name')
                # * Sums
                sub_capacity = sum(sub_capacity)
                sub_reimbursed_vacancies = sum(sub_reimbursed_vacancies)
                sub_non_reimbursed_vacancies = sum(sub_non_reimbursed_vacancies)
                # * Adding to dict
                values['sub_caap']['company_id_{0}'.format(company_id.id)]['capacity'] = sub_capacity
                values['sub_caap']['company_id_{0}'.format(company_id.id)]['reimbursed_vacancies'] = sub_reimbursed_vacancies
                values['sub_caap']['company_id_{0}'.format(company_id.id)]['non_reimbursed_vacancies'] = sub_non_reimbursed_vacancies
                values['sub_caap']['company_id_{0}'.format(company_id.id)]['company_name'] = company_name
                values['sub_caap']['company_id_{0}'.format(company_id.id)]['parish_name'] = company_parish


        values['sub_aids_sad'] = dict()
        sub_aids_sad_ids = content.filtered(lambda self: self.sub_family_aids == 'sad')
        values['sub_aids_sad']['company_ids'] = sub_aids_sad_ids.mapped('company_id').ids if sub_aids_sad_ids.mapped('company_id') else []
        for sub_aids_sad_id in sub_aids_sad_ids:
            company_ids = sub_aids_sad_id.mapped('company_id')
            for company_id in company_ids:
                values['sub_aids_sad']['company_id_{0}'.format(company_id.id)] = dict()
                # * Filtering content
                sub_capacity = content.filtered(lambda self, company_id=company_id: self.company_id.id == company_id.id).mapped('capacity')
                sub_reimbursed_vacancies = content.filtered(lambda self, company_id=company_id: self.company_id.id == company_id.id).mapped('reimbursed_vacancies')
                sub_non_reimbursed_vacancies = content.filtered(lambda self, company_id=company_id: self.company_id.id == company_id.id).mapped('non_reimbursed_vacancies')
                company_name = content.filtered(lambda self, company_id=company_id: self.company_id.id == company_id.id).mapped('company_id.name')
                company_parish = content.filtered(lambda self, company_id=company_id: self.company_id.id == company_id.id).mapped('parish_id.name')
                # * Sums
                sub_capacity = sum(sub_capacity)
                sub_reimbursed_vacancies = sum(sub_reimbursed_vacancies)
                sub_non_reimbursed_vacancies = sum(sub_non_reimbursed_vacancies)
                # * Adding to dict
                values['sub_aids_sad']['company_id_{0}'.format(company_id.id)]['capacity'] = sub_capacity
                values['sub_aids_sad']['company_id_{0}'.format(company_id.id)]['reimbursed_vacancies'] = sub_reimbursed_vacancies
                values['sub_aids_sad']['company_id_{0}'.format(company_id.id)]['non_reimbursed_vacancies'] = sub_non_reimbursed_vacancies
                values['sub_aids_sad']['company_id_{0}'.format(company_id.id)]['company_name'] = company_name
                values['sub_aids_sad']['company_id_{0}'.format(company_id.id)]['parish_name'] = company_parish


        values['sub_residence_infected'] = dict()
        sub_residence_infected_ids = content.filtered(lambda self: self.sub_family_aids == 'residence_infected')
        values['sub_residence_infected']['company_ids'] = sub_residence_infected_ids.mapped('company_id').ids if sub_residence_infected_ids.mapped('company_id') else []
        for sub_residence_infected_id in sub_residence_infected_ids:
            company_ids = sub_residence_infected_id.mapped('company_id')
            for company_id in company_ids:
                values['sub_residence_infected']['company_id_{0}'.format(company_id.id)] = dict()
                # * Filtering content
                sub_capacity = content.filtered(lambda self, company_id=company_id: self.company_id.id == company_id.id).mapped('capacity')
                sub_reimbursed_vacancies = content.filtered(lambda self, company_id=company_id: self.company_id.id == company_id.id).mapped('reimbursed_vacancies')
                sub_non_reimbursed_vacancies = content.filtered(lambda self, company_id=company_id: self.company_id.id == company_id.id).mapped('non_reimbursed_vacancies')
                company_name = content.filtered(lambda self, company_id=company_id: self.company_id.id == company_id.id).mapped('company_id.name')
                company_parish = content.filtered(lambda self, company_id=company_id: self.company_id.id == company_id.id).mapped('parish_id.name')
                # * Sums
                sub_capacity = sum(sub_capacity)
                sub_reimbursed_vacancies = sum(sub_reimbursed_vacancies)
                sub_non_reimbursed_vacancies = sum(sub_non_reimbursed_vacancies)
                # * Adding to dict
                values['sub_residence_infected']['company_id_{0}'.format(company_id.id)]['capacity'] = sub_capacity
                values['sub_residence_infected']['company_id_{0}'.format(company_id.id)]['reimbursed_vacancies'] = sub_reimbursed_vacancies
                values['sub_residence_infected']['company_id_{0}'.format(company_id.id)]['non_reimbursed_vacancies'] = sub_non_reimbursed_vacancies
                values['sub_residence_infected']['company_id_{0}'.format(company_id.id)]['company_name'] = company_name
                values['sub_residence_infected']['company_id_{0}'.format(company_id.id)]['parish_name'] = company_parish


        values['sub_direct_team'] = dict()
        sub_direct_team_ids = content.filtered(lambda self: self.sub_addicted == 'direct_team')
        values['sub_direct_team']['company_ids'] = sub_direct_team_ids.mapped('company_id').ids if sub_direct_team_ids.mapped('company_id') else []
        for sub_direct_team_id in sub_direct_team_ids:
            company_ids = sub_direct_team_id.mapped('company_id')
            for company_id in company_ids:
                values['sub_direct_team']['company_id_{0}'.format(company_id.id)] = dict()
                # * Filtering content
                sub_capacity = content.filtered(lambda self, company_id=company_id: self.company_id.id == company_id.id).mapped('capacity')
                sub_reimbursed_vacancies = content.filtered(lambda self, company_id=company_id: self.company_id.id == company_id.id).mapped('reimbursed_vacancies')
                sub_non_reimbursed_vacancies = content.filtered(lambda self, company_id=company_id: self.company_id.id == company_id.id).mapped('non_reimbursed_vacancies')
                company_name = content.filtered(lambda self, company_id=company_id: self.company_id.id == company_id.id).mapped('company_id.name')
                company_parish = content.filtered(lambda self, company_id=company_id: self.company_id.id == company_id.id).mapped('parish_id.name')
                # * Sums
                sub_capacity = sum(sub_capacity)
                sub_reimbursed_vacancies = sum(sub_reimbursed_vacancies)
                sub_non_reimbursed_vacancies = sum(sub_non_reimbursed_vacancies)
                # * Adding to dict
                values['sub_direct_team']['company_id_{0}'.format(company_id.id)]['capacity'] = sub_capacity
                values['sub_direct_team']['company_id_{0}'.format(company_id.id)]['reimbursed_vacancies'] = sub_reimbursed_vacancies
                values['sub_direct_team']['company_id_{0}'.format(company_id.id)]['non_reimbursed_vacancies'] = sub_non_reimbursed_vacancies
                values['sub_direct_team']['company_id_{0}'.format(company_id.id)]['company_name'] = company_name
                values['sub_direct_team']['company_id_{0}'.format(company_id.id)]['parish_name'] = company_parish


        values['sub_reintegration_program'] = dict()
        sub_reintegration_program_ids = content.filtered(lambda self: self.sub_addicted == 'reintegration_program')
        values['sub_reintegration_program']['company_ids'] = sub_reintegration_program_ids.mapped('company_id').ids if sub_reintegration_program_ids.mapped('company_id') else []
        for sub_reintegration_program_id in sub_reintegration_program_ids:
            company_ids = sub_reintegration_program_id.mapped('company_id')
            for company_id in company_ids:
                values['sub_reintegration_program']['company_id_{0}'.format(company_id.id)] = dict()
                # * Filtering content
                sub_capacity = content.filtered(lambda self, company_id=company_id: self.company_id.id == company_id.id).mapped('capacity')
                sub_reimbursed_vacancies = content.filtered(lambda self, company_id=company_id: self.company_id.id == company_id.id).mapped('reimbursed_vacancies')
                sub_non_reimbursed_vacancies = content.filtered(lambda self, company_id=company_id: self.company_id.id == company_id.id).mapped('non_reimbursed_vacancies')
                company_name = content.filtered(lambda self, company_id=company_id: self.company_id.id == company_id.id).mapped('company_id.name')
                company_parish = content.filtered(lambda self, company_id=company_id: self.company_id.id == company_id.id).mapped('parish_id.name')
                # * Sums
                sub_capacity = sum(sub_capacity)
                sub_reimbursed_vacancies = sum(sub_reimbursed_vacancies)
                sub_non_reimbursed_vacancies = sum(sub_non_reimbursed_vacancies)
                # * Adding to dict
                values['sub_reintegration_program']['company_id_{0}'.format(company_id.id)]['capacity'] = sub_capacity
                values['sub_reintegration_program']['company_id_{0}'.format(company_id.id)]['reimbursed_vacancies'] = sub_reimbursed_vacancies
                values['sub_reintegration_program']['company_id_{0}'.format(company_id.id)]['non_reimbursed_vacancies'] = sub_non_reimbursed_vacancies
                values['sub_reintegration_program']['company_id_{0}'.format(company_id.id)]['company_name'] = company_name
                values['sub_reintegration_program']['company_id_{0}'.format(company_id.id)]['parish_name'] = company_parish


        values['sub_call_center'] = dict()
        sub_call_center_ids = content.filtered(lambda self: self.sub_violence == 'call_center')
        values['sub_call_center']['company_ids'] = sub_call_center_ids.mapped('company_id').ids if sub_call_center_ids.mapped('company_id') else []
        for sub_call_center_id in sub_call_center_ids:
            company_ids = sub_call_center_id.mapped('company_id')
            for company_id in company_ids:
                values['sub_call_center']['company_id_{0}'.format(company_id.id)] = dict()
                # * Filtering content
                sub_capacity = content.filtered(lambda self, company_id=company_id: self.company_id.id == company_id.id).mapped('capacity')
                sub_reimbursed_vacancies = content.filtered(lambda self, company_id=company_id: self.company_id.id == company_id.id).mapped('reimbursed_vacancies')
                sub_non_reimbursed_vacancies = content.filtered(lambda self, company_id=company_id: self.company_id.id == company_id.id).mapped('non_reimbursed_vacancies')
                company_name = content.filtered(lambda self, company_id=company_id: self.company_id.id == company_id.id).mapped('company_id.name')
                company_parish = content.filtered(lambda self, company_id=company_id: self.company_id.id == company_id.id).mapped('parish_id.name')
                # * Sums
                sub_capacity = sum(sub_capacity)
                sub_reimbursed_vacancies = sum(sub_reimbursed_vacancies)
                sub_non_reimbursed_vacancies = sum(sub_non_reimbursed_vacancies)
                # * Adding to dict
                values['sub_call_center']['company_id_{0}'.format(company_id.id)]['capacity'] = sub_capacity
                values['sub_call_center']['company_id_{0}'.format(company_id.id)]['reimbursed_vacancies'] = sub_reimbursed_vacancies
                values['sub_call_center']['company_id_{0}'.format(company_id.id)]['non_reimbursed_vacancies'] = sub_non_reimbursed_vacancies
                values['sub_call_center']['company_id_{0}'.format(company_id.id)]['company_name'] = company_name
                values['sub_call_center']['company_id_{0}'.format(company_id.id)]['parish_name'] = company_parish


        values['sub_shelter_house'] = dict()
        sub_shelter_house_ids = content.filtered(lambda self: self.sub_violence == 'shelter_house')
        values['sub_shelter_house']['company_ids'] = sub_shelter_house_ids.mapped('company_id').ids if sub_shelter_house_ids.mapped('company_id') else []
        for sub_shelter_house_id in sub_shelter_house_ids:
            company_ids = sub_shelter_house_id.mapped('company_id')
            for company_id in company_ids:
                values['sub_shelter_house']['company_id_{0}'.format(company_id.id)] = dict()
                # * Filtering content
                sub_capacity = content.filtered(lambda self, company_id=company_id: self.company_id.id == company_id.id).mapped('capacity')
                sub_reimbursed_vacancies = content.filtered(lambda self, company_id=company_id: self.company_id.id == company_id.id).mapped('reimbursed_vacancies')
                sub_non_reimbursed_vacancies = content.filtered(lambda self, company_id=company_id: self.company_id.id == company_id.id).mapped('non_reimbursed_vacancies')
                company_name = content.filtered(lambda self, company_id=company_id: self.company_id.id == company_id.id).mapped('company_id.name')
                company_parish = content.filtered(lambda self, company_id=company_id: self.company_id.id == company_id.id).mapped('parish_id.name')
                # * Sums
                sub_capacity = sum(sub_capacity)
                sub_reimbursed_vacancies = sum(sub_reimbursed_vacancies)
                sub_non_reimbursed_vacancies = sum(sub_non_reimbursed_vacancies)
                # * Adding to dict
                values['sub_shelter_house']['company_id_{0}'.format(company_id.id)]['capacity'] = sub_capacity
                values['sub_shelter_house']['company_id_{0}'.format(company_id.id)]['reimbursed_vacancies'] = sub_reimbursed_vacancies
                values['sub_shelter_house']['company_id_{0}'.format(company_id.id)]['non_reimbursed_vacancies'] = sub_non_reimbursed_vacancies
                values['sub_shelter_house']['company_id_{0}'.format(company_id.id)]['company_name'] = company_name
                values['sub_shelter_house']['company_id_{0}'.format(company_id.id)]['parish_name'] = company_parish


        values['sub_children_support'] = dict()
        sub_children_support_ids = content.filtered(lambda self: self.sub_violence == 'sub_one_time')
        values['sub_children_support']['company_ids'] = sub_children_support_ids.mapped('company_id').ids if sub_children_support_ids.mapped('company_id') else []
        for sub_children_support_id in sub_children_support_ids:
            company_ids = sub_children_support_id.mapped('company_id')
            for company_id in company_ids:
                values['sub_children_support']['company_id_{0}'.format(company_id.id)] = dict()
                # * Filtering content
                sub_capacity = content.filtered(lambda self, company_id=company_id: self.company_id.id == company_id.id).mapped('capacity')
                sub_reimbursed_vacancies = content.filtered(lambda self, company_id=company_id: self.company_id.id == company_id.id).mapped('reimbursed_vacancies')
                sub_non_reimbursed_vacancies = content.filtered(lambda self, company_id=company_id: self.company_id.id == company_id.id).mapped('non_reimbursed_vacancies')
                company_name = content.filtered(lambda self, company_id=company_id: self.company_id.id == company_id.id).mapped('company_id.name')
                company_parish = content.filtered(lambda self, company_id=company_id: self.company_id.id == company_id.id).mapped('parish_id.name')
                # * Sums
                sub_capacity = sum(sub_capacity)
                sub_reimbursed_vacancies = sum(sub_reimbursed_vacancies)
                sub_non_reimbursed_vacancies = sum(sub_non_reimbursed_vacancies)
                # * Adding to dict
                values['sub_children_support']['company_id_{0}'.format(company_id.id)]['capacity'] = sub_capacity
                values['sub_children_support']['company_id_{0}'.format(company_id.id)]['reimbursed_vacancies'] = sub_reimbursed_vacancies
                values['sub_children_support']['company_id_{0}'.format(company_id.id)]['non_reimbursed_vacancies'] = sub_non_reimbursed_vacancies
                values['sub_children_support']['company_id_{0}'.format(company_id.id)]['company_name'] = company_name
                values['sub_children_support']['company_id_{0}'.format(company_id.id)]['parish_name'] = company_parish


        values['sub_ambulatory_care'] = dict()
        sub_ambulatory_care_ids = content.filtered(lambda self: self.sub_violence == 'ambulatory_care')
        values['sub_ambulatory_care']['company_ids'] = sub_ambulatory_care_ids.mapped('company_id').ids if sub_ambulatory_care_ids.mapped('company_id') else []
        for sub_ambulatory_care_id in sub_ambulatory_care_ids:
            company_ids = sub_ambulatory_care_id.mapped('company_id')
            for company_id in company_ids:
                values['sub_ambulatory_care']['company_id_{0}'.format(company_id.id)] = dict()
                # * Filtering content
                sub_capacity = content.filtered(lambda self, company_id=company_id: self.company_id.id == company_id.id).mapped('capacity')
                sub_reimbursed_vacancies = content.filtered(lambda self, company_id=company_id: self.company_id.id == company_id.id).mapped('reimbursed_vacancies')
                sub_non_reimbursed_vacancies = content.filtered(lambda self, company_id=company_id: self.company_id.id == company_id.id).mapped('non_reimbursed_vacancies')
                company_name = content.filtered(lambda self, company_id=company_id: self.company_id.id == company_id.id).mapped('company_id.name')
                company_parish = content.filtered(lambda self, company_id=company_id: self.company_id.id == company_id.id).mapped('parish_id.name')
                # * Sums
                sub_capacity = sum(sub_capacity)
                sub_reimbursed_vacancies = sum(sub_reimbursed_vacancies)
                sub_non_reimbursed_vacancies = sum(sub_non_reimbursed_vacancies)
                # * Adding to dict
                values['sub_ambulatory_care']['company_id_{0}'.format(company_id.id)]['capacity'] = sub_capacity
                values['sub_ambulatory_care']['company_id_{0}'.format(company_id.id)]['reimbursed_vacancies'] = sub_reimbursed_vacancies
                values['sub_ambulatory_care']['company_id_{0}'.format(company_id.id)]['non_reimbursed_vacancies'] = sub_non_reimbursed_vacancies
                values['sub_ambulatory_care']['company_id_{0}'.format(company_id.id)]['company_name'] = company_name
                values['sub_ambulatory_care']['company_id_{0}'.format(company_id.id)]['parish_name'] = company_parish


        values['sub_braille_press'] = dict()
        sub_braille_press_ids = content.filtered(lambda self: self.sub_violence == 'braille_press')
        values['sub_braille_press']['company_ids'] = sub_braille_press_ids.mapped('company_id').ids if sub_braille_press_ids.mapped('company_id') else []
        for sub_braille_press_id in sub_braille_press_ids:
            company_ids = sub_braille_press_id.mapped('company_id')
            for company_id in company_ids:
                values['sub_braille_press']['company_id_{0}'.format(company_id.id)] = dict()
                # * Filtering content
                sub_capacity = content.filtered(lambda self, company_id=company_id: self.company_id.id == company_id.id).mapped('capacity')
                sub_reimbursed_vacancies = content.filtered(lambda self, company_id=company_id: self.company_id.id == company_id.id).mapped('reimbursed_vacancies')
                sub_non_reimbursed_vacancies = content.filtered(lambda self, company_id=company_id: self.company_id.id == company_id.id).mapped('non_reimbursed_vacancies')
                company_name = content.filtered(lambda self, company_id=company_id: self.company_id.id == company_id.id).mapped('company_id.name')
                company_parish = content.filtered(lambda self, company_id=company_id: self.company_id.id == company_id.id).mapped('parish_id.name')
                # * Sums
                sub_capacity = sum(sub_capacity)
                sub_reimbursed_vacancies = sum(sub_reimbursed_vacancies)
                sub_non_reimbursed_vacancies = sum(sub_non_reimbursed_vacancies)
                # * Adding to dict
                values['sub_braille_press']['company_id_{0}'.format(company_id.id)]['capacity'] = sub_capacity
                values['sub_braille_press']['company_id_{0}'.format(company_id.id)]['reimbursed_vacancies'] = sub_reimbursed_vacancies
                values['sub_braille_press']['company_id_{0}'.format(company_id.id)]['non_reimbursed_vacancies'] = sub_non_reimbursed_vacancies
                values['sub_braille_press']['company_id_{0}'.format(company_id.id)]['company_name'] = company_name
                values['sub_braille_press']['company_id_{0}'.format(company_id.id)]['parish_name'] = company_parish


        values['sub_school_dogs'] = dict()
        sub_school_dogs_ids = content.filtered(lambda self: self.sub_violence == 'school_dogs')
        values['sub_school_dogs']['company_ids'] = sub_school_dogs_ids.mapped('company_id').ids if sub_school_dogs_ids.mapped('company_id') else []
        for sub_school_dogs_id in sub_school_dogs_ids:
            company_ids = sub_school_dogs_id.mapped('company_id')
            for company_id in company_ids:
                values['sub_school_dogs']['company_id_{0}'.format(company_id.id)] = dict()
                # * Filtering content
                sub_capacity = content.filtered(lambda self, company_id=company_id: self.company_id.id == company_id.id).mapped('capacity')
                sub_reimbursed_vacancies = content.filtered(lambda self, company_id=company_id: self.company_id.id == company_id.id).mapped('reimbursed_vacancies')
                sub_non_reimbursed_vacancies = content.filtered(lambda self, company_id=company_id: self.company_id.id == company_id.id).mapped('non_reimbursed_vacancies')
                company_name = content.filtered(lambda self, company_id=company_id: self.company_id.id == company_id.id).mapped('company_id.name')
                company_parish = content.filtered(lambda self, company_id=company_id: self.company_id.id == company_id.id).mapped('parish_id.name')
                # * Sums
                sub_capacity = sum(sub_capacity)
                sub_reimbursed_vacancies = sum(sub_reimbursed_vacancies)
                sub_non_reimbursed_vacancies = sum(sub_non_reimbursed_vacancies)
                # * Adding to dict
                values['sub_school_dogs']['company_id_{0}'.format(company_id.id)]['capacity'] = sub_capacity
                values['sub_school_dogs']['company_id_{0}'.format(company_id.id)]['reimbursed_vacancies'] = sub_reimbursed_vacancies
                values['sub_school_dogs']['company_id_{0}'.format(company_id.id)]['non_reimbursed_vacancies'] = sub_non_reimbursed_vacancies
                values['sub_school_dogs']['company_id_{0}'.format(company_id.id)]['company_name'] = company_name
                values['sub_school_dogs']['company_id_{0}'.format(company_id.id)]['parish_name'] = company_parish


        values['sub_cri'] = dict()
        sub_cri_ids = content.filtered(lambda self: self.sub_violence == 'cri')
        values['sub_cri']['company_ids'] = sub_cri_ids.mapped('company_id').ids if sub_cri_ids.mapped('company_id') else []
        for sub_cri_id in sub_cri_ids:
            company_ids = sub_cri_id.mapped('company_id')
            for company_id in company_ids:
                values['sub_cri']['company_id_{0}'.format(company_id.id)] = dict()
                # * Filtering content
                sub_capacity = content.filtered(lambda self, company_id=company_id: self.company_id.id == company_id.id).mapped('capacity')
                sub_reimbursed_vacancies = content.filtered(lambda self, company_id=company_id: self.company_id.id == company_id.id).mapped('reimbursed_vacancies')
                sub_non_reimbursed_vacancies = content.filtered(lambda self, company_id=company_id: self.company_id.id == company_id.id).mapped('non_reimbursed_vacancies')
                company_name = content.filtered(lambda self, company_id=company_id: self.company_id.id == company_id.id).mapped('company_id.name')
                company_parish = content.filtered(lambda self, company_id=company_id: self.company_id.id == company_id.id).mapped('parish_id.name')
                # * Sums
                sub_capacity = sum(sub_capacity)
                sub_reimbursed_vacancies = sum(sub_reimbursed_vacancies)
                sub_non_reimbursed_vacancies = sum(sub_non_reimbursed_vacancies)
                # * Adding to dict
                values['sub_cri']['company_id_{0}'.format(company_id.id)]['capacity'] = sub_capacity
                values['sub_cri']['company_id_{0}'.format(company_id.id)]['reimbursed_vacancies'] = sub_reimbursed_vacancies
                values['sub_cri']['company_id_{0}'.format(company_id.id)]['non_reimbursed_vacancies'] = sub_non_reimbursed_vacancies
                values['sub_cri']['company_id_{0}'.format(company_id.id)]['company_name'] = company_name
                values['sub_cri']['company_id_{0}'.format(company_id.id)]['parish_name'] = company_parish

        return request.render('clds.social_res_index', values)

    
